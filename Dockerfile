ARG IMAGE_TAG=master
FROM registry.gitlab.e.foundation:5000/e/documentation/user/jekyll:$IMAGE_TAG AS jekyll

COPY . /tmp
RUN date > /tmp/build_date
RUN jekyll build -d /tmp/e_docs_website && rm /tmp/Gemfile*

FROM registry.gitlab.e.foundation:5000/e/documentation/user/ubuntu:$IMAGE_TAG AS ubuntu

COPY --from=jekyll /tmp/e_docs_website/ /tmp/e_docs_website/
RUN minify -r -o /tmp/e_docs_website/ /tmp/e_docs_website/

FROM registry.gitlab.e.foundation:5000/e/documentation/user/nginx:$IMAGE_TAG
COPY --chown=www-data:www-data --from=ubuntu /tmp/e_docs_website/ /tmp/e_docs_website/
