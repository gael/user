var selectedFilters = {}

// Allow sorting by number, ref https://github.com/DataTables/Plugins/blob/master/sorting/any-number.js
_anyNumberSort = function(a, b, high) {
	var reg = /[+-]?((\d+(\.\d*)?)|\.\d+)([eE][+-]?[0-9]+)?/;        
	a = a.replace(',','.').match(reg);
	a = a !== null ? parseFloat(a[0]) : high;
	b = b.replace(',','.').match(reg);
	b = b !== null ? parseFloat(b[0]) : high;
	return ((a < b) ? -1 : ((a > b) ? 1 : 0));    
}

jQuery.extend( jQuery.fn.dataTableExt.oSort, {
	"any-number-asc": function (a, b) {
		return _anyNumberSort(a, b, Number.POSITIVE_INFINITY);
	},
	"any-number-desc": function (a, b) {
		return _anyNumberSort(a, b, Number.NEGATIVE_INFINITY) * -1;
	}
});

// To allow reseting the sort options, ref https://stackoverflow.com/questions/27008837/jquery-datatable-reset-sorting
jQuery.fn.dataTableExt.oApi.fnSortNeutral = function ( oSettings )
{
    /* Remove any current sorting */
    oSettings.aaSorting = [];

    /* Sort display arrays so we get them in numerical order */
    oSettings.aiDisplay.sort( function (x,y) {
        return x-y;
    } );
    oSettings.aiDisplayMaster.sort( function (x,y) {
        return x-y;
    } );

    /* Redraw */
    oSettings.oApi._fnReDraw( oSettings );
};

function updateFilters() {
  $(".smartphone-table tr").filter(function() {
    $(this).toggle(true)
  });
  for (var column in selectedFilters) {
    value = selectedFilters[column].value.toLowerCase();
    if (column === "device-type" && value != "-1") {
      $(".smartphone-table tbody tr").each((_, el) => {
        const deviceType = $(el).attr("data-device-type");
        if (value !== deviceType) {
          $(el).toggle(false);
        }
      })
      continue;
    }
    $(".smartphone-table tr ." + column).filter(function() {
      var lineValue = $(this).text().toLowerCase().replaceAll(/\s/g, ' ');
      if (selectedFilters[column].type == "truncate") {
        lineValue = Math.trunc(parseFloat(lineValue))
      }

      if ((selectedFilters[column].type == "text" && lineValue.indexOf(value) < 0 ||
          lineValue != value && selectedFilters[column].type == "truncate") &&
        value != -1) {
        $(this.closest('tr')).toggle(false)
      }
    });
  }

}

function addFilterSelector(selectorId, colomnClass, type="text") {
  $("#" + selectorId).change(function(event) {
    var value = event.target.value;
    selectedFilters[colomnClass] = {}
    selectedFilters[colomnClass].value = value
    selectedFilters[colomnClass].type = type
    updateFilters()
  })
}

function addFilterTruncate(selectorId, colomnClass) {
  $("#" + selectorId).change(function(event) {
    var value = event.target.value;
    selectedFilters[colomnClass] = {}
    selectedFilters[colomnClass].value = value
    selectedFilters[colomnClass].type = "truncate"
    updateFilters()
  })
}

function addFilterRadio(radioName, colomnClass) {
  $('input[type=radio][name=' + radioName + ']').change(function(event) {
    var value = event.target.value;
    selectedFilters[colomnClass] = {}
    selectedFilters[colomnClass].type = "text"
    selectedFilters[colomnClass].value = value
    updateFilters()
  })
}

$(document).ready(function() {
  $("#myInput").on("keyup", function() {
    var value = $(this).val().toLowerCase();
    $(".smartphone-table tbody tr").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });

  $('.custom-select').change(function() {
    var txtSearch = "";
    txtSearch = txtSearch + $(this).find('option:selected').text();
  });

  setupPopoverForDevicesModels();

  addFilterSelector("brand", "brand");
  addFilterSelector("release", "release");
  addFilterTruncate("size", "size");
  addFilterSelector("region", "region");
  addFilterSelector("channel", "channel");
  addFilterSelector("maturity", "maturity");
  addFilterSelector("install", "install");
  addFilterSelector("device-type", "device-type", "device-type");

  addFilterRadio("battery", "battery");
  addFilterRadio("sim", "sim");


  $('#btnReset').on('click', function() {
    clear_form_elements("device-search");
    clear_form_elements("myCollapse");
    $("#myInput").keyup();
    $('#smartphone-table').dataTable().fnSortNeutral();
  });

  $('#smartphone-table').DataTable({
    paging: false,
    stateSave: true,
    fixedHeader: true,
    bFilter: false,
    info: false,
    "columns": [
      null,
      null,
      null,
      null,
      null,
      { "type": "any-number" },
      null,
      null,
    ]
  });

});

function setupPopoverForDevicesModels() {
  $('[data-toggle="popover-models"]').popover({
    html: true,
    trigger: 'hover',
  })
}

function clear_form_elements(class_name) {
  $("." + class_name).find(':input').each(function() {
    switch (this.type) {
      case 'text':
      case 'select-multiple':
        $(this).val('');
        break;
      case 'radio':
        this.checked = false;
        break;
    }
  });
  selectedFilters = {};
  updateFilters();
}