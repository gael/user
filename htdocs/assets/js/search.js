const DEFAULT_LANG = 'en';

$(document).ready(() => {

    setupAutocomplete();

    document.querySelector(".search-form")?.addEventListener("submit", async e => {
        e.preventDefault();
        const searchQuery = document.querySelector(".search-input.tt-input").value;
        makeQuery(searchQuery)
    });

    if (!shouldRedirectForSearchResults()) {
        const urlParams = new URLSearchParams(window.location.search);
        if (!urlParams.has('q')) return;
        const searchQuery = urlParams.get('q');
        $(".search-input").val(searchQuery);
        makeQuery(searchQuery);
    }

});

function setupAutocomplete() {
    const lang = getCurrentLang();

    const sections = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: `/${lang}/autocomplete`,
            transform: function(response) {
                return response['autocomplete'];
            }
        },
    });

    $('.search-input').typeahead({
        hint: true,
        minLength: 1,
    }, {
        name: 'sections',
        source: sections
    })

    $('.search-input').bind('typeahead:select', function(_event, suggestion) {
        makeQuery(suggestion);
    });
}

function setLoading(isLoading) {
    if (isLoading) {
        $(".loading-icon").css("display", "flex");
    } else {
        $(".loading-icon").css("display", "none");
    }
}

function showSearchResultsMessage(shouldShow) {
    if (shouldShow) {
        $(".search-results-message").css("display", "block");
    } else {
        $(".search-results-message").css("display", "none");
    }
}

function showNoResultsMessage(shouldShow) {
    if (shouldShow) {
        $(".no-results-message").css("display", "flex");
    } else {
        $(".no-results-message").css("display", "none");
    }
}

async function makeQuery(searchQuery) {
    searchQueryEncoded = encodeURIComponent(searchQuery);
    if (searchQueryEncoded === "") {
        return
    }
    if (shouldRedirectForSearchResults()) {

        const lang = getCurrentLang();
        
        let redirectTo = `/${lang}/search?q=${searchQueryEncoded}`;
        if (lang === DEFAULT_LANG) {
            redirectTo = `/search?q=${searchQueryEncoded}`;
        }

        window.location = redirectTo;
        return
    }

    searchQueryEncoded = searchQueryEncoded.replaceAll("%2F", "");

    setLoading(true);
    showSearchResultsMessage(false);
    showNoResultsMessage(false);
    clearListOfResults();
    const body = await searchForQuery(searchQueryEncoded);
    const specificPost = body['specific_post'];
    if (specificPost) {
        window.location = specificPost['url'];
        return
    }
    const results = body['search_results'];
    setLoading(false);
    
    if (results.length > 0) {
        updateSearchResultsMessage(searchQuery, results.length);
        showSearchResultsMessage(true);
    } else {
        showNoResultsMessage(true);
    }
    updateListOfResults(results);
}

function updateSearchResultsMessage(query, numberOfResults) {
    const newMessageElement = document.querySelector(".template-search-message").content.cloneNode(true);
    $(newMessageElement).find(".search-results__counter").text(numberOfResults);
    $(newMessageElement).find(".search-query").text(query);

    const container = $(".search-results-message");
    container.empty();
    container.append(newMessageElement);
}

function shouldRedirectForSearchResults() {
    return $(".search-results").length === 0;
}

async function searchForQuery(searchQuery) {
    const lang = getCurrentLang();
    return fetch(`/${lang}/search/${searchQuery}`).then(async r => {
        const body = await r.json();
        return body;
    }).catch(error => {
        console.error(error);
        return {
            'search_results': [],
            'specific_post': null,
        };
    });
}

function getCurrentLang() {
    const html = document.querySelector("html");
    const lang = html.getAttribute('lang');
    if (lang === null) return DEFAULT_LANG;
    return lang;
}

function updateListOfResults(results) {
    clearListOfResults();
    addResultsToListContainer(results);
}

function addResultsToListContainer(results) {
    const resultsContainer = getSearchResultsContainer();
    for (let i in results) {
        const result = results[i];
        const element = createSearchResultElement(result);
        resultsContainer.appendChild(element);
    }
}

function getSearchResultsContainer() {
    return document.querySelector(".search-results");
}

function clearListOfResults() {
    const resultsContainer = getSearchResultsContainer();
    while (resultsContainer.firstChild) {
        resultsContainer.firstChild.remove();
    }
}

function createSearchResultElement(searchResult) {
    const newResultElement = document.querySelector(".template-search-item").content.cloneNode(true);

    const url = searchResult["url"];

    $(newResultElement).find(".search-result__title .search-result__link").text(searchResult["title"]);
    $(newResultElement).find(".search-result__body").html(searchResult["description"]);
    $(newResultElement).find(".search-result__link").attr("href", url);
    $(newResultElement).find(".search-result__footer .search-result__link").text(fromUrlToBreadcrumbs(url));

    return newResultElement;
}

function fromUrlToBreadcrumbs(urlStr) {
    let breadcrumb = urlStr;
    if (breadcrumb.endsWith('/index.html')) {
        breadcrumb = breadcrumb.slice(0, -11);
    }
    return breadcrumb;
}