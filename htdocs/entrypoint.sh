#!/bin/bash
rsync -rlDog --delete --exclude .gitignore /tmp/e_docs_website/ /usr/share/nginx/html/
nginx -g "daemon off;"