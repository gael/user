module DevicesPagesPlugin
    class DevicesPagesGenerator < Jekyll::Generator
      safe true
  
      def generate(site)
        site.data['laptops'].each do | device, device_data |
          site.pages << LaptopInstallPage.new(site, device, device_data, 'titles.devices_install_page')
          site.pages << LaptopInfoPage.new(site, device, device_data, 'titles.devices_information_page', true)
        end

        site.data['devices'].each do | device, device_data |
          site.pages << InstallPage.new(site, device, device_data, 'titles.devices_install_page')
          site.pages << InfoPage.new(site, device, device_data, 'titles.devices_information_page', true)
          if device_data.has_key?('upgrade_available')
            site.pages << UpgradePage.new(site, device, device_data, 'titles.devices_upgrade_page')
          end
        end
      end
    end

    class BaseDevicePage < Jekyll::Page
      def initialize(site, dir, device_data, title, should_index_page = false)
        @site = site
        @base = site.source
        @dir  = dir
  
        @basename = 'index'
        @ext      = '.html'
        @name     = 'index.html'
  
        @data = {
          'device' => device_data,
          'title' => title,
          'do_not_index_page' => !should_index_page,
        }
        data.default_proc = self.get_default_proc
      end

      def url_placeholders
        {
          :device   => @dir,
          :basename   => basename,
          :output_ext => output_ext,
        }
      end
    end

    class InstallPage < BaseDevicePage
      def get_default_proc
        return proc do |_, key|
          site.frontmatter_defaults.find(relative_path, :install, key)
        end
      end
    end

    class InfoPage < BaseDevicePage
      def get_default_proc
        return proc do |_, key|
          site.frontmatter_defaults.find(relative_path, :information, key)
        end
      end
    end

    class UpgradePage < BaseDevicePage
      def get_default_proc
        return proc do |_, key|
          site.frontmatter_defaults.find(relative_path, :upgrade, key)
        end
      end
    end

    class LaptopInstallPage < BaseDevicePage
      def get_default_proc
        return proc do |_, key|
          site.frontmatter_defaults.find(relative_path, :laptop_install, key)
        end
      end
    end

    class LaptopInfoPage < BaseDevicePage
      def get_default_proc
        return proc do |_, key|
          site.frontmatter_defaults.find(relative_path, :laptop_information, key)
        end
      end
    end
  end