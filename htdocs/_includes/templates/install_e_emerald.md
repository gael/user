## Installing /e/ from the bootloader

1. Download the build package linked in the `Downloads` section above and extract it to a folder of your choice
2. Connect the device to your PC if not already done
> Ensure you have a cable which allowed file transfer. Some cables can only be used for charging.

3. Move to the directory where you have extracted the files in the first step. To do that type the below command in a console
```bash
cd path-of-the-extracted-files
```
4. Install /e/OS by typing the following commands in the console

```
fastboot erase userdata
fastboot erase metadata

fastboot flash gz_a gz.img
fastboot flash lk_a lk.img
fastboot flash md1img_a md1img.img
fastboot flash scp_a scp.img
fastboot flash spmfw_a spmfw.img
fastboot flash sspm_a sspm.img
fastboot flash tee_a tee.img

fastboot flash boot_a boot.img
fastboot flash dtbo_a dtbo.img
fastboot flash vbmeta_a vbmeta.img
fastboot flash super super.img

fastboot --set-active=a
fastboot -w reboot
```

{% include alerts/tip.html content="If your device is not detect by fastboot, try to use `sudo fastboot` instead"%}