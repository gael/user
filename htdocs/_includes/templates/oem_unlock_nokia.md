## Unlocking the bootloader

{% include alerts/warning.html content="An official method of unlocking the bootloader is not available, but there may be some unofficial methods available. Unofficial methods are not recommended or supported by /e/OS, so use them at your own risk! An unlocked bootloader is required to install" %}
