## Installing a custom recovery

1. Connect your device to your PC via USB.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type

    ```shell
    adb reboot bootloader
    ```
1. Once the device is in fastboot mode, verify your PC finds it by typing

    ```shell
    fastboot devices
{% if device.recovery_install_command %}    ```
1. Flash recovery onto your device

    ```shell
    {{ device.recovery_install_command }} recoveryfilename.img
    ```
    
    > {% translate content.recovery_file_name_text %} 
{% endif %}    
1. Now reboot into recovery to verify the installation
  * {% t devices.with_the_device_powered_off %}
  * {% t device.recovery_boot %}