# Install {%- if device.minimal_os %} /e/ minimal {%- else %} /e/ {%- endif %}  on a {{ device.vendor }} {{ device.name }} - "{{ device.codename }}" {%- if device.is_beta %} (beta) {%- endif %}

{% include alerts/tip.html content="Please read through the instructions at least once before actually following them, so as to avoid any problems later." %}

{% if device.can_rollback != true %}
{% include alerts/warning.html content="Downgrading Smartphones already on Android Q or LineageOS 17.x to Pie or other OS can cause instability or at worst brick some devices" %}
{% endif %}

{% include alerts/danger.html content="Flash your device only if you **know** what you are doing and are OK taking the **associated risk**. All builds are provided as best effort, **without any guarantee**. The /e/ project and its project members deny any and all responsibility about the consequences of using /e/ software and or /e/ services." %}

{% assign backup_and_restore_with_twrp_link = "/pages/backup-and-restore-with-twrp" | relative_url%}
{% if device.uses_twrp %}
    {% capture content %}
        Installing a custom recovery or unlocking the bootloader will erase all data on your device! [Take a backup]({{ backup_and_restore_with_twrp_link }}) of your data before proceeding!!
    {% endcapture %}
{% include alerts/tip.html content=content %}
{% endif %}

{% assign install_adb_link = "/pages/install-adb" | relative_url%}
{% assign enable_usb_debug_link = "/pages/enable-usb-debugging" | relative_url %}
# Requirements
- If required take a backup of all important data from your phone on an external storage device before proceeding.
- Do not take a backup on the same device as some of these actions will format the device and delete the backup.
{%- if device.uses_twrp %}
- A guide on backing up with TWRP is available [here]({{ backup_and_restore_with_twrp_link }})
{%- endif %}
- Ensure your phone is charged more than 50%
{%- if device.vendor == "Samsung" %}
- Check that `adb` is enabled on your PC. If not you can find the setup instructions [here]({{ install_adb_link }})
{%- else %}
- Check that `adb` and `fastboot` are enabled on your PC. If not you can find the setup instructions [here]({{ install_adb_link }})
{%- endif %}
- Download all the files and images mentioned in the download section below before starting the installation
- Make sure you have a working data cable to connect your device to the PC. There are cables which are only for charging and do not transfer data.
{% if device.oem_unlock_code_required %}
- Your device requires a code to unlock the bootloader. Get the code [here]({{ device.oem_unlock_code_required }}) before proceeding with the next steps
{% endif %}
- Enable USB debugging on your device. You can find the instructions [here]({{ enable_usb_debug_link }})

{%- if device.should_be_latest_version != false %}
{% include alerts/danger.html content="Do not install /e/OS on top of a higher version of stock OS.
Before installing /e/OS ensure your device has the latest stock OS of the same version as /e/OS. If only lower versions of the stock are available then install the last available lower version before installing /e/OS. " %}
{%- endif %}

{%- if device.before_install_args %}

{% capture upgrade_os_version %}
Before installing /e/OS on the  {{device.vendor}} {{device.name }} {{ device.codename }} for the first time, install the latest stock OS version {{ device.before_install_args.version }} build on the device
{% endcapture%}

{% include alerts/tip.html content = upgrade_os_version  %}

{%- endif %}

{%- if device.revert_to_stock %}

- If you need to revert to stock ROM (vendor provided ROM), Please follow the instructions given in the OS specific guide linked here


{%- for guide in device.revert_to_stock %}
     - {{ guide.os | capitalize}} OS  [Guide]({{guide.url | relative_url}})
{%- endfor %}
{%- endif  %}

{%- if device.name == "FP2" %}
{% include alerts/tip.html content = "Please note that the bootloader of the FP2 is unlocked by default."%}
{%-endif %}
