{%- assign device = page.device -%}

## Unlocking the bootloader

{% include alerts/tip.html content="The steps below only need to be run once per device." %}
{% include alerts/warning.html content="Unlocking the bootloader will erase all data on your device!
Before proceeding, ensure the data you would like to retain is backed up to your PC and/or your Google account, or equivalent. Please note that OEM backup solutions like Samsung and Motorola backup may not be accessible from /e/OS once installed." %}

1. Visit [Lenovo’s ZUI official unlocking website](https://www.zui.com/iunlock), where you’ll be asked to fill in some device and contact information.

1. Follow the instructions and get your unlock file.

1. Enable OEM unlock in the Developer options under device Settings
1. Connect the device to your PC via USB.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:

     `adb reboot bootloader`

     You can also boot into fastboot mode via a key combination:
        
     {% t devices.download_boot_lenovo %}

 1. Once the device is in fastboot mode, verify your PC finds it by typing:

    `fastboot devices`

    If you don’t get any output or an error:
       - on Windows: make sure the device appears in the device manager without a triangle. Try other drivers until the command above works!
       - on Linux or macOS: If you see no permissions fastboot try running fastboot as root. When the output is empty, check your USB cable and port!

1.  Now type the following command to unlock the bootloader:

    `fastboot flash unlock sn.img`

    Where `sn.img` is the bootloader unlock file you received in the email.

1.   Next, use your second unlock command
   
     `fastboot oem unlock-go`
      - At this point the device may display on-screen prompts which will require interaction to continue the process of unlocking the bootloader. Please take whatever actions the device asks you to to proceed.
    
1. Wait for the bootloader unlocking process to complete. Once finished, you can check if bootloader is successfully unlocked by typing:

    `fastboot getvar unlocked`

    Verify that the response is `unlocked: yes`. In that case, you can now install third-party firmware.

1.   If the device does not automatically reboot, reboot it. It should now be unlocked.
1.   Since the device resets completely, you will need to re-enable USB debugging to continue.
