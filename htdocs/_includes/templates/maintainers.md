{% if device.unofficial_maintainers %}

**For maintaining the device trees for the {{ device.codename }} we thank**
{% for maintainer in device.unofficial_maintainers %}
- [{{ maintainer.username }}]({{ maintainer.url }}) (OS version: {{ maintainer.os_version }})
{%- endfor %}

{% endif %}