## Installing /e/ 

### Installing /e/ using IMG or image file


1. Boot your FP4 in bootloader mode, and plug it to your computer
1. On your computer, download the zip file from the link provided above. Unzip the folder and browse into it 
1. Flash /e/ with the following commands:

>> 

    fastboot flash bluetooth_a bluetooth.img
    fastboot flash bluetooth_b bluetooth.img
    fastboot flash devcfg_a devcfg.img
    fastboot flash devcfg_b devcfg.img
    fastboot flash dsp_a dsp.img
    fastboot flash dsp_b dsp.img
    fastboot flash modem_a modem.img
    fastboot flash modem_b modem.img
    fastboot flash xbl_a xbl.img
    fastboot flash xbl_b xbl.img
    fastboot flash tz_a tz.img
    fastboot flash tz_b tz.img
    fastboot flash hyp_a hyp.img
    fastboot flash hyp_b hyp.img
    fastboot flash keymaster_a keymaster.img
    fastboot flash keymaster_b keymaster.img

    fastboot flash abl_a abl.img
    fastboot flash abl_b abl.img
    fastboot flash boot_a boot.img
    fastboot flash boot_b boot.img
    fastboot flash recovery_a recovery.img
    fastboot flash recovery_b recovery.img
    fastboot flash dtbo_a dtbo.img
    fastboot flash dtbo_b dtbo.img
    fastboot flash vbmeta_system_a vbmeta_system.img
    fastboot flash vbmeta_system_b vbmeta_system.img
    fastboot flash vbmeta_a vbmeta.img
    fastboot flash vbmeta_b vbmeta.img
    fastboot flash super super.img

    fastboot flash aop_a aop.img
    fastboot flash aop_b aop.img
    fastboot flash featenabler_a featenabler.img
    fastboot flash featenabler_b featenabler.img
    fastboot flash imagefv_a imagefv.img
    fastboot flash imagefv_b imagefv.img
    fastboot flash multiimgoem_a multiimgoem.img
    fastboot flash multiimgoem_b multiimgoem.img
    fastboot flash qupfw_a qupfw.img
    fastboot flash qupfw_b qupfw.img
    fastboot flash uefisecapp_a uefisecapp.img
    fastboot flash uefisecapp_b uefisecapp.img
    fastboot flash xbl_config_a xbl_config.img
    fastboot flash xbl_config_b xbl_config.img
    fastboot flash core_nhlos_a core_nhlos.img
    fastboot flash core_nhlos_b core_nhlos.img

    fastboot erase userdata
    fastboot erase metadata

    fastboot --set-active=a

## Locking the Bootloader
 Once you have completed the above steps  and before rebooting you can and should lock your Bootloader. 

1. Boot your device into bootloader if not already there, and plug it to your computer
1. Lock critical partitition with the following command
   - `fastboot flashing lock_critical`
   - Approve with  <kbd>Voume +</kbd> then <kbd>power</kbd>
1. Reboot again your device into bootloader, and plug it to your computer
1. Lock the device with the following command
   - `fastboot flashing lock`
   - Approve with  <kbd>Voume +</kbd> then <kbd>power</kbd>

{% include alerts/success.html content= "Congratulations !! Your phone should now be booting into /e/OS !! "%}



