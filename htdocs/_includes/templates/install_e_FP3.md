## Installing /e/ from the bootloader

{% include alerts/tip.html content="Some of the commands have a -S after them. Please note it is a hyphen '-' followed by a capital 'S' and the value 522239K "%}

1. Unzip the archive

    ```
    unzip fileyoudownloaded
    ```

   > Replace `fileyoudownloaded` with the name of the archive file you downloaded . You can unzip the files into the folder where you copied the fastboot files.
   
   > Alternately you can create a folder and unzip the archive there. 
     
   >  In the second case the below commands will need the path to the folder where you unzipped the archive.
1. Install /e/ using the following commands (from fastboot mode)

    ```
    fastboot -w
    fastboot flash system_a system.img -S 522239K
    fastboot flash boot_a boot.img
    fastboot flash vendor_a vendor.img -S 522239K
    fastboot flash dtbo_a dtbo.img
    fastboot flash vbmeta_a vbmeta.img

    fastboot flash system_b system.img -S 522239K
    fastboot flash boot_b boot.img
    fastboot flash vendor_b vendor.img -S 522239K
    fastboot flash dtbo_b dtbo.img
    fastboot flash vbmeta_b vbmeta.img

    fastboot flashing lock
    ```

    - The last command will ask you to confirm on the device to lock the bootloader.


    - Select **LOCK THE BOOTLOADER** with `Volume +` and valid with `Power`.


    - On previous version of `fastboot`, the last command is `fastboot oem lock`

1. Enjoy! The device will now reboot automatically on /e/.

### Getting a `fastboot` error ?

-  On some versions of `fastboot` you may get an error while running the `Fastboot flash boot boot.img` command

- The below error may show up on the console

    ```
    fastboot error: couldn’t parse partition size ‘0x’
    ```

- A workaround for this error is as under. Run this command on the console:

    ```
    fastboot getvar current-slot
    ```
- This would show the current active slot. For e.g. if it shows `a` , run the below command

    ```
    fastboot --set-active=b
    fastboot flash boot_b boot.img
    ```

{% include alerts/tip.html content= "If you find an issue with this installation document or if you want to suggest improvements, Please create a dedicated thread on our  [community forum](https://community.e.foundation/) "%}

{% include alerts/success.html content= "Congratulations !! Your phone should now be booting into /e/OS !! "%}