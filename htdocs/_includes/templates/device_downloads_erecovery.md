## Downloads for the {{ device.codename }}

- [/e/ Recovery for dev build](https://images.ecloud.global/dev/{{ device.codename }})
{% if device.build_version_dev %}
- /e/OS build : [{{ device.build_version_dev }} dev](https://images.ecloud.global/dev/{{ device.codename }})
{%- endif %}

{% if device.build_version_stable %}
- /e/OS build : [{{ device.build_version_stable }} stable](https://images.ecloud.global/stable/{{ device.codename }})
{%- endif %}

> To understand the difference between /e/OS builds check [this guide](/build-status)
