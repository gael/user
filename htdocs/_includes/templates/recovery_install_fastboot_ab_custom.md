{%- assign device = page.device -%}
{% if device.custom_recovery_codename %}
{% assign custom_recovery_codename = device.custom_recovery_codename %}
{% else %}
{% assign custom_recovery_codename = device.codename %}
{% endif %}

## Temporarily Booting a custom recovery using `fastboot`

1. Download a custom recovery - you can download one [here]({{ device.custom_recovery }}). Download the file named `vendor_boot.img` from the directory named with the latest date.

1. Connect your device to your PC via USB.

1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type:
    ```  
    adb reboot bootloader
    ```  

1. Once the device is in fastboot mode, verify your PC finds it by typing:
    ```
    fastboot devices
    ```   

{% if device.recovery_install_command %}
1. Flash a recovery image onto your device
    ```   
    {{ device.recovery_install_command }} recoveryfilename.img
    ```  
    > {% translate content.recovery_file_name_text %} 

1. Manually reboot into recovery mode
   
  * {% t devices.with_the_device_powered_off %}
  * {% t device.recovery_boot %}
      
{% else %}
1. Temporarily flash a recovery on your device by typing:
    ```    
    fastboot flash recovery recoveryfilename.img
    ```
    on some devices the below command may be required
    ```
    fastboot boot recoveryfilename.img
    ```
    or
    ```  
    fastboot flash boot recoveryfilename.img
    ```

> {% translate content.recovery_file_name_text %}    

1. Manually reboot into recovery mode
  * {% t devices.with_the_device_powered_off %}
  * {% t device.recovery_boot %}

{% endif %}

