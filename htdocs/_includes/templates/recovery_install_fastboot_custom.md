## Unlocking the bootloader
{% if device.vendor == "LG" %}
   {% include alerts/danger.html content="Please note LG has stopped officially supporting the unlocking of its bootloader. For more details please refer this [document](https://www.xda-developers.com/lg-bootloader-unlocking-shut-down-december-31/)" %}
{% endif %}
{% include templates/unlock_bootloader.md %}

{% if device.unlock_bootloader_lg %}

{% include templates/unlock_bootloader_lg.md %}

{% else %}

1. Unlock your bootloader by following [this]({{ device.unlock_bootloader_guide }}) guide.

{% endif %}

{% include templates/recovery_install_fastboot_generic.md %}
