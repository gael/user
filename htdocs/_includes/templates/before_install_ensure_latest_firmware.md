## Updating firmware

1. Ensure you have downloaded the latest {{ device.build_version_dev }} firmware for the {{ device.codename }}

1. Now we will sideload the downloaded firmware file

{% include templates/sideload.md %}
