- MiUI Unlock tool [link](https://en.miui.com/unlock/download_en.html)

- [MiUI Flash Tool](https://xiaomiflashtool.com/)
{% if device.stockupgrade_toolver %}
    The version we tested was {{ device.stockupgrade_toolver }}
{% endif %}

{%  include alerts/tip.html content="Xiaomi users when reverting to the stock MiUi ROM use the Mi Flash Tool. This requires a windows PC. Linux users can try an alternate method to flash the stock ROM. They can unzip the ROM file and run the flash-all.sh file from a console screen. " %}
