{% capture tip_content %}
To suggest changes to this install guide documentation, please visit the {{ device.name }} specific [topic on our community forum]({{device.community_doc_url}}). 
To report issues in the build please refer [this guide]({% tl support-topics/report-an-issue %})
{% endcapture %}

{% include alerts/tip.html content=tip_content %}
