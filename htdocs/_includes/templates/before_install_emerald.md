## Unlock the bootloader

{% include alerts/danger.html content="Please take a backup of your data as unlocking the bootloader will reset your device to factory defaults. "%}
- Enable `USB debugging` and `Allow OEM Unlock` following the guide given [here](/pages/enable-usb-debugging)
- Reboot in fastboot

    - automatically: from your computer, run

         ```shell
adb reboot bootloader
         ```

    **or**

    - manually:

         Turn off your phone and start it with `power` + `volume up` keys.

         On the menu presenting three options, press `volume up` until fastboot is selected, and press `volume down` to accept this choice
- OEM unlock the device

    Once your phone is in fastboot mode, on your PC type in a console the below command
    ```shell
fastboot flashing unlock
    ```
    and press `volume down` on your phone to accept

- The device will reboot automatically after an unlock.
