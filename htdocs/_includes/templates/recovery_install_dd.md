{%- assign device = page.device -%}

{% if device.vendor == "LG" %}
   {% include alerts/danger.html content="Please note LG has stopped officially supporting the unlocking of its bootloader. For more details please refer this [document](https://www.xda-developers.com/lg-bootloader-unlocking-shut-down-december-31/)" %}
{% endif %}
{% if device.custom_recovery_codename %}
{% assign custom_recovery_codename = device.custom_recovery_codename %}
{% else %}
{% assign custom_recovery_codename = device.codename %}
{% endif %}


{% if device.required_bootloader %}

{% capture bootloader_versions -%}
    `{% for bootloader in device.required_bootloader -%}
        {{ bootloader }}{% unless forloop.last %} / {% endunless %}
    {%- endfor %}`
{%- endcapture %}

{% capture content %}
    Your device must be on bootloader version: {{ bootloader_versions }} otherwise the instructions found in this page will not work. The current bootloader version can be checked by running the command `getprop ro.bootloader` in a terminal app or an adb shell from a command prompt (on Windows) or terminal (on Linux or macOS) window.
{% endcapture %}

{% include alerts/danger.html content=content %}

{% endif %}
## Rooting your device

> Important: The device must be rooted before proceeding any further.

{% case device.root_method[0] %}
{% when 'custom' %}
1. Root your device by following [this]({{ device.root_method[1] }}) guide.
{% when 'kingroot' %}
1. Download KingRoot from [here](https://kingroot.net/).
   1. Install and run the apk to achieve root. Ensure you have a working Internet connection.
{% when 'towelroot' %}
1. Download TowelRoot from [here](https://towelroot.com/).
   1. Click the large lambda symbol to download the apk.
   1. Install and run the apk to achieve root.
{% endcase %}

{% if device.downgrade_bootloader %}

## Downgrading bootloader


In order to be able to boot a custom recovery and LineageOS you need to downgrade your bootloader.

   1. Download the relevant files for your device variant from here.
   1. Place aboot.bin and laf.bin on the root of /sdcard:
      - Using adb:
        ```shell
        adb push aboot.bin /sdcard/aboot.bin
        adb push laf.bin /sdcard/laf.bin
        ```

   1. Now, open an adb shell from a command prompt (on Windows) or terminal (on Linux or macOS) window. 
      - In that shell, type the following commands:

        ```shell
        su
        dd if=/sdcard/aboot.bin of=/dev/block/platform/msm_sdcc.1/by-name/aboot
        dd if=/sdcard/laf.bin of=/dev/block/platform/msm_sdcc.1/by-name/laf
        exit
        exit
        ```

{% include alerts/warning.html content = "Do NOT shut down or reboot your device now, because you will not be able to boot again until you finish these instructions!"  %}


{% endif %}

## Installing a custom recovery using `dd`

> Warning: Installing a custom recovery will erase all data on your device! Before proceeding, ensure the data you would like to retain is backed up to your PC and/or a online drive.

1. Place the recovery image file on the root of /sdcard (USB debugging have to be enabled in developer options) :

    ```shell
    adb push recoveryfilename.img /sdcard/recoveryfilename.img
    ```
2. Now, open an `adb shell` from a command prompt (on Windows) or terminal (on Linux or macOS) window. In that shell, type the following commands:

    ```shell
    su
    dd if=/sdcard/recoveryfilename.img of=/dev/block/platform/msm_sdcc.1/by-name/recovery
    ```

    {% translate content.device_install %}
3. Manually reboot into recovery
   With the device powered off, hold `Volume Down` + `Power`. Release all buttons and press `Power` again as soon as the LG logo appears.

4. Accept the factory reset prompt using the hardware buttons. If you have done everything correctly, this will not actually reset your device but instead will install the custom recovery.
