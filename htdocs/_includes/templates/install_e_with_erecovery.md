# Steps to install /e/OS from recovery

   {%  include alerts/tip.html content="Use the volume keys to navigate and power key to select" %}

   {% include alerts/tip.html content="If your PC is unable to detect your device in adb ..in the /e/ recovery main screen tap `Advanced` >> `Enable adb`" %}

On /e/ Recovery Main Screen:
1. Select  `Factory reset`
1. Select `Format data / Factory reset` option
1. Next screen will display a warning that this action cannot be undone
1. Select `Format data` to proceed or `Cancel` if you want to go back
1. If you selected `Format data` ..the format process will complete

   {% include alerts/tip.html content="You will see text in small font on the lower left side of the screen mentioning format progress"%}

   > The small font text would be similar to this

	```
    Wiping data
	Formatting/data
	Formatting/cache
	Data wipe complete
	```

1. Display will now return to the Factory Reset screen

   {% include alerts/tip.html content="Use the volume up key to choose the arrow at the top to go back to main screen" %}

   In /e/ Recovery main screen:
1. Select `Apply Update` and in next screen `Apply update from adb`
1. In the next screen, the device is now in sideload mode

    > Note at this point the Cancel option is highlighted that does not mean you have canceled the action. The
   device is in adb sideload mode.

1. On your PC type begin adb sideload. Type the below command in a console

   `adb sideload downloaded_file_name.zip`

   > Replace downloaded_file_name.zip with the name of the /e/ OS file you downloaded in the previous section

1. Press `enter` key on the keyboard to start the sideloading
   The screen will show the progress percentage...This might pause at 47%
1. Give it some time
1. The PC console will now display `Total xfer: 1.00x`
1. The phone screen will now display some text with a message similar to
   > `Script succeeded result was [1.000000]`

   This means that the install was successful
1. Use the `volume key` to select the `back arrow` on the top left of the phone screen
1. Go back to the main screen and select `Reboot system now`
    > The reboot process may take 5 - 10 minutes

{% include alerts/success.html content= "Congratulations !! Your phone should now be booting into /e/OS !! "%}
