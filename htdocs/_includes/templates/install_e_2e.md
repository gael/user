## Installing /e/ from the bootloader

1. Download the build package linked in the `Downloads` section above and extract it to a folder of your choice
2. Connect the device to your PC if not already done
> Ensure you have a cable which allowed file transfer. Some cables can only be used for charging.

3. Move to the directory where you have extracted the files in the first step. To do that type the below command in a console
```bash
cd path-of-the-extracted-files
```
4. Install /e/OS by typing the following commands in the console
```bash
fastboot -w
fastboot flash boot boot.img
fastboot flash recovery recovery.img
fastboot flash vbmeta vbmeta.img
fastboot flash vbmeta_system vbmeta_system.img
fastboot flash vbmeta_vendor vbmeta_vendor.img
fastboot reboot fastboot
fastboot flash system system.img
fastboot flash product product.img
fastboot flash vendor vendor.img
fastboot reboot
```

{% include alerts/tip.html content="If your device is not detect by fastboot, try to use `sudo fastboot` instead"%}
