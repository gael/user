{% if device.vendor == "HTC" %}
   {% include alerts/danger.html content="Please note HTC has stopped officially supporting the unlocking of its bootloader. For more details please refer this [document](https://www.htcdev.com/process)" %}
{% endif %}
{% include alerts/tip.html content="The steps given below only need to be run once per device." %}

{% if device.vendor != "Sony" %}
{% include alerts/warning.html content="Unlocking the bootloader will erase all data on your device! Before proceeding, ensure the data you would like to retain is backed up to your PC and/or an online drive" %}
{% endif %}
