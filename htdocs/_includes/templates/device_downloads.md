## Downloads for the {{ device.codename }}

[//]: # ( Recovery related )

{% if device.custom_recovery %}

- [Custom recovery]({{ device.custom_recovery }}) (not required if you already have it running on your phone)
{% else %}
{% if device.display_erecovery != false %}
Try the /e/ Recovery (**limited functionality**)
{% if device.build_version_dev %}
-  [/e/ Recovery](https://images.ecloud.global/dev/{{ device.codename }}) for dev build
{% endif %}
{% if device.build_version_stable and device.codename != "FP4" %}
- [/e/ Recovery for stable build](https://images.ecloud.global/stable/{{ device.codename }})
{% endif %}
{% endif %}
{% endif %}

[//]: # ( Patches related )


{% if device.additional_downloads.additional_patch %}
Additional Patches :
Flash this [patch]({{ device.additional_downloads.additional_patch }}) on your {{ device.codename }} before flashing /e/OS
{% endif %}

{% if device.additional_downloads.latest_firmware %}
- Latest [firmware zip file]({{device.additional_downloads.latest_firmware}})
{% endif %}

[//]: # ( Build type related )

{% assign security_patch = '' %}
{% if device.security_patch_date %}
    {% assign security_patch = '(Security patch: ' | append: device.security_patch_date | append: ')' %}
{%- endif %}

{% if device.build_version_dev %}
- /e/OS build : [{{ device.build_version_dev }} dev](https://images.ecloud.global/dev/{{ device.codename }}) {{ security_patch }}
{%- endif %}


{% if device.build_version_stable %}
- /e/OS build : [{{ device.build_version_stable }} stable](https://images.ecloud.global/stable/{{ device.codename }}) {{ security_patch }}
{%- endif %}

> To understand the difference between /e/OS builds check [this guide](/build-status)

[//]: # ( Before Install related )


{% if device.before_e_install %}
- Check the **Pre-Install Instructions** section below for some additional downloads for your device.
{% endif  %}

{% if device.additional_downloads.file %}
{% capture additional_downloads %}templates/additional_downloads_{{ device.additional_downloads.file }}.md{% endcapture %}
{% include {{ additional_downloads }} %}
{% endif %}

{% if device.additional_downloads.stockupgrade %}
- Stock ROM : [here]({{ device.additional_downloads.stockupgrade }})
{% if device.additional_downloads.stockupgrade_tool %}
- Stock flashing tool : [here]({{ device.additional_downloads.stockupgrade_tool }})
{% if device.additional_downloads.stockupgrade_toolver %}
- Tool version tested {{ device.additional_downloads.stockupgrade_toolver }}
{% endif %}
{% endif %}

> - Follow the steps to install the stock ROM as given on the vendor site
> - Once the Stock ROM is flashed check if it is working for e.g receiving and making calls, audio, FMRadio ..
> - Next enable developer options on the phone
> - Ensure the device bootloader is unlocked. Flashing stock ROM can lock the bootloader on some devices.
> - Follow the device unlock guidelines as suggested by your vendor. For e.g. on Xiaomi devices it would require running the Mi Unlock tool. Since the device was already unlocked the process should take a couple of minutes only.

{% endif %}

{% include alerts/warning.html content="Please note some of the above links can lead to external sites" %}
