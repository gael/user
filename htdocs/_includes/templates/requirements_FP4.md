{% include templates/requirements.md %}

{% include alerts/danger.html content="Ensure that you flash an image with a security patch level greater than the level of the existing security patch. Failure to do so will prevent you from locking your bootloader. " %}
{% include alerts/tip.html content="On your phone you can check the security patch level here `Settings` >> `About Phone` >> `Android Version` >>  `Android security patch level`. Check the level against the level of the security patch on the /e/OS build as given in the `Downloads for the FP4` section below " %}
