---
layout: page
permalink: /pages/unauthorized-device
search: exclude
title: Fix unauthorized device error in adb
---

{% tf pages/unauthorized-device.md %}
