---
layout: page
permalink: /pages/insufficient-permissions
search: exclude
title: Fix Insufficient permissions error in Linux
---

{% tf pages/insufficient-permissions.md %}
