## Reporta tu experiencia!

Creemos que /e/ se convertirá en el tercer sistema operativo móvil principal en el futuro. ¡Puedes formar parte de la historia! Probablemente estemos viviendo la parte más emocionante de la aventura con su inicio: unas cuantas personas juntas -desarrolladores y usuarios- dando forma a una revolución.

Como tú eres parte de esa revolución, nos gustaría conta tu historia con /e/ y no sólo la nuestra.

Habla de ti y de tu experiencia: escribir un simple mensaje en Twitter, Mastodont y otras redes sociales, muestra cómo se ve, por qué es importante para ti. También puedes abrir una cuenta en Medium para hablar escribir un pequeño artículo con ilustraciones. Publica algunos vídeos en [peertube](https://joinpeertube.org/en/) (o incluso en youtube...).

## Y vamos a transmitir!

Nos encantaria que nos cuentes tu experiencia positiva, y transmitiremos todas las mejores historias sobre /e/ en las redes sociales, en nuestros boletines... Sólo tienes que enviarnos un mensaje mencionando a [@gael_duval](https://twitter.com/gael_duval) para las publicaciones en Twitter o a [@gael@mastodon.social](https://mastodon.social/@gael) para las publicaciones en Mastodon, o escribenos para avisarnos a <contact@e.email>

Muchas gracias por su soporte!
