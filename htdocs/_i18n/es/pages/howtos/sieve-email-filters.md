## Objectivo del documento
Explicar cómo usar filtros para organizar automáticamente los correos entrantes en base a una serie de condiciones.

## Cómo configurar los filtros

Abre [la aplicación de correo en el cloud](https://ecloud.global/apps/rainloop/); haz clic en el icono de persona de la esquina superior derecha y luego en "Configuración".

![ajustes de correo](/images/howtos/ecloud/rainloop_email_settings.png)

Haz clic en "Filtros" en la barra lateral izquierda, luego en "Añadir un Filtro".
Dale un nombre al filtro, luego aprieta el botón "Añadir condición". Aquí puedes escoger entre diferentes condiciones como el origen, destino o asunto de un correo.
Finalmente, en la sección de"Acciones", elige qué quieres que pase con estos correos y dale al botón "Hecho". No te olvides luego de "Guardar" para que los cambios que acabase de hacer sean almacenados en el servidor.

He aquí un ejemplo de un filtro que mueve todos los correos provenientes de "gitlab@e.email" a una carpeta llamada Gitlab:

![crear un filtro](/images/howtos/ecloud/create_a_filter.png)

## Cómo usarlo con "Ocultar mi e-mail"
Abre la aplicación de correo en el cloud, haz clic en el icono de ajustes (engranaje) que hay abajo a la izquierda.

![ajustes de correo](/images/howtos/ecloud/email_settings.png)

Pincha en "Crear carpeta" e introduce un nombre, opcionalmente una ubicación. Pulsa en "Crear" y, finalmente, "Atrás" (en la parte superior).

![crear carpeta](/images/howtos/ecloud/create_email_folder.png)

Ahora vamos a descubrir nuestra dirección de "Ocultar mi e-mail". Pincha en el icono de tu cuenta en el cloud, esquina superior derecha, luego "Ajustes".

![Ajustes de cuenta](/images/howtos/ecloud/account_settings.png)

Pulsa en "Ocultar mi e-mail" en la barra lateral izquirda. Pincha en "COPIAR".

![copiar alias](/images/howtos/ecloud/copy_hide_my_email.png)

Ahora vuelve a la aplicación de correo, icono de persona y "Configuración".

![ajustes de correo](/images/howtos/ecloud/rainloop_email_settings.png)

Haz clic en "Filtros" a la izquierda, luego "Añadir un filtro". Dale un nombre. En las Condiciones elige "Destinatarios contiene" y _pega_ (ctrl+v o cmd+v) tu alias de "Ocultar mi e-mail". En Acciones, elige "Mover" y la carpeta que hayas creado para estos correos.

![crear filtro OMC](/images/howtos/ecloud/create_hme_filter.png)

Clic en "Hecho", luego "Guardar" y "Atrás". A partir de ahora, los correos enviados a tu dirección "Ocultar mi e-mail" serán recogidos en la carpeta elegida, fuera de tu bandeja de entrada principal.
