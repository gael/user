## Objetivo del documento

Explicar cómo usar el alias de correo `murena.io` disponible para todas las cuentas de ecloud `e.email`.

## Direcciones de correo murena.io

Los usuarios de ecloud tienen ahora la opción de usar el dominio murena.io para sus comunicaciones de correo electrónico. Por ejemplo, una usuaria llamada "jane.doe" puede ahora usar tanto `jane.doe@e.email` como `jane.doe@murena.io` para escribir y recibir correos. Puedes mandar e-mails con este alias, de forma que tus contactos verán los correos escritos desde murena.io. Puedes incluso establecer esta dirección como predeterminada en distintas aplicaciones, pero no olvides que tu identificador de usuario sigue siendo el correo acabado en `@e.email`.

## Cómo configurar el alias en ecloud

Abre la aplicación de correo en ecloud y pincha en el botón de ajustes (engranaje) que hay abajo a la izquierda.

![ajustes de correo](/images/howtos/ecloud/email_settings.png)

Haz clic en "Cuentas". Bajo la sección de Identitades verás tanto tu correo e.email como el de murena.io. Puedes reordenarlos usando el icono de 6 puntos. La primera de las direcciones será la predeterminada cuando inicies un nuevo hilo de correo (conversaciones existentes usarán la dirección que se usó originalmente).

![identidades](/images/howtos/ecloud/rainloop_identities.png)

Ahora dale a "Atrás" y "Nuevo". Verás tu nueva identidad en el campo "De". Si pinchas en ella, puedes elegir otra de tus direcciones. A cada nuevo correo se elegirá la dirección predeterminada.

## Cómo configurarlo en el correo

Abre la aplicación de "Mail" en /e/OS. Dale al menú de 3 líneas de la esquina superior izquierda, "configuración". Bajo "Cuentas", dale a tu dirección de correo.

![ajustes de correo](/images/howtos/ecloud/phone_email_settings.png)

Ahora haz clic en "Enviando correo", luego en "Gestión de identidades". Pincha en el icono de 3 puntos de la esquina superior derecha, luego "Nueva identidad".
Entra tu nombre y tu nueva dirección de correo murena.io. Finalmente, "Guardar".

![editar identidad](/images/howtos/ecloud/phone_email_edit_identity.png)

Ahora en la pantalla de "Gestionar identidades", mantén pulsada la nueva identidad que has creado para que aparezca un menú contextual donde elegirás "Mover arriba / hacer predeterminada". La que esté primera se usará para cada nuevo correo que escribas.

![identidad predeterminada](/images/howtos/ecloud/phone_make_default_identity.png)

Pincha "Atrás" varias veces hasta que llegues a tu bandeja de entrada. Ahora, cuando escribas un nuevo correo (icono del lápiz), verás tu nueva identidad murena.io en el campo "De". Si haces clic en ella, puedes elegir la anterior sólo para ese correo.

## Cómo configurar en un cliente de correo de PC

Es importante tener en cuenta que se puede usar la dirección murena.io como alias de correo **pero no es aún un ID de usuario**.
En tu cliente de correo, sigue siendo necesario proporcionar la dirección `@e.email` como usuario junto con la contraseña.
En principio, la configuración del servidor debería ser detectada automáticamente. Introduce la contraseña y guarda.
En caso que fuera necesario, [aquí](https://e.foundation/email-configuration/) tienes los parámetros para una configuración manual.

![cliente_email_1](/images/howtos/ecloud/email_client_1.png)

Una vez tu correo e.email esté configurado como de costumbre, abre la preferencias de la cuenta.

![propiedades correo](/images/howtos/ecloud/evolution_email_properties.png)

En "aliases", añade tu alias murena.io y haz clic en OK.

![aliases](/images/howtos/ecloud/evolution_aliases.png)

Ahora deberías poder elegir qué dirección usar cada vez que escribes un correo. También hay una opción para elegir cuál es la identidad por defecto. 

## Consejos

Puedes establecer un [filtro de correo](/es/support-topics/email-filters) para organizar los correos recibidos en cada dirección en carpetas distintas.
