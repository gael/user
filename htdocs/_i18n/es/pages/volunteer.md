## Quieres convertirte en voluntario y ayudar a /e/:

 - ¿Quieres presentar /e/ al mundo?
 - Apoyar a /e/ en eventos locales en torno a la privacidad de los usuarios y los datos
 - Organizar un evento de instalación en tu localidad
 - o incluso presentar tus ideas para llegar a más personas en todo el mundo.

Si tienes ganas de chequear una o varias de las opciones anteriores, pónte en contacto con nosotros.

## ¿Cómo solicitar?

[Contáctenos](https://e.foundation/contact/)