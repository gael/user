## Development status of microG

microG development is ongoing. We discuss with its maintainer regularly and also support the microG project financially. 
Improvements and updates are going on. Recently FCM support was added and improved.
The contact-tracing API has been added too (it is not enabled by default on /e/OS though, see below).
There are more updates in the pipeline. 
The project is very complex and would greatly benefit if more users contribute to it.

## How to use contact tracing

### Nota del equipo de desarrollo de /e/OS
- La aplicación de rastreo de contactos no es necesaria para el funcionamiento de /e/OS y puede ser eliminada fácilmente por los usuarios a su discreción.
- /e/OS no almacena ningún dato generado por la aplicación o la API de notificaciones de exposición
- Esta función se proporciona a los usuarios de /e/OS que deseen utilizar una aplicación de seguimiento de contratos especificada por el Gobierno de Covid-19.

### Objetivo de esta guía
La API de notificaciones de exposición es necesaria para que el teléfono inteligente interactúe con las aplicaciones de seguimiento covid-19 utilizadas en su país.
- La API está desactivada por defecto
- La aplicación requiere la intervención directa del usuario para ser activada.
  Esto incluye
  - La instalación de la API
  - Instalar las aplicaciones específicas de su país
  - Activar la API cuando se le solicite
Este documento compartirá los pasos necesarios para activar el marco en el sistema operativo /e/ OS


{% include alerts/warning.html content="No todas las aplicaciones de rastreo covid requieren la API de notificaciones de exposición para funcionar. Por ejemplo, la aplicación TousAntiCovi de Francia no requiere la API de notificaciones de exposición. Compruebe que la app específica de su país funciona en el /e/OS, antes de habilitar la API" %}

### ¿Cómo funciona el rastreo de contactos de Covid-19?

El seguimiento de los contactos de COVID-19 se realiza a través de la API de notificaciones de exposición. La API tiene las cuatro características siguientes

- Publicidad por Bluetooth : Su teléfono emite un Identificador de Proximidad Rodante (RPI) por Bluetooth. Cada diez minutos se publica un nuevo RPI, para que sea imposible rastrearle. Los RPI se derivan de una clave privada que se genera aleatoriamente una vez al día por su dispositivo. Esta derivación garantiza criptográficamente que los RPIs no pueden vincularse entre sí, excepto si usted compartió la clave privada.
- Escaneo de Bluetooth : Cada 5 minutos aproximadamente su teléfono busca dispositivos Bluetooth cercanos que emitan RPIs y almacena el RPI junto con una marca de tiempo en una base de datos local.
- Compartición de la clave : Cuando ha dado positivo en la prueba de COVID-19, puede pedir a su teléfono que comparta sus claves privadas de los últimos 14 días con la aplicación de rastreo de contactos de COVID-19. Sólo con esta clave privada es posible que otros vean si han estado en contacto con usted durante más de 10 minutos. La implementación de microG se asegurará de que este intercambio de claves no se produzca sin el consentimiento explícito del usuario.
- Cálculo del riesgo : A partir de la lista de claves privadas compartidas por la aplicación complementaria y de la lista de RPI recopilada en los últimos 14 días, su teléfono comprueba si ha estado en contacto estrecho con una persona que supuestamente ha dado positivo. El teléfono calcula una puntuación total de riesgo y proporciona información sobre la fecha y la hora, la duración del contacto y la intensidad de la señal de la exposición a la aplicación complementaria. La aplicación complementaria no sabrá qué llaves o personas estuvieron en contacto estrecho, ni obtendrá ninguna información sobre el lugar donde se produjo el contacto.

microG ofrece la única implementación de código abierto de esta API.

### Pasos para instalar la API de notificaciones de exposición en /e/OS

1. Descargue la última versión de [microG ES](https://gitlab.e.foundation/e/apps/GmsCore/-/releases) 

     - Las descargas para ambas versiones (`/dev` y `/stable`) están disponibles en esta ubicación. Comprueba tu versión y descarga el apk correspondiente

1. Instalarlo manualmente
1. Compruebe regularmente si hay actualizaciones y, si están disponibles, aplíquelas manualmente


### Configuración de una aplicación para trabajar con microG ES

Aquí utilizaremos el ejemplo de la aplicación Covid Radar (de España)
 - Tras la instalación de la aplicación, pulse para abrirla en el teléfono

     ![](/images/Covid_3.png)
 - Esto mostrará una ventana emergente pidiendo permisos para
   - Habilitar el Bluetooth
   - Habilitar la activación de la API de notificaciones de exposición
 - Elige `ok` para ambas ventanas emergentes
 - Ahora tu aplicación, que en nuestro ejemplo era Covid Radar, empezará a funcionar.
 - La API de notificaciones de exposición también se activará en esta fase, como puede verse en esta captura de pantalla

     ![](/images/Covid_7.png)

### ¿Cómo desactivar la aplicación o el framework?

Puede desactivar fácilmente el rastreo de contactos de COVID-19 desde su /e/ OS. Todo lo que necesita hacer es..
- Desinstalar la aplicación de rastreo de contactos que descargó
- Desactivar la API de Notificaciones de Exposición desde `Configuración` > `Sistema` > `microG` > `Notificaciones de Exposición`

Además, también es posible desinstalar la versión de microG Notificaciones de Exposición. Para ello, entra en "Ajustes" > "Aplicaciones y notificaciones" > "Servicios centrales de microG", y luego toca el botón de los tres puntos en la parte superior derecha y elige "Desinstalar actualizaciones".

### Cómo lidiar con las notificaciones de exposición a microG y las actualizaciones de /e/OS

El proyecto /e/ proporciona dos paquetes de microG, con o sin Notificaciones de Exposición(ES).
Cuando una nueva versión del paquete de microG está disponible en una versión de /e/OS, el proceso de actualización actualizará
la versión anterior con la que viene por defecto, es decir, `microG` sin ES. Si usted es un usuario de `microG ES`, tiene que
tiene que actualizar el paquete `microG ES` antes de actualizar /e/OS. Se puede hacer automáticamente
por la aplicación Apps o manualmente si ha desactivado las actualizaciones automáticas. Una vez hecho, puede actualizar /e/OS.
Al reiniciar, su configuración de microG estará intacta y actualizada.
 
