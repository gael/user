## Samsung-Smartphones
Für Samsung-Smartphones gibt es ein Werkzeug namens „wdi-simple.exe“, das mit dem Easy-Installer geliefert wird. Es wird während dem Installationsprozess aufgerufen und installiert die Treiber, die zum Betrieb mit Heimdall nötig sind.

Für Neugierige: dieses Werkzeug ist unsere Version des [libwdi Projekts](https://github.com/pbatard/libwdi). 
