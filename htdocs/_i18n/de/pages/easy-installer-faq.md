## What does the Easy Installer do?

The Easy Installer is a software that will guide you as you flash /e/ on your phone all by yourself. Unfortunately, you can't just plug in your phone and wait until the process is done. There is always some manual action required. The Easy Installer will tell you what and when you have to do something. For example: pressing one or several physical buttons, taping on some buttons, enabling options, rebooting your phone, etc.

It will also perform commands in the background that you would otherwise have to run yourself. 

Within a shell...(you know, this scary black screen window where geeks write strange things :computer:)

<img src="../images/Shell2.png" width="400" alt="Linux shell prompt example">

In addition, it will download source files and check their integrity. 

Please do not rush, and read all the instructions carefully. They aren't perfect yet, and are still being improved.

## My phone isn't detected

###  **Windows users**:  YOU NEED drivers for your phone.
This is a complicated subject, but you have to know that there isn't one single driver that works for everything.

A driver is a piece of software that allows your Windows computer to communicate with a plugged phone. There are drivers for your screen, mouse, keyboard, printer, gamepad, smartphone, etc. Unfortunately, some drivers are hard to find and don't work with all smartphone modes. 

{% tf pages/easy-installer-install-drivers.md %}


### The phone asks me to allow USB debugging. What should I do?
The Easy Installer communicates with your phone thanks to a tool called ADB (Android Debug Bridge). When you use it for the first time, on a specific computer, a prompt message is displayed on your phone. 

<img src="../images/Easy-installer-rsaFingerprint.png" width="300" alt="Example of RSA fingerprint message">

If you're on a trusted computer, you should also tick the checkbox to always allow connections between the computer and the device. If you do not tick the checkbox, you will have to accept it again next time.
You must accept this message by clicking on the "Allow" button. 

####  I cancelled the message on my phone 
If you clicked on the cancel button, then your phone can't communicate with your PC.
Just unplug then replug your phone, the message will show up again.


### Try another cable
You should try to use a different USB cable. Only use a cable that allows data transfer.
Some cables are only meant for battery charging.
USB cables are the source of many issues, because they're often not made to last.

### Try another computer
If any of the above solutions worked, you should try with another computer


## Downloading of sources failed immediatly after the beginning
[Check that "images" server is available](https://status.ecloud.global).
If server is unavailable, please try again later.

## Downloading of sources failed after a long period
Click on the restart button displayed, then the downloading will restart from where it stopped.
The sources server has a timeout set to 1 hours. Consequently, if your connection is slow, it might stop before the end. 

## Instructions and illustrations don't correspond to my phone
Pictures are here to help but there aren't the main content. If you must choose, please follow the text intructions.

You'll find  help from [the community forum](https://community.e.foundation/c/community/easy-installer/136)

Help to improve instruction by making suggestion, translating or providing screenshots of your phone would be appreciate a lot!

## I want to select dev or stable builds
At the moment, you can only get the stable build, but
this is definitively a feature we'd like to add. 


## Ich verstehe kein Englisch, ich will den Easy-Installer in meiner Sprache
Der Easy-Installer benutzt die Sprache deines Computers. Wenn deine Sprache nicht unterstützt wird, 
wird Englisch als Standard benutzt.

Easy-installer supports (partially sometimes) the following languages:
- English
- French
- German
- Dutch
- Russian
- Basque

## GS290 flashing does not finalize
We don't have all the card in hand yet, but we're investigating into this issue to bring a solution as soon as possible.
Please share logs with us, and join [this discussion](https://community.e.foundation/t/gs290-easy-installer-stops/27917)  on our community forum.

## "OEM unlock" option is missing in settings of my Samsung phone
The Samsung process to enable to activate "oem unlock" option in developer settings is quite complicated
because they want to block the bootloader. Consequently, you won't find official documentation about it.

However, you can find many links on internet on how to enable
the "OEM unlock" option.

Community members also suggested:
- https://www.xda-developers.com/fix-missing-oem-unlock-samsung-galaxy-s9-samsung-galaxy-s8-samsung-galaxy-note-8/
- https://androidmore.com/fix-missing-oem-unlock-toggle-samsung/


There are already several discussions on our forum about this:
- https://community.e.foundation/t/i-do-not-have-the-option-for-unlock-oem-in-developer-options/20222/2
- https://community.e.foundation/t/galaxy-s8-oem-unlock/28305
- https://community.e.foundation/t/unclear-about-instructions-unlock-oem/20875/9


You might also want to do the following:
- Connect your phone to Wi-fi
- Update your device to the latest version of the original OS

## Die Installation von „TWRP“ (recovery) schlägt fehl
Bei der Installation auf Samsung-Telefonen muss ein spezieller Schritt eingelegt werden. Nachdem das Wiederherstellungssystem (recovery) installiert worden ist, muss du den Heruntelade-Modus verlassen (indem du „Ausschalten“ + „Lautstärke leiser“ gedrückt hältst). Sobald der Bildschirm schwarz wird (wie beim Ausschalten), musst du sofort in den Wiederherstellungsmodus (recovery) neu starten (indem du „Ausschalten“ + „Lautstärke lauter“ + „Bixby/Startmenü“ drückst und hältst). Das ist nicht ganz einfach, aber möglich, z.B. indem man einfach den Finger auf der Lautstärketaste verschiebt.

**Bemerkung:** Wir wissen, dass noch keine Warnung angezeigt wird, wir werden dies in einer zukünftigen Version hinzufügen. 

Du kannst mehr Informationen im Gemeinschaftsforum finden:

- https://community.e.foundation/t/e-on-samsung-galaxy-s8-cant-reboot-into-twrp/21696
- https://community.e.foundation/t/e-easy-installer-samsung-galaxy-s7-no-teamwin-help-please/27887/3


Wenn es immer noch blockiert ist, und du einen Windows-Computer hast, kannst du versuchen, TWRP (Das Wiederherstellungssystem/recovery) zu installieren, indem du die [ODIN Werkzeuge](https://samsungodin.com/) anstatt Heimdall benutzt. Wenn dann TWRP installiert ist, kannst du den Easy-Installer neu starten und den Vorgang abschließen.

## Der Entsperr-Code der Fairphone-Webseite funktioniert nicht
Auf deinem Fairphone muss die neuste Version des Fairphone OS laufen, wenn der OEM-Entsperrcode funktionieren soll.
Wenn du trotzdem dein Fairphone nicht entsperren kannst, nimm bitte [direkt Kontakt mit Fairphone](https://www.fairphone.com/de/uber/kontakt/) auf. 


## Auf welchen Betriebssystemen habt ihr den Easy-Installer getestet?
Wir haben den Easy-installer für Ubuntu (20.04) und Windows (10) entwickelt.
Er sollte ohne Probleme auf Windows 7, 8, 10, Ubuntu, ElementaryOS und Linux Mint laufen.


## Ich will den Easy-Installer auf einem Windows-Computer ohne Administratorrechten installieren
Es ist möglich, ihn zu installieren, aber es gibt keine Verknüpfung in dem Nicht-Administrator-Konto.

Kennst du dich gut mit NSIS (Ein Werkzeug, um Windows Installer zu erstellen) aus? Wir freuen uns über Mithilfe bei <diesem Problem>
## Ich will die Logdatei sehen
Der Easy-Installer erzeugt eine Logdatei mit einem Zufalls-generiertem Namen (wie z.B. 45761d08-711f-435a-881d-a236949654bb.log), die nützliche Informationen enthalten kann, wenn etwas nicht so läuft wie es sollte.

Wo die Logdatei gefunden werden kann:

- **Linux:** ~/snap/easy-installer/common
- **Windows:** %LOCALAPPDATA%\easy-installer

Die Logdatei ist zu groß, um direkt in das Forum hochgeladen zu werden. Wenn du ein Problem melden willst, kopiere den Inhalt der Logdatei nach https://haste.tchncs.de/ . Dann klicke auf das Speichern-Symbol, welches dann einen Link erzeugt, denn du im Forum posten kannst.

Falls die Logdatei sogar zu groß für hastebin ist, und du schon ein /e/-Konto hast, kannst du die Datei mit ZIP komprimieren und in dein /e/-Cloudspeicher hochladen.

## My question is not listed here
If you don't find an answer to your question, a solution to your issue, please contact the [community support](https://t.me/joinchat/D1jNSmJHKo4u70ha).
You can also check on the [community forum](https://community.e.foundation/c/community/easy-installer/) or directly ask for help there.

## Ich finde keine Antwort auf meine Frage
Wenn du keine Antwort auf deine Frage findest und keine Lösung für dein Problem, kontaktiere bitte den [Gemeinschafts-Support](https://t.me/joinchat/GNc9FU0qB5YluUQOrQf94A).
Du kannst auch im [Gemeinschaftsforum](https://community.e.foundation/c/community/easy-installer/) nach Hilfe fragen.
