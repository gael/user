## Purpose of the document

This document explains how to use murena.io email alias for your email account.

## murena.io email adresses

Ecloud users now have the choice to use murena.io domain for emails. For example, a user called "jane.doe" is now able to use both `jane.doe@e.email` and `jane.doe@murena.io` email adresses. You can send emails with this alias, so others will see received emails coming from your murena.io alias. You can use it as your default email address for sending emails, but don't forget that your account name is still the one with e.email

## How to configure it on ecloud

Open email app in the cloud, click on settings button at the bottom.

![email settings](/images/howtos/ecloud/email_settings.png)

Click on "Accounts". Under identities section you will see both your e.email and murena.io identities. You can move your murena.io identity above e.email identity using the icon with 6 dots. The one on the top will be used by default when you are writing a new email.

![identities](/images/howtos/ecloud/rainloop_identities.png)

Now, click back, then new message. You will see your new identity in the "from" section. If you click on it, you can change it to the old one. For the next email, you will again see the default identity.

## How to configure it on the phone

Open "Mail" app. Click on the 3 lines icon on top left, then "Settings". Under Accounts, click on your email address.

![email settings](/images/howtos/ecloud/phone_email_settings.png)

Now click on "Sending mail", then on "Manage identities". Click on the icon with 3 dots on the top right, then on "New identity".
Enter your name and your new email address. Now click "save"

![edit identity](/images/howtos/ecloud/phone_email_edit_identity.png)

Now on the "Manage identities" screen, press and hold your finger over the new identity you created, then choose "Move to top / make default". The one on top will be used each time you write a new email.

![make it default identity](/images/howtos/ecloud/phone_make_default_identity.png)

Click "back" multiple times until you are back in your Inbox. Now each time you write an email (you can click pen icon) , you will see your new identity in "from" section. If you click on it, you can change it to the old one temporarily.

## How to configure in a desktop email app

Please note that you can use murena.io as an email address **but not as a user ID**.
In your email client, you still need to identify with your `@e.email` user. 
Normally your settings should be detected automatically. Enter your password and save it if you don't want to be asked each time. Settings for manual configuration are available [here](https://e.foundation/email-configuration/).

![email_client_1](/images/howtos/ecloud/email_client_1.png)

Once your email is configured in the app as usual, open email properties.

![email properties](/images/howtos/ecloud/evolution_email_properties.png)

In "aliases" add your murena.io alias. Then click OK.

![aliases](/images/howtos/ecloud/evolution_aliases.png)

Now you should be able to manually choose which email address to use in "from" field each time. In your email app, you should be able to set the default "from" address to use by adding a new identity, and choosing to use it by default. 

## Tips

You can also use an [email filter](/support-topics/email-filters) to sort your emails sent to both addresses in different folders.
