## Purpose of the document

Diese Anleitung erklärt, wie Apps in dem ecloud.global-Konto verwaltet werden, wie unbenötigte Apps deaktiviert und für dich nützliche Apps aktiviert werden.
Es werden auch einige der häufige gestellten Fragen über verfügbare Apps auf ecloud.global abgehandelt.

## Häufig gestellte Fragen zu verfügbaren Apps

### Welche Apps sind derzeit auf ecloud.global verfügbar?
   - Dateien
   - E-Mail (Rainloop)
   - Kontakte
   - Kalender
   - Fotos
   - Notizen (Nextcloud)
   - Aufgaben
   - Aktivität
   - Carnet (alternative Notizen-App)
   - Deck (Projektplaner)
   - Lesezeichen
   - News (RSS-Leser)

### Warum sind nicht alle verfügbaren Apps aufgelistet?
Aus verschieden Gründen:
 - Alle Apps müssen überprüft und gegebenenfalls angepasst werden. Manche funktionieren nicht ohne Anpassung oder erfordern externe Komponenten.
 - Wenn wir sie aktivieren, engagieren wir uns, die App auch in Zukunft verfügbar zu halten, und Aktualisierungen und Unterstützung für Benutzer zu Verfügung zu stellen.
 - Vor der Aktivierung für jeden Benutzer müssen wir die Sicherheit und die Leistungsfähigkeit der App sicherstellen.
 - Eventuell könnte die Benutzeroberfläche und die Benutzerfreundlichkeit der Cloud darunter leiden.

Letztes Jahr haben wir [eine Umfrage durchgeführt](https://community.e.foundation/t/adding-new-apps-at-ecloud-global/12404/69). In der Folge haben wir _Carnet_, _Deck_, _Lesezeichen_ und _News_ eingeführt.\
_Karten_ und _Formulare_ benötigen noch etwas Anpassung, bevor sie veröffentlicht werden können.

### Könnt ihr die App XYZ aktivieren?

Ab und zu werden wir ähnlich Umfragen durchführen. Du kannst ein neues Thema im [Gemeinschafts-Forum](https://community.e.foundation/) eröffnen, und sehen, ob andere Benutzer die gewünschte App auch als nützlich erachten. Wir werden beliebte Themen beachten und den Vorschlag überprüfen.

### Haben Premium-Unterstützer Zugang zu mehr oder besseren Apps?

Zurzeit noch nicht, aber das kann sich in Zukunft ändern. Z.B. kann es sein, dass wir Zugang beschränken für Apps, die höhere Hardware-Anforderungen haben (wie Musik-Streaming), oder die ein Lizenz-Limit für die Anzahl an Benutzern fordern.

## App-Anordnung anpassen

Du kannst ganz einfach die Anordnung der Apps ändern, indem du sie an den neuen Platz ziehst. Siehe folgendes Video:

{% include components/video.html
    poster_image="/images/howtos/ecloud/reordering_applications.png"
    name="Reordering applications"
    mp4="/images/howtos/ecloud/reordering_applications.mp4"
    webm="/images/howtos/ecloud/reordering_applications.webm"
%}
