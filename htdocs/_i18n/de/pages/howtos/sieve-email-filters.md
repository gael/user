## Zweck des Dokuments
Die Verwendung von Filtern zur Festlegung von Regeln für die automatische Organisation eingehender E-Mails wird erläutert.

## Konfiguration

Öffnen Sie [die E-Mail-App in der Cloud] (https://ecloud.global/apps/rainloop/), klicken Sie auf das Personensymbol oben rechts und dann auf "Einstellungen".

![E-Mail Einstellungen](/images/howtos/ecloud/rainloop_email_settings.png)

Klicken Sie auf der linken Seite auf "Filter" und dann auf "Filter hinzufügen".
Geben Sie dem Filter einen Namen und klicken Sie dann auf "Bedingung hinzufügen". Hier können Sie zwischen verschiedenen Bedingungen wählen, z. B. E-Mails von oder an, die eine bestimmte Aktion auslösen sollen.
Wählen Sie dann unter "Aktionen" aus, was mit diesen E-Mails geschehen soll, und klicken Sie auf "Fertig". Vergessen Sie nicht, auf "Speichern" zu klicken, damit die Änderungen auf dem Server gespeichert werden.

Hier ein Beispiel mit einem Filter, der alle E-Mails, die von gitlab@e.email kommen, in den Ordner Gitlab verschiebt:

![Filter erstellen](/images/howtos/ecloud/create_a_filter.png)

## Verwendung mit Hide my email
Öffnen Sie die E-Mail-App in der Cloud und klicken Sie unten auf "Einstellungen".

![E-Mail Einstellungen](/images/howtos/ecloud/email_settings.png)

Klicken Sie auf "Ordner erstellen" und geben Sie dem Ordner einen Namen. Klicken Sie auf "Erstellen". Klicken Sie dann auf die Schaltfläche "Zurück".

![Email-Ordner erstellen](/images/howtos/ecloud/create_email_folder.png)

Lassen Sie uns nun Ihre "Meine E-Mail verbergen"-Adresse herausfinden. Klicken Sie auf das Symbol Ihres Hauptkontos oben rechts. Dann Einstellungen.

![Kontoeinstellungen](/images/howtos/ecloud/account_settings.png)

Klicken Sie auf der linken Seite auf "Meine E-Mail verbergen". Klicken Sie dann auf "Kopieren", um Ihren Alias zu kopieren.

![Kopie meine E-Mail verbergen](/images/howtos/ecloud/copy_hide_my_email.png)

Gehen Sie nun zurück zur E-Mail-App, klicken Sie oben rechts auf das Personen-Icon und dann auf "Einstellungen".

![E-Mail Einstellungen](/images/howtos/ecloud/rainloop_email_settings.png)

Klicken Sie auf der linken Seite auf "Filter", dann auf "Filter hinzufügen". Geben Sie dem Filter einen Namen, wählen Sie bei den Bedingungen "Empfänger", "enthält" und fügen Sie "Meine E-Mail-Adresse ausblenden" ein. Unter "Aktionen" wählen Sie "Verschieben nach" und den Ordner, den Sie für diese E-Mails erstellt haben.

![hme Filter erstellen](/images/howtos/ecloud/create_hme_filter.png)

Klicken Sie auf "Fertig" und dann auf "Speichern". Klicken Sie auf "Zurück". Jetzt werden E-Mails, die an Ihre "Hide my email"-Adresse gesendet wurden, in dem von Ihnen erstellten Ordner gesammelt.
