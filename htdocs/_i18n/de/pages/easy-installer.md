## Was ist der /e/OS easy-installer?

- Der Easy Installer ist eine Desktop-Anwendung, die **Nutzern hilft, /e/ zu installieren**, auf unterstützen Geräten.

- die aktuelle Version dieses Assistenten funktioniert unter **Linux** und **Windows OS**

![](/images/easy-installer-mainscreen.png)

## Wie kann ich den /e/OS easy-installer installieren?

Die Easy-Installer Beta-Version ist verfügbar für Linux und Windows OS.

[Installationsanleitung für Linux](easy-installer-linux)

[Installationsanleitung für Windows](easy-installer-windows)
## Liste der vom Easy-Installer unterstützen Geräten

- Die Beta-Version des Easy-Installers unterstützt **10 Geräte**

{% include alerts/tip.html content="Damit der Easy-Installer richtig auf dem PC funktioniert, müssen zuerst die Betriebssystem-spezifischen Fastboot-Treiber installiert werden. Folge dazu den Installationsanleitungen des jeweiligen Gerätes." %}


| **Firma** | **Gerätename**                     | **Code Name** | **Zusätzliche Info**   |**Treiberinstallation**                                          | **Model(le)**         |
| ---------- | ----------------------------------- | ------------ | --------------------- | --------------------------------------------------------------- | --------------------- |
| Gigaset    | [GS290](devices/GS290)              | gs290        |                       |[Installationsanleitung](pages/install_easy_installer/win_gs290)          | gs290                 |
| Fairphone  | [Fairphone 3](devices/FP3)          | FP3          |                       |[Installationsanleitung](pages/install_easy_installer/win_fp3)            | FP3                   |
| Fairphone  | [Fairphone 3+](devices/FP3)         | FP3+         |                       |[Installationsanleitung](pages/install_easy_installer/win_fp3)            | FP3+                  |
| Samsung    | [Galaxy S9](devices/starlte)        | starlte      | Exynos only           |[Installationsanleitung](pages/install_easy_installer/win_samsung_phones) | SM-G960F, SM-G960F/DS |
| Samsung    | [Galaxy S9 Plus](devices/star2lte)  | star2lte     | Exynos only           |[Installationsanleitung](pages/install_easy_installer/win_samsung_phones) | SM-G965F, SM-G965F/DS |
| Samsung    | [Galaxy S8](devices/dreamlte)       | dreamlte     | Exynos only           |[Installationsanleitung](pages/install_easy_installer/win_samsung_phones) | SM-G950F              |
| Samsung    | [Galaxy S8 Plus](devices/dream2lte) | dream2lte    | Exynos only           |[Installationsanleitung](pages/install_easy_installer/win_samsung_phones) | SM-G955F              |
| Samsung    | [Galaxy S7](devices/herolte)        | herolte      | Exynos only           |[Installationsanleitung](pages/install_easy_installer/win_samsung_phones) | SM-G930F              |
| Samsung    | [Galaxy S7 Edge](devices/hero2lte)  | hero2lte     | Exynos only           |[Installationsanleitung](pages/install_easy_installer/win_samsung_phones) | SM-G935F              |
  
>Für zusätzliche Details kann [dieser Foreneintrag](https://community.e.foundation/t/list-devices-working-with-the-easy-installer/28396), der von Benutzern aktualisiert wird, im [Forum](https://community.e.foundation) besucht werden. Dort werden Erfahrungen ausgetauscht von /e/-OS-Benutzern zum Easy Installer für die unterstützen Geräte.
## Warum werden nicht mehr Geräte oder Betriebssysteme unterstützt?

Aufgrund Personalmangels kann das /e/-Team nicht die ganze Arbeit bewältigen, um mehr Geräte oder andere Betriebssysteme hinzuzufügen. Darum bitten wir Benutzer mit Programmiererfahrung, uns bei folgenden Punkten zu helfen:
- Den Easy-Installer nach MacOS zu portieren
   - die Java-Software soll ohne Probleme laufen
   - das Skript zur Portierung benötigt noch etwas Arbeit
- Unterstützung neuer Geräte
   - mit den Architektur-basierten Konfigurationsdateien ist es einfach, neue Geräte hinzuzufügen
  
{% include alerts/tip.html content="In [diesem Dokument](easy-installer-contribute) teilen wir mehr Einzelheiten mit, wie man andere Geräte hinzufügen kann"%}




## Ich möchte die Quelldatei (source) weiterentwickeln. Wie kann ich helfen?

Um den Easy-Installer Programmiercode zu bauen oder verbessern, lies bitte [diese Anleitung …](easy-installer-contribute)


{% include alerts/tip.html content="Für weitere Diskussionen zum Easy-Installer, für Hilfe oder Mitarbeit, komme [zum Gemeinschaftsforum](https://community.e.foundation/c/community/easy-installer/136)" %}

## Noch mehr Fragen zum Easy-Installer?
Schau in die [Easy-Installer FAQ](easy-installer-faq) rein!

**Vielen Dank im Voraus!**
