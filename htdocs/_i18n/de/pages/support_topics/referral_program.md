# Ecloud Freunde-werben Programm

Get up to 40€ of credits for your cloud storage by inviting your friends to ecloud!

For every friend who joins and creates an ecloud account, you will earn 2€ to be used for
cloud storage on ecloud.

ecloud is your personal email account, your agenda and contacts, your drive on the cloud and
your online office suite, all combined in one single service, simple to use. ecloud is powered by
proven open-source software like NextCloud and OnlyOffice.

Your ecloud account is free up to 1GB of storage. Our paid plans start at 1,99€ per month for
20GB.

## How does this work?

Once you have a valid e.email account, it is very easy.

All you have to do is sign up at [https://murena.com/my-account/](https://murena.com/my-account/), our ecommerce platform, so we can assign your cloud credits.

In your murena.com account space, locate the section ‘Referral’ and check your unique and
anonymous link.

You can copy your unique link easily and use it elsewhere, for example, emailing it to your
friends or even sharing it in a tweet.

For every friend who uses this link to create a free account, you will receive 2€ of cloud
credits in your murena.com account.

## How do I invite my friends?

Es ist sehr einfach, du musst nur deinen eindeutigen Registrierungslink senden, der in deinem 
Murena.com-Konto zu finden ist. Der Link ist komplett anonym, er sammelt keine identifizierenden 
Informationen wie Name oder E-Mail-Adresse.

## How can I use the credits?

Your credits can be used once you subscribe to a extended storage plan. They will be
converted in discount towards your paid subscription.

## How long are my credits valid?

Your credits will be valid for 24 months.

## How long will this referral program last?

The ecloud referral program will be active for 2 months, from April 6th to June 6th, 2022.

## Can I redeem my credits against the purchase of a phone or something else at murena.com?

Your credits are only valid towards the subscription of paid cloud plan.

## How does it work for my friends?

Upon clicking on the referral link, your friend will be taken to the account invitation page where
she/he will be able to create a user ID and password.

Nach dem Erstellen des Kontos erhält dein Freund einen Link zu einem Ecloud-Konto, von dem er/sie auf das neu erstellte e.email-Konto, den Cloudspeicher oder die Only-Office-Suite zugreifen kann.

## How do I create an ecloud account?

Es ist sehr einfach. Besuche [https://murena.io/signup](https://murena.io/signup), um eine Einladung anzufordern. Du wirst nur in das Freunde-werben-Programm aufgenommen, wenn du dich auch unter [https://esolutions.shop/my-account/](https://esolutions.shop/my-account/) registrierst und deine Freunde-werben-Links benutzt.

## What is ecloud?

ecloud is a complete, fully “deGoogled”, online ecosystem.

Ecloud ist dein persönliches E-Mail-Konto mit der e.email-Domain, deinem Kalender und Kontakten, deinem Cloudspeicher und deiner Online-Office-Suite – alles in einem einzigen, einfach zu bedienenden Dienst. Ecloud wird mithilfe von bewährter Open-Source-Software wie NextCloud und OnlyOffice betrieben.

## Is the referring link anonymous ?

Der Link ist komplett anonym, er sammelt keine identifizierenden Informationen wie Name oder E-Mail-Adresse. Der Sender des Links kann nicht einsehen, wer diesen benutzt hat.

## Ich sehe meine Guthaben nicht im murena.com-Konto, was soll ich tun?

Kontaktiere uns unter [helpdesk@e.email](mailto:helpdesk@e.email), damit wir das Problem ermitteln und lösen können.
