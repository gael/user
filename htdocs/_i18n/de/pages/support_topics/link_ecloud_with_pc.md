
## Worum geht es hier
Diese Anleitung erklärt, wie man einen PC mit der eCloud verbinden kann.
## Warum verbinden? 

Die eCloud ist dein persönlicher Speicherort für Daten wie Dokumente, Musik und Bilddateien. 

Mit /e/OS und deiner /e/ ID ist es möglich, einfach Daten von deinem Smartphone zu deiner persönlichen eCloud zu übertragen. 

Wenn deine eCloud mit deinem PC verbunden ist, kannst du einfach auf deine Dokumente und Bilder sowohl von deinem PC wie von deinem Smartphone zugreifen. 


In dieser Anleitung zeigen wir dir, wie du deinen PC mit deiner eCloud verbinden kannst. 

Die Verbindungsmethode ist je nach Betriebssystems deines PCs unterschiedlich. Wir decken einige der bekanntesten Betriebssysteme ab.

## Wie man mit verschiedenen Betriebssystemen verbindet

[Ubuntu](/support-topics/link-ecloud-with-ubuntu)

[Windows](/support-topics/link-ecloud-with-windows)

Es werden noch mehr Betriebssysteme zu dieser Liste hinzugefügt werden


## Du hast noch keine eCloud oder /e/-Konto? 

Schau dir [diese Anleitung](/create-an-ecloud-account) an
