## What is TWRP
[TWRP](https://twrp.me/) is a custom recovery used for installing custom software on an android device. This custom software can include smaller modifications like rooting your device or even replacing the firmware of the device with a completely custom "ROM" like [/e/OS](https://e.foundation/)

## Get TWRP for your device
Check for the `TWRP` build for your device on [this page](https://twrp.me/Devices/)
- You would need your `devicecode` to get the correct TWRP image for your device.

## Understand the TWRP backups

TWRP makes `nandroid` backups, which are near-complete images of your system.
- You use nandroid backups to restore your phone to exactly the state it was in when you backed up: the version of Android, your wallpaper, your home screen, right down to which text messages you had left unread.

{% include alerts/tip.html content="Ensure you copy the backup on to an external storage device. Do not make the backup on the same device as in some cases the devices are completely formatted and this will wipe out your backup. "%}

## Flash TWRP on your device

For this document we will assume you have TWRP on your device. If not please refer [this document](install-twrp)

## Mache eine `nandroid`-Sicherung deines Geräts

- Boot into TWRP recovery
  To get in the recovery the Key combination may be different for each device. Refer the section on the particular [device info pages]({% tl devices %})  for boot modes
- For some devices it could be

  `Power` and `Volume Down` buttons simultaneously then use the volume keys to boot into `Recovery Mode`

- Sobald dein Gerät neu gestartet wurde, sollte – wie unten gezeigt – der TWRP-Hauptbildschirm angezeigt werden

![](/images/TWRP_Backup.png)
- In the next screen Tap the `Name` bar at the top to give the backup a recognizable name.
- Check the Boot, System, and Data boxes, and then swipe the bar along the bottom to back up.
  > Backups are fairly large, so if you get an error about there not being enough space, you may have to delete some things on your internal storage or SD card before continuing

![](/images/TWRP_Backup1.png)
- Tippe im nächsten Bildschirm oben die `Name`-Leiste, um deiner Sicherung einen wiedererkennbaren Namen zu geben.
- Hake die Boot-, System- und Daten-Kästchen ab und wische Leiste entlang des unteren Randes, um die Sicherung zu starten.
  > Sicherungen sind ziemlich groß; wenn du also eine Fehlermeldung über nicht ausreichenden Speicherplatz bekommst, musst du gegebenenfalls einige Dateien vom internen Speicher oder der SD-Karte löschen, bevor du fortfahren kannst

- The backup will take a few minutes to complete, so be patient.
- Once it is finished, you can tap `Back` to go back to TWRP’s main menu
- Alternately you can tap `Reboot System` to reboot back into Android

## Restore from a TWRP Backup

- Boot back into TWRP, and tap the `Restore` button on the home screen

-Starte erneut in TWRP und tippe auf die `Wiederherstellen`-Schaltfläche auf dem Startbildschirm

- TWRP will show you a list of your previous backups
- Tap the one you want and it will show you the following screen.

- TWRP zeigt dir eine Liste deiner bisherigen Sicherungen
- Tippe die Sicherung deiner Wahl an, und dir wird folgender Bildschirm angezeigt.

- Make sure all the boxes are checked and swipe the bar to restore.

- Stelle sicher, dass alle Kästchen abgehakt sind, und wische die Leiste zum Wiederherstellen.

- The restore will take a few minutes, but when it’s finished, you can reboot your phone back into Android.

## Empfohlene Lektüre
[Was sollte ich in TWRP sichern?](https://twrp.me/faq/whattobackup.html)
