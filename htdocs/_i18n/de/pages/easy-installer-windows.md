## Prerequisite
You must have drivers for your phone already installed
{% include alerts/tip.html content="To install drivers for your phone please refer the install guides for the device as given [here](easy-installer#list-of-devices-supported-by-the-easy-installer)" %}

## Installation steps for Windows OS

### Download

To download the Easy-installer beta exe for windows click [here](https://images.ecloud.global/easy-installer/Easy-installer-setup.exe )

- Save it to a folder on your Hard Drive

![](/images/win_setup_easyinstaller_download_1.png)

- Verify the [sha256sum](https://images.ecloud.global/easy-installer/Easy-installer-setup.exe.sha256sum)

### Execute

- You can execute the downloaded executable file by double clicking on the .exe


{% include alerts/warning.html content="Windows Defender Smartscreen may display the below popups and ask you to confirm the installation of Easy-installer-setup.exe" %}

![](/images/win_setup_easyinstaller_popup_1.png)

Klicke auf `Mehr Infos`
Das Fenster sieht jetzt so aus

![](/images/win_setup_easyinstaller_popup_2.png)

Click on `Run anyway`

On some computer you may get this pop up

![](/images/win_setup_easyinstaller_warning_1.jpg)


Click `Yes`

This would start the installation program and display a welcome screen like this

![](/images/win_setup_easyinstaller_3.1.png)

### Read and Agree to the License agreement

- Read the license agreement and click `I Agree` if you accept the conditions for installation.


![](/images/win_setup_easyinstaller_3.2.png)

### Choose a folder

- Choose where you want to install the application.

![](/images/win_setup_easyinstaller_3.3.png)

Standardmäßig wird unter Windows ein Ordner namens `easy-installer` in den Programmdateien auf dem Laufwerk C erstellt.

- You can select the default and click `Install` or choose a different folder

![](/images/win_setup_easyinstaller_3.3.1.png)

### Start the Installation

- You should see an image like this while the installation runs. It should take a few minutes to complete the installation.

- Do not switch off the PC or interrupt the installation process.

![](/images/win_setup_easyinstaller_3.4.png)

### Installation complete

- Once the installation is complete you will see a screen like this

![](/images/win_setup_easyinstaller_3.5.png)

### Where to find the Easy-Installer program

- You can find the Easy-Installer in your home menu list

![](/images/windows_easy_installer_icon.png)

- Click on it to open and try out our Easy Installer !!

### Troubleshooting

- If you have an anti-virus on your windows PC in some cases it could block the Easy-Installer from working. In such a scenario you may have to manually allow Easy-Installer through the Anti-virus settings


- When flashing a **Samsung** device, you may see this popup. Click on `Yes`

![](/images/win_setup_easyinstaller_warning_2.jpg)

wdi-simple is a driver installation tool. The source code is available  [at libwdi project](https://github.com/pbatard/libwdi)


