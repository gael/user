## Einrichtungsschritte für Linux

### Über die Kommandozeile

> Eventuell musst du ein `sudo` vor die Befehle stellen. Das hängt von deiner Konfiguration ab.

- Öffne eine Konsole und tippe den unten stehenden Befehl, um das Easy-Installer Snap Paket zu installieren

    ```shell
 snap install easy-installer --channel=latest/beta
 ```

### Der einfachste Weg!!
> Wir benutzen hier die Ubuntu Anwendungsverwaltung

  - Öffne die Ubuntu Anwendungsverwaltung. Es sollte ein Fenster wie dieses zu sehen sein
![](/images/ubuntu-software.png)

  - Klicke auf die „Alle“-Ordner oder klicke auf das Suchsymbol
 - Tippe „easy-installer“. Daraufhin sollte der /e/OS Easy-Installer wie unten angezeigt werden.

![](/images/easy-installer_search.png)

 - Klicke auf die „Installieren“-Schaltfläche, um die Installation zu starten
 ![](images/easy-installer_2.png)
 -
 - Vielleicht musst du das Administrator-Passwort eingeben, um die Installation abzuschließen. Das hängt von deiner Konfiguration ab
 - Nach der abgeschlossenen Installation wirst du so ein Fenster sehen  

 ![](images/easy-installer_4.png)

 - Klicke auf die Berechtigungen-Schaltfläche, um die Berechtigungen anzusehen, die der Easy-Installer für den Betrieb braucht

 ![](images/easy-installer_5.png)

{% include snippets/easy_installer.html
   title="Möchtest du mehr über diese Berechtigungen wissen? "
   details=site.data.easy-installer.list
 %}

![](/images/easyinstaller-in-apps.png)

- Nach dem Abschluss der Installation solle ein neues Symbol auf dem Schreibtisch zu sehen sein, wie auf dem Bildschirmfoto oben gezeigt

- Klicke auf das Symbol und probier den Easy-Installer aus!!

### Anleitung zum Installieren auf verschiedenen Distributionen mit Snap
Um den Easy-Installer als Snap auf verschieden anderen Distributionen zu installieren, folge [dieser Anleitung](https://snapcraft.io/easy-installer)
