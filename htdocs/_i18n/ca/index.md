## Sobre el projecte

> Aprèn més sobre el projecte /e/

- [Què és /e/? Llegeix-ne la descripció!]({% translate_link what-s-e %})
- [Instal·la el sistema operatiu /e/ en un dispositiu compatible!]({% translate_link devices %})
- [Crea un compte gratuït a ecloud, el núvol /e/]({% translate_link create-an-ecloud-account %})
- [Com-es-fa]({% translate_link support-topics %})
- [Preguntes més freqüents]({% translate_link support-topics %})
- [Comparteix la teva /e/xperiència!]({% translate_link testimonies %})


## Contribueix a /e/

> Vols ajudar el projecte?

- [Projectes que cerquen col·laboració]({% translate_link projects-looking-for-contributors %})
- [Ofertes de feina a /e/]({% translate_link jobs %})
- [Responsables de ROM]({% translate_link rom-maintainer %})
- [Traductors]({% translate_link translators %})
- [Provadors]({% translate_link testers %})
- [Voluntaris]({% translate_link volunteers %})

## Més informació

> Informació sobre el projecte i les seves aplicacions

- [Instal·lador fàcil /e/ OS]({% translate_link easy-installer %})
- [Aplicació de Mapes]({% translate_link maps %})
- [Els diferents tipus de build]({% translate_link build-status %})
- [PMF de l'instal·lador d'aplicacions]({% translate_link apps %})
- [Nota legal i de privacitat de la e Foundation](https://e.foundation/legal-notice-privacy/)

## Dreceres

> Enllaços a les pàgines web de /e/, fòrums de discussió i canals de Telegram

- [La web principal de /e/](https://e.foundation/)
- [Codi font d'/e/](https://gitlab.e.foundation/e)
- [Necessites ajuda?](https://e.foundation/contact/)
- [Informa d'un error al sistema /e/ o a les aplicacions](https://gitlab.e.foundation/e/backlog/-/issues/new)
- [Demana que el teu telèfon sigui suportat](https://community.e.foundation/c/e-devices/request-a-device)
