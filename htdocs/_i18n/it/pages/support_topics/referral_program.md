# Programma di affiliazione Ecloud

Ricevi fino a 40€ di crediti per il tuo cloud storage invitando i tuoi amici su ecloud!

Per ogni amico che si iscrive e crea un account ecloud, guadagnerai 2€ da utilizzare per
il cloud storage su ecloud.

ecloud è il tuo account di posta elettronica personale, la tua agenda e i tuoi contatti, il tuo
disco sul cloud e la tua suite office online, il tutto riunito in un unico servizio, semplice da usare.
ecloud si basa su software open-source di comprovata efficacia come NextCloud e OnlyOffice.

Il tuo account ecloud è gratuito fino a 1 GB di spazio di archiviazione. I nostri piani a 
pagamento partono da 1,99€ al mese per 20GB.

## Come funziona?

Una volta che si dispone di un account e.email valido, è molto semplice.

Tutto ciò che devi fare è registrarti su [https://murena.com/my-account/](https://murena.com/my-account/), la nostra piattaforma di ecommerce, in modo che possiamo assegnarti i crediti cloud.

Nello spazio del tuo account murena.com, individua la sezione " Affiliazione" e consulta il
tuo link personale e anonimo.

È possibile copiare il proprio link personale e utilizzarlo altrove, ad esempio inviandolo
via e-mail agli amici o condividendolo in un tweet.

Per ogni amico che utilizza il link per creare un account gratuito, riceverai 2€ di crediti
cloud per il tuo account murena.com.

## Come invito i miei amici?

È molto semplice, basta inviare il link di registrazione personalizzato che si trova nel proprio
account murena.com. Il link è completamente anonimo, non raccoglie informazioni
identificative come nome o indirizzo e-mail.

## Come utilizzo il credito?

I credito può essere usato al momento di sottoscrivere un piano di archiviazione
avanzato. Sarà convertito in sconto per l'abbonamento a pagamento.

## Che validità avrà il mio credito?

Il credito ottenuto sarà valido per 24 mesi.

## Quanto durerà il programma di affiliazione?

Il proramma di affiliazione resterà attivo per 2 mesi, dal 6 Aprile al 6 Giugno 2022.

## Poso usare il mio credito per l'acquisto di un telefono o altro su murena.com?

Il credito è valido solo per la sottoscrizione di un piano cloud a pagamento.

## Come funziona per i miei amici?

Cliccando sul link di riferimento, i tuoi amici sarano indirizzati alla pagina di richiesta account
dove potranno creare un ID utente e una password.

Al termine del processo di creazione dell'account, i tuoi amici riceveranno un link a un account ecloud da cui potranno accedere all'account e.email appena creato, all'archiviazione cloud o iniziare a utilizzare la suite Only Office.

## Come posso creare un account ecloud?

È molto semplice. Visita [https://murena.io/signup](https://murena.io/signup) per richiedere il tuo invito. Sarai incluso nel programma di affiliazione solo una volta che ti iscriverai anche tu a [https://murena.com/my-account/](https://murena.com/my-account/) e utilizzerai i tuoi link di affiliazione.

## Cos'è ecloud?

ecloud è un ecosistema online integrato, completamente “deGoogled”.

ecloud è composto dal tuo account di posta elettronica personalizzato sul dominio e.email, agenda e contatti, disco su cloud e dalla tua suite online Only Office, tutti combinati in un unico servizio, semplice da usare. ecloud si basa su software open-source di successo come NextCloud e OnlyOffice.

## Il link di affiliazione è anonyimo ?

Il link è completamente anonimo, non raccoglie informazioni personali come nome o
indirizzo e-mail. Chi invia il link non ha alcuna visibilità su chi abbia utilizzato il
link di affiliazione.

## Non vedo il mio credito nel mio spazio murena.com, come posso fare ?

Contattaci su [helpdesk@e.email](mailto:helpdesk@e.email) in modo che possiamo approfondire il problema e provare
a risolverlo.
