## Racconta la tua esperienza!

Pensiamo che /e/ diverrà in futuro il terzo SO mobile. Puoi diventare un protagonista della storia! Stiamo probabilmente vivendo la parte più eccitante dell' avventura dal suo inizio: poche persone insieme - sviluppatori & utenti - che danno forma ad una rivoluzione.

Dato che fai parte di questa rivoluzione, vorremmo raccontare la tua esperienza con /e/, non solo la nostra.

Raccontaci di te e della tua esperienza: con un semplice post su twitter, mastodon e altri social media, fai vedere com'è, perchè è importante per te. Potresti anche aprire un account su Medium per scrivere un piccolo articolo con immagini. O postare dei video su [peertube](https://joinpeertube.org/en/) (o anche youtube...)...

## E noi lo segnaleremo!

Ci piace leggere le vostre esperienze positive, e segnaleremo tutte le storie più belle su /e/ sui social media, sulle nostre newsletter... Basta citare [@gael_duval](https://twitter.com/gael_duval) nei post su Twitter o [@gael@mastodon.social](https://mastodon.social/@gael) nei post su Mastodon, o scrivervi per avvisarci all'indirizzo: <contact@e.email>

Grazie ancora per il tuo aiuto!
