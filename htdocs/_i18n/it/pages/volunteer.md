## Vuoi diventare un Volontario ed aiutare /e/:

 - Vorresti presentare /e/ al mondo
 - Aiutare /e/ agli eventi locali su utenti e privacy dei dati
 - Ospitare un party di installazione nella tua zona
 - o anche inviarci le tue idee su come raggiungere più persone di tutto il mondo.

Se ti sentiresti di spuntare una o più delle opzioni precedenti allora entra in contatto con noi.

## Come farlo?

[Contattaci](https://e.foundation/contact/)