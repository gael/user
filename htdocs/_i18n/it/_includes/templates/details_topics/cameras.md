
{% assign details_topics_titles = details_topics_titles | push: "FotoCamera(e)" %}

{% capture cameras_content %}

    {% if device.cameras.first %}
        No of Cameras: {{ device.cameras | size }}
        <ul>
            {% for el in device.cameras %}
                <li>
                    {{ el.info }},
                    {% if el.flash != '' %}
                        {{ el.flash }}
                    {% else %}
                        No
                    {% endif %} flash
                </li>
            {% endfor %}
        </ul>
    {% else %}
        {{ device.cameras }}
    {% endif %}
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: cameras_content %}