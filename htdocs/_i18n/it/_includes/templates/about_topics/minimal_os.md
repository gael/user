{%- if device.minimal_os %}
    {% assign subtitle = subtitle | append: "<br> (/e/ minima)" %}

    {% assign about_topics_titles = about_topics_titles | push: "Maggiori informazioni sulla versione minima di /e/ OS" %}

    {% capture minimal_os_content %}
        {{ device.codename }} ha un limite di spazio sul disco rigido. Per questo motivo il set completo di applicazioni predefinite di /e/ OS non può essere installato. Per aggirare questo problema abbiamo creato una versione minima del sistema operativo /e/. <br> Dalla versione minima sono state rimosse le seguenti app:
        <ul>
            <li>MagicEarth</li>
            <li>PdfViewer</li>
            <li>Weather</li>
            <li>LibreOfficeViewer</li>
        </ul>
    {% endcapture %}

    {% assign about_topics_contents = about_topics_contents | push: minimal_os_content %}
{%- endif %}