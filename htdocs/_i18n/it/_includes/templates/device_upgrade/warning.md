{% assign device = page.device %}

### Attenzione:

  > - Questa build di aggiornamento non ha potuto essere testata in quanto non erano disponibili tester specifici per il dispositivo.
 > - Alcuni dispositivi potrebbero aver bisogno di un ritorno alla ROM stock prima dell'aggiornamento.
 > - Per flashare la ROM stock è necessario utilizzare il tool fornito dal produttore.
 > - Potrebbe anche essere necessario scaricare un'immagine di recovery creata appositamente per il tuo dispositivo.
