## About the Project

> Discover more about project /e/

- [What's /e/? Read the product description!]({% translate_link what-s-e %})
- [Install /e/ mobile OS on a supported smartphone!]({% translate_link devices %})
- [Create a free ecloud account]({% translate_link create-an-ecloud-account %})
- [HOWTOs]({% translate_link support-topics %})
- [FAQ]({% translate_link support-topics %})
- [Share your /e/xperience!]({% translate_link testimonies %})


## Contribute to /e/

> Do you want to help the project?

- [Progetti in cerca di contributori]({% translate_link projects-looking-for-contributors %})
- [Offerte di lavoro in /e/]({% translate_link jobs %})
- [ROM Maintainer]({% translate_link rom-maintainer %})
- [Traduttori]({% translate_link translators %})
- [Tester]({% translate_link testers %})
- [Volontari]({% translate_link volunteers %})
- [Programma di caccia ai Bug]({% translate_link bug-bounty-program %})

## Altre Informazioni

> Informazioni sul progetto o le sue app

- [/e/ OS easy-installer]({% translate_link easy-installer %})
- [App Maps]({% translate_link maps %})
- [Differenti tipologie di build]({% translate_link build-status %})
- [FAQ Apps Installer]({% translate_link apps %})
- [e.foundation Legal Notice & Privacy](https://e.foundation/legal-notice-privacy/)

## Scorciatoie

> Link rapidi ai siti web di /e/, ai forum e ai canali telegram

- [Sito web di /e/](https://e.foundation/)
- [Codice Sorgente di /e/](https://gitlab.e.foundation/e)
- [Serve Aiuto? Assistenza?](https://e.foundation/contact/)
- [Segnala un bug nella ROM o nelle App di /e/](https://gitlab.e.foundation/e/backlog/-/issues/new)
- [Aggiungi il tuo Smartphone all'elenco di quelli supportati](https://community.e.foundation/c/e-devices/request-a-device)
