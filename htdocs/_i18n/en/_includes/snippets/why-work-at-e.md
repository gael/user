## Why work at /e/?

We are a **fully remote**, multicultural and international team. We enjoy working from home, from a nice _café_ or by the sea.
We have a common mission; we aim high, and we know it will take effort and time. If you are looking for a challenge… **come join us!**

## What we offer

- Fully remote position with flexible working hours.
- Exciting challenge: to become the 3rd mobile OS.
- Potential to impact a large and constantly growing user base.
{% if page.contract_type == 'CDI' -%}
- French CDI.
{%- elsif page.contract_type == 'CDI_contractor' -%}
- Salaried position or freelancer.
{%- elsif page.contract_type == 'contractor' -%}
- Freelancing contractor with perks: paid time off, training platform, hardware upgrades when needed.
{%- endif -%}
