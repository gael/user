{% assign about_topics_titles = about_topics_titles | push: "What's not (yet) working?" %}

{% capture whats_not_working_content %}
    {% if device.codename == 'FP3'%}
        Please find issues opened by users on this device <a href="https://gitlab.e.foundation/groups/e/-/boards/12?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=FP3%2FFP3%2B">here</a><br> 
        You can also track the progress of the issue here.
    {% else %}
        Please find issues opened by users on this device <a href="https://gitlab.e.foundation/groups/e/-/boards/12?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D={{device.codename}}">here</a><br> 
        You can also track the progress of the issue here.
    {% endif %}
{% endcapture %}

{% assign about_topics_contents = about_topics_contents | push: whats_not_working_content %}