{% for el in device.install %}

    {%- if el.mode == "Easy Installer" %}
        
        {% assign about_topics_titles = about_topics_titles | push: "Install with our easy-installer (for anyone)" %}

        {% capture install_with_easyinstaller %}
        
        Our easy installer is a software application compatible with Windows and Linux that will help install /e/OS on your phone following simple steps on your screen.
To install /e/OS on the {{ device.name }}- {{ device.codename }} via our easy installer, <a href="{{ "/easy-installer" | relative_url }}">click here</a>

        {% endcapture %}

        {% assign about_topics_contents = about_topics_contents | push: install_with_easyinstaller %}
    {%- endif %}

{% endfor %}
