{% assign about_topics_titles = about_topics_titles | push: "Install via command line (for advanced users)" %}

{% capture install_content %}
To install /e/OS on the {{ device.name }} - "{{ device.codename }}" <a href="{{ site.baseurl | append: "/devices/" | append: device.codename | append: "/install" }}"> click here</a>
{% endcapture %}

{% assign about_topics_contents = about_topics_contents | push: install_content %}