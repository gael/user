{% assign about_topics_titles = about_topics_titles | push: "Is your model not supported?" %}

{% capture model_not_supported_content %}
    Contact us to <a href="https://community.e.foundation/c/e-devices/request-a-device">Request support </a>for your device <br>
    The more the users requesting for a device / model the better the chances of it getting on the <a href="{% tl devices %}">officially supported list</a> !!!
{% endcapture %}

{% assign about_topics_contents = about_topics_contents | push: model_not_supported_content %}