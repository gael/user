{% assign about_topics_titles = about_topics_titles | push: "Build" %}

{% capture build_content %}
Want to build /e/OS for the {{ device.name }} - {{ device.codename }} ? <a href="{% translate_link support-topics/build-e %}"> Click here</a>
{% endcapture %}

{% assign about_topics_contents = about_topics_contents | push: build_content %}