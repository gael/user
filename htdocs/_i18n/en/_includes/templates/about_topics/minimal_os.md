{%- if device.minimal_os %}
    {% assign subtitle = subtitle | append: "<br> (/e/ minimal)" %}

    {% assign about_topics_titles = about_topics_titles | push: "Know more about the /e/ OS minimal version" %}

    {% capture minimal_os_content %}
        The {{ device.codename }} has a space limitation on the hard disk. Due to this the complete set of default /e/ OS apps cannot be installed. To get around this issue we have come out with a minimal OS version of /e/. <br> The following apps have been removed from the minimal version:
        <ul>
            <li>MagicEarth</li>
            <li>PdfViewer</li>
            <li>Weather</li>
            <li>LibreOfficeViewer</li>
        </ul>
    {% endcapture %}

    {% assign about_topics_contents = about_topics_contents | push: minimal_os_content %}
{%- endif %}