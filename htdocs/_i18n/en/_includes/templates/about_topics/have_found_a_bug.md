{% assign about_topics_titles = about_topics_titles | push: "Have you found a bug?" %}

{% capture found_a_bug_content %}
If you have found a bug please report it <a href="https://gitlab.e.foundation/e/management/issues/new">here</a>
<br>
Have some suggestions to improve the installation process? 
<br> Please send us an <a href="mailto:support@e.email?subject=Documentation Improvement">email here</a>
{% endcapture %}

{% assign about_topics_contents = about_topics_contents | push: found_a_bug_content %}