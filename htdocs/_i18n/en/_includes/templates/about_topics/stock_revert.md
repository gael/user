{% if device.codename == "GS290" %}
    {% assign about_topics_titles = about_topics_titles | push: "Would you like to roll-back to stock ROM ?" %}

    {% capture stock_revert_content %}
        To revert to the GS290 Stock ROM refer the document <a href="{{ "/pages/revert_e_GS290_windows.html" | relative_url }}">here</a>
    {% endcapture %}

    {% assign about_topics_contents = about_topics_contents | push: stock_revert_content %}

{% endif %}

{% if device.codename == "FP3" %}
    {% assign about_topics_titles = about_topics_titles | push: "Would you like to roll-back to stock ROM ?" %}

    {% capture stock_revert_content %}
        To revert to the FP3/FP3+ Stock ROM refer the document <a href="https://support.fairphone.com/hc/en-us/articles/360048050332-FP3-Install-Fairphone-OS-offline">here</a>
    {% endcapture %}

    {% assign about_topics_contents = about_topics_contents | push: stock_revert_content %}

{% endif %}
{% if device.codename == "2e" %}
    {% assign about_topics_titles = about_topics_titles | push: "Would you like to roll-back to stock ROM ?" %}

    {% capture stock_revert_content %}
        To revert to the 2e Stock ROM refer the document <a href="https://community.myteracube.com/t/advanced-installing-factory-android-image-w-sp-flash/1003">here</a>
    {% endcapture %}

    {% assign about_topics_contents = about_topics_contents | push: stock_revert_content %}

{% endif %}

{% if device.codename == "starlte" or device.codename == "star2lte" %}
    {% assign about_topics_titles = about_topics_titles | push: "Would you like to roll-back to stock ROM ?" %}

    {% capture stock_revert_content %}
        To revert to the {{ device.codename }} Stock ROM on Windows OS refer the document <a href="{{ "pages/revert_samsung_to_stock_on_windows" | relative_url }}">here</a>
    {% endcapture %}

    {% assign about_topics_contents = about_topics_contents | push: stock_revert_content %}

{% endif %}
{% if device.codename == "lake" %}
    {% assign about_topics_titles = about_topics_titles | push: "Would you like to roll-back to stock ROM ?" %}

    {% capture stock_revert_content %}
        To revert to the {{ device.codename }} Stock ROM on Windows OS refer the document <a href="{{ "pages/revert_motorola_to_stock_on_windows" | relative_url }}">here</a>
    {% endcapture %}

    {% assign about_topics_contents = about_topics_contents | push: stock_revert_content %}

{% endif %}

