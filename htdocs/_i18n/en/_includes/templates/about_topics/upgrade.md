{% if device.upgrade_available %}
    {% assign about_topics_titles = about_topics_titles | push: "Upgrade" %}

    {% capture upgrade_content %}
      
    To upgrade the {{ device.name }} - "{{ device.codename }}" to the {{ device.upgrade_available }} build of /e/OS <a href="{{ site.baseurl | append: "/devices/" | append: device.codename | append: "/upgrade" }}"> click here</a>
      
    {% endcapture %}

    {% assign about_topics_contents = about_topics_contents | push: upgrade_content %}

{% endif %}