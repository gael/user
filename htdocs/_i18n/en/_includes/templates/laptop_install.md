{% assign device = page.device %}

### /e/OS code sources for this laptop :

{% if device.gitlocation %}

You can download the /e/OS for PineBook [here]( {{ device.gitlocation }} )

{% endif %}

### Download the Build

Get the /e/ OS build to flash on your {{ device.type }} [here]({{ device.stockupgrade }})

### Installation guide

{% if device.install_e_method %}
{% capture install_e_method %}templates/install_e_{{ device.install_e_method }}.md{% endcapture %}
{% include {{ install_e_method }} %}
{% endif %}

Please note: In case you run  into issue with the installation steps or find any step missing please let us know by sending a mail to <support@e.email> and we will have the details added.
