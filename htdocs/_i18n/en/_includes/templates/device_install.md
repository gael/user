{% assign device = page.device %}

{% assign requirements_file_name     = "requirements" %}
{% assign downloads_file_name        = "device_downloads" %}
{% assign before_install_file_name   = "before_install" %}
{% assign oem_unlock_file_name       = "oem_unlock" %}
{% assign recovery_install_file_name = "recovery_install" %}
{% assign copy_partitions_file_name  = "copy_partitions" %}
{% assign install_e_file_name        = "install_e" %}
{% assign help_file_name             = "help" %}

{% if device.requirements_file_name %}
    {% assign requirements_file_name = device.requirements_file_name %}
{% endif %}

{% if device.downloads_file_name %}
    {% assign downloads_file_name = device.downloads_file_name %}
{% endif %}

{% if device.before_install_file_name %}
    {% assign before_install_file_name = device.before_install_file_name %}
{% endif %}

{% if device.oem_unlock_file_name %}
    {% assign oem_unlock_file_name = device.oem_unlock_file_name %}
{% endif %}

{% if device.recovery_install_file_name %}
    {% assign recovery_install_file_name = device.recovery_install_file_name %}
{% endif %}

{% if device.copy_partitions_file_name %}
    {% assign copy_partitions_file_name = device.copy_partitions_file_name %}
{% endif %}

{% if device.install_e_file_name %}
    {% assign install_e_file_name = device.install_e_file_name %}
{% endif %}

{% if device.help_file_name %}
    {% assign help_file_name = device.help_file_name %}
{% endif %}

{% capture requirements_template_path %}templates/{{ requirements_file_name }}.md{% endcapture %}
{% capture downloads_template_path %}templates/{{ downloads_file_name }}.md{% endcapture %}
{% capture before_install_template_path %}templates/{{ before_install_file_name }}.md{% endcapture %}
{% capture oem_unlock_template_path %}templates/{{ oem_unlock_file_name }}.md{% endcapture %}
{% capture recovery_install_template_path %}templates/{{ recovery_install_file_name }}.md{% endcapture %}
{% capture copy_partitions_template_path %}templates/{{ copy_partitions_file_name }}.md{% endcapture %}
{% capture install_e_template_path %}templates/{{ install_e_file_name }}.md{% endcapture %}
{% capture help_template_path %}templates/{{ help_file_name }}.md{% endcapture %}

{% include {{ requirements_template_path }} %}
{% include {{ downloads_template_path }} %}
{% include {{ before_install_template_path }} %}
{% include {{ oem_unlock_template_path }} %}
{% include {{ recovery_install_template_path }} %}
{% include {{ copy_partitions_template_path }} %}
{% include {{ install_e_template_path }} %}
{% include templates/maintainers.md %}
{% include {{ help_template_path }} %}
