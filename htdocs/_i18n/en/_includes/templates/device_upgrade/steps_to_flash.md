{% assign device = page.device %}

### Steps to Flash the Upgrade /e/OS ROM


1. Open a command window and change into the directory where you downloaded the files

1. Take a complete backup of your phone data
1. Ensure your {{ device.codename }} smartphone is fully charged
1. Remove any pattern or PIN locks you may have set on your phone
  > you can set these up after the OS has been successfully installed on your phone

1. If you already have TWRP recovery installed on your device
    - skip steps 6 and 7
    - Go directly into TWRP recovery (step 9)
    - To do that reboot your device and during startup follow the steps as given here
    - {% t devices.with_the_device_powered_off %}, {% t device.recovery_boot %}

1. To flash TWRP recovery
   - Reboot your device and press volume-down during startup.
   - This will bring the device into fastboot mode

1. Flash the TWRP image in the fastboot mode by running below command in the PC
    ```
    fastboot flash recovery twrpimagename.img
    ```
    on some devices the below command may be required
    ```
    fastboot boot twrpimagename.img
    ```
   > replace the twrpimagename.img with the name of the twrp image file name you downloaded previously
   > the boot command will flash a temporary TWRP image on your smartphone

    {% if device.twrp_version %}
    > The TWRP version we tested was {{ device.twrp_version }}

    {% endif %}

1. Reboot the device via the command
   ```
   fastboot reboot
   ```

   and press volume-up while booting to go into TWRP recovery

1. If your device is un-encrypted
   - Continue with installing the new build as given in step 13


1. If your device is encrypted
   - Ensure you have taken a backup as proposed in step 2
   - Continue to step 11

1. In TWRP wipe screen on your device click the format data button on the lower side to remove encryption

1. Select the Wipe button and in the wipe screen select system + data + cache and swipe to wipe

1. Sideload the /e/ .zip package

    - On the device, In TWRP select

     Advanced >> ADB Sideload >> then swipe to begin sideload

    - On the host machine, sideload the package by running the below command
```
	adb sideload /e/ OS image name.zip
```
     >replace the /e/ OS image name.zip with the name of the /e/ OS zip file name,
     e.g. adb sideload e-0.7-p-2020011437643-test-FP2.zip

1. Once the installation is successfully completed reboot system from the TWRP screen.

1. Your {{ device.codename}} should now be running the  {{ device.upgrade_available}} version of /e/..enjoy !!!
