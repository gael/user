### Manually upgrading your {{ device.codename }} to {{ device.upgrade_available }}
   - Make sure your computer has working adb. Setup instructions can be found in the `Basic Requirements` section above.
   - Enable USB debugging on your device. Instructions are available in the `Basic Requirements` section above.
   - Next on the phone
      - Open `Settings >> System >> Developer Options` here select `Root Access Options` and choose `ADB Only` 
      - Open `Settings >> System >> Developer Options` here choose `ADB Only` 
   - Now on the PC 
      - In the same console Run `adb reboot sideload`
   
{% include alerts/warning.html content="The device may reboot to a blank black screen, this is a known bug on some recoveries, proceed with the instructions." %}   
    
- Run `adb sideload /path/to/zip (inserting the path to your downloaded /e/OS ROM file)`
    
- Once you have installed everything successfully, click the back arrow in the top left of the screen, then `Reboot system now`

{% include alerts/warning.html content="Avoid installing any additional apps or services if suggested by the recovery. They may cause your device to bootloop, as well as attempt to access and or corrupt your data." %}
