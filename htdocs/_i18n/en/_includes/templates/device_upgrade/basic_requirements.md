### Basic Requirements

* adb and fastbooot should be setup and working on your PC
  > To set up adb follow details provided [here](../../pages/install-adb)

* adb and fastboot need root rights in Linux and administrator rights in Windows.
  > In Linux prepend sudo to any adb or fastboot command For e.g. “sudo fastboot reboot” and
    in Windows start the command window as administrator.

{% include alerts/danger.html content="Before following these instructions please ensure that the device is on the latest Android firmware available for your device. For example **if available**, before flashing /e/R install Stock Android 11, before /e/Q it should be Stock Android 10 or before /e/ Pie it should be Stock Android 9." %}