{% assign device = page.device %}

### Download

* Download Upgrade Build for the {{ device.codename }} here
{% if device.build_version_dev %}
- [{{ device.build_version_dev }} (dev)](https://images.ecloud.global/dev/{{device.codename}}/) ({{ device.build_version_dev }})
{% endif %}
{% if device.build_version_stable %}
- [{{ device.stable }} (stable)](https://images.ecloud.global/stable/{{device.codename}}/) ({{ device.build_version_stable }})
{% endif %}

* Download [TWRP](https://dl.twrp.me/{{device.codename}})
  {% if device.twrp_version %}

> The TWRP version we tested was {{ device.twrp_version }}

  {% endif %}

> Download of TWRP is not required if you already have it running on your {{ device.codename }}

{% if device.stockupgrade %}
* Download the latest stock ROM build for the {{ device.codename }}
  > stock ROM tested {{ device.stockupgrade }}

{% if device.stockupgrade_tool %}

* Download the {{ device.stockupgrade_tool }}
   > tool version tested {{ device.stockupgrade_toolver }}

{% endif %}
- Follow the steps to install the stock ROM as given on the vendor site

- Once the Stock ROM is flashed check if it is working for e.g receiving and making calls, audio, FMRadio ..

- Next enable developer options on the phone

- Ensure the device bootloader is unlocked. Flashing stock ROM can lock the bootloader on some devices.
  > Follow the device unlock guidelines as prescibed by your vendor. For e.g. on Xiaomi devices it would require running the Mi Unlock tool. Since the device was already unlocked the process should take a couple of minutes only.


{% endif %}
