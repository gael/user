{% assign device = page.device %}

# {% t titles.devices_upgrade_page %}
## Manually upgrade {{ device.codename }} to {{ device.upgrade_available }}

{% if device.type == "phone" or device.type == "Phone" %}
> **Warning** : Downgrading Smartphones already on Android Q or LineageOS 17.x to Pie or from Pie OS to Oreo or Nougat can cause instability or at worst brick some devices.
{% endif%}

{% include templates/suggestions.md %}