{% assign device = page.device %}

### Warning:

  > - Please note this upgrade build could not be tested as there were no device specific testers available.
  > - Some devices may need a revert to stock ROM prior to upgrade.
  > - To flash the stock ROM you would need to use a tool provided by your vendor
  > - You may also need to download a recovery image built specifically for your device.
