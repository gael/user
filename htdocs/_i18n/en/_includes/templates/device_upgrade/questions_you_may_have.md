{% assign device = page.device %}

### Answers to some questions you may have on the process
#### Why do I need to manually upgrade the OS
> - The updater app does not support upgrades from one Android version to another. It will hide any update of a different version.
> - We are working on resolving this issue.
> - While this issue is resolved, users need to manually upgrade their OS.
> - Upgrading manually requires similar steps to installing /e/OS for the first time.

{%  if device.stockupgrade %}
#### Why do I need to flash Stock ROM before flashing the /e/ROM to upgrade?
> - The stock ROM has firmware and configuration settings which is not included in the /e/OS ROM. These files are required for the hardware to work correctly. By flashing the stock your smartphone is optimized to receive this operating system update. This will be a one time process and subsequent /e/OS updates will be available OTA (over the air).
> - Flashing the /e/OS upgrade directly without flashing the stock ROM can result in the device going into a bootloop.
{% endif %}
