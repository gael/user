{% assign device = page.device %}

{% assign title = device.vendor %}
{% assign subtitle = device.codename %}
{% assign device_title = device.name %}
{% assign details_title = "Device Details" %}
{% assign image_url = "/images/devices/" | append: device.codename | append: ".jpg" %}
{% assign image_alt = device.codename %}
{% assign e_os_build_versions_status = "" | split: ',' %}
{% if device.build_version_dev %}
    {% assign t = device.build_version_dev | append: ' (dev)' %}
    {% assign e_os_build_versions_status = e_os_build_versions_status | push: t %}
{% endif%}
{% if device.build_version_stable %}
    {% assign t = device.build_version_stable | append: ' (stable)' %}
    {% assign e_os_build_versions_status = e_os_build_versions_status | push: t %}
{% endif%}

{% assign e_os_supported_models = device.models %}

{% assign about_topics_titles = "" | split: ',' %}
{% assign about_topics_contents = "" | split: ',' %}

{% assign details_topics_titles = "" | split: ',' %}
{% assign details_topics_contents = "" | split: ',' %}

{% assign maintainers = "" | split: ',' %}

{% if device.maintainers and device.maintainers.first %}
    {% assign maintainers = device.maintainers %}
{% else %}
    {% assign maintainers = maintainers | push: "/e/OS Build Team" %}
{% endif %}

{% tf _includes/templates/about_topics/support_easy_installer.md %}
{% tf _includes/templates/about_topics/install.md %}
{% tf _includes/templates/about_topics/upgrade.md %}
{% tf _includes/templates/about_topics/build.md %}
{% tf _includes/templates/about_topics/whats_not_working.md %}
{% tf _includes/templates/about_topics/have_found_a_bug.md %}
{% tf _includes/templates/about_topics/model_not_supported.md %}
{% tf _includes/templates/about_topics/stock_revert.md %}
{% tf _includes/templates/about_topics/minimal_os.md %}


{% tf _includes/templates/details_topics/release.md %}
{% tf _includes/templates/details_topics/soc.md %}
{% tf _includes/templates/details_topics/nand.md %}
{% tf _includes/templates/details_topics/ram.md %}
{% tf _includes/templates/details_topics/cpu.md %}
{% tf _includes/templates/details_topics/architecture.md %}
{% tf _includes/templates/details_topics/gpu.md %}
{% tf _includes/templates/details_topics/network.md %}
{% tf _includes/templates/details_topics/storage.md %}
{% tf _includes/templates/details_topics/sd_card.md %}
{% tf _includes/templates/details_topics/screen.md %}
{% tf _includes/templates/details_topics/device_tree.md %}
{% tf _includes/templates/details_topics/bluetooth.md %}
{% tf _includes/templates/details_topics/wi_fi.md %}
{% tf _includes/templates/details_topics/peripherals.md %}
{% tf _includes/templates/details_topics/cameras.md %}
{% tf _includes/templates/details_topics/dimensions.md %}
{% tf _includes/templates/details_topics/battery.md %}
{% tf _includes/templates/details_topics/boot_modes.md %}

{% include device/product_page.html %}