{% assign device = page.device %}

{% assign manually_upgrade_file_name       = "manually_upgrade" %}
{% assign basic_requirements_file_name     = "basic_requirements" %}
{% assign warning_file_name                = "warning" %}
{% assign download_file_name               = "download" %}
{% assign steps_to_flash_file_name         = "steps_to_flash" %}
{% assign questions_you_may_have_file_name = "questions_you_may_have" %}

{% if device.manually_upgrade_file_name %}
    {% assign manually_upgrade_file_name = device.manually_upgrade_file_name %}
{% endif %}

{% if device.basic_requirements_file_name %}
    {% assign basic_requirements_file_name = device.basic_requirements_file_name %}
{% endif %}

{% if device.warning_file_name %}
    {% assign warning_file_name = device.warning_file_name %}
{% endif %}

{% if device.download_file_name %}
    {% assign download_file_name = device.download_file_name %}
{% endif %}

{% if device.steps_to_flash_file_name %}
    {% assign steps_to_flash_file_name = device.steps_to_flash_file_name %}
{% endif %}

{% if device.questions_you_may_have_file_name %}
    {% assign questions_you_may_have_file_name = device.questions_you_may_have_file_name %}
{% endif %}

{% capture manually_template_path %}_includes/templates/device_upgrade/{{ manually_upgrade_file_name }}.md{% endcapture %}
{% capture basic_requirements_template_path %}_includes/templates/device_upgrade/{{ basic_requirements_file_name }}.md{% endcapture %}
{% capture warning_template_path %}_includes/templates/device_upgrade/{{ warning_file_name }}.md{% endcapture %}
{% capture download_template_path %}_includes/templates/device_upgrade/{{ download_file_name }}.md{% endcapture %}
{% capture steps_to_flash_template_path %}_includes/templates/device_upgrade/{{ steps_to_flash_file_name }}.md{% endcapture %}
{% capture questions_you_may_have_template_path %}_includes/templates/device_upgrade/{{ questions_you_may_have_file_name }}.md{% endcapture %}

{% tf {{ manually_template_path }} %}
{% tf {{ basic_requirements_template_path }} %}
{% tf {{ warning_template_path }} %}
{% tf {{ download_template_path }} %}
{% tf {{ steps_to_flash_template_path }} %}
{% tf {{ questions_you_may_have_template_path }} %}