{% assign details_topics_titles = details_topics_titles | push: "Release" %}

{% capture released_content %}

    {%- if device.release != null -%}

        {%- if device.release.first -%}
            {%- for model in device.release -%}
                {%- assign release = model.first[1] | split: '-' -%}
                {{ model.first[0] }}: {% include snippets/release.html -%}
                {%- unless forloop.last -%}
                    <br/>
                {% endunless %}
            {%- endfor %}
        {%- else %}
            {% assign release = device.release | split: '-' -%}
            {% include snippets/release.html %}
        {%- endif %}

    {%- else %}
        –
    {%- endif %}
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: released_content %}