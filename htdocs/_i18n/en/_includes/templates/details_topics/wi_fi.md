{% assign details_topics_titles = details_topics_titles | push: "Wi-Fi" %}

{% capture wifi_content %}
    <span class="span-boot">
        {{ device.wifi }}
    </span>
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: wifi_content %}