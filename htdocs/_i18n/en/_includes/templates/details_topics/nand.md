{% if device.nand %}
    {% assign details_topics_titles = details_topics_titles | push: "nand" %}

    {% capture nand_content %}
        {{ device.nand }}
    {% endcapture %}

    {% assign details_topics_contents = details_topics_contents | push: nand_content %}
{% endif %}