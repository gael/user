{% assign details_topics_titles = details_topics_titles | push: "GPU" %}

{% capture gpu_content %}
    {%- if device.gpu != null -%}
        {{ device.gpu }}
    {%- else %}
        –
    {%- endif %}
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: gpu_content %}