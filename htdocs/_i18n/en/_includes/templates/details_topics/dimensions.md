{% assign details_topics_titles = details_topics_titles | push: "Dimensions" %}

{% capture dimensions_content %}
    {% if device.height and device.width and device.depth %}
        {{ device.height }} (h)<br>
        {{ device.width }} (w)<br>
        {{ device.depth }} (d)
    {% else %}
        –
    {% endif %}
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: dimensions_content %}