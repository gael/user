{% if device.recovery_boot %}
    {% assign details_topics_titles = details_topics_titles | push: "Boot Modes" %}

    {% capture boot_modes_content %}
        <strong>Recovery :</strong><br> <span class="span-boot">{% t devices.with_the_device_powered_off %} - {% t device.recovery_boot %}</span>
        <strong>Download :</strong><br> <span class="span-boot">{% t devices.with_the_device_powered_off %} - {% t device.download_boot %}</span>
    {% endcapture %}

    {% assign details_topics_contents = details_topics_contents | push: boot_modes_content %}

{% endif %}