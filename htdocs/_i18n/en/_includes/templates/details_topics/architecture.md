{% assign details_topics_titles = details_topics_titles | push: "Architecture" %}

{% capture architecture_content %}
    {%- if device.architecture.cpu -%}
        CPU: {{ device.architecture.cpu }}<br/>
        Android: {{ device.architecture.userspace }}
    {%- else -%}
        {{ device.architecture }}
    {%- endif -%}
{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: architecture_content %}