{% assign details_topics_titles = details_topics_titles | push: "Storage" %}

{% capture storage_content %}

    {%- if device.storage != null -%}
        {{ device.storage }}
    {%- else %}
        –
    {%- endif %}

{% endcapture %}

{% assign details_topics_contents = details_topics_contents | push: storage_content %}