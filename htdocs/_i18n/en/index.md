## About the /e/OS Project

> Discover more about project /e/OS

- [What's /e/OS? Read the product description!]({% translate_link what-s-e %})
- [Install /e/OS on a supported smartphone!]({% translate_link devices %})
- [Create a free cloud account]({% translate_link create-an-ecloud-account %})
- [HOWTOs]({% translate_link support-topics %})
- [FAQ]({% translate_link support-topics %})
- [Share your /e/xperience!]({% translate_link testimonies %})


## Contribute to /e/OS

> Do you want to help the project?

- [Projects looking for contributors]({% translate_link projects-looking-for-contributors %})
- [Job openings at /e/]({% translate_link jobs %})
- [ROM Maintainer]({% translate_link rom-maintainer %})
- [Translators]({% translate_link translators %})
- [Testers]({% translate_link testers %})
- [Volunteers]({% translate_link volunteers %})
- [Bug bounty program]({% translate_link bug-bounty-program %})

## Learn more about

> Information about the /e/OS project or its applications

- [/e/OS easy-installer]({% translate_link easy-installer %})
- [Maps application]({% translate_link maps %})
- [The different build types]({% translate_link build-status %})
- [Apps Installer FAQ]({% translate_link apps %})
- [e.foundation Legal Notice & Privacy](https://e.foundation/legal-notice-privacy/)

## Shortcuts

> Quick links to /e/OS websites, forums and telegram channels

- [/e/OS web site](https://e.foundation/)
- [/e/OS Source Code](https://gitlab.e.foundation/e)
- [Need Help? Support?](https://e.foundation/contact/)
- [Report a bug in /e/OS or apps](https://gitlab.e.foundation/e/backlog/-/issues/new)
- [Add your Smartphone to the supported list](https://community.e.foundation/c/e-devices/request-a-device)
