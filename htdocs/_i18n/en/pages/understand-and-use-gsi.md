{% include alerts/danger.html content="Please ensure you have read all the sections and understand the limitations of GSI before proceeding with an installation." %}
## What is a GSI

 A generic system image (GSI) is a system image with modifications made to the configurations for specific Android devices. It is a pure Android implementation with unmodified Android Open Source Project (AOSP) code that any Android device running Android 8.1 or higher should be able to run successfully.

GSI's are used for running tests. The stock image of an Android device is replaced with a GSI and then it is tested to ensure that the device implements vendor interfaces correctly with each of the latest version of Android. 

## Generic GSI vs Device Specific

The GSI was conceived as a generic implementation of the android source code which should be able to run on all devices. A generic GSI will work for you if you are
  - using the device for testing
  - are not particular about features like VoLTE or Bluetooth
  - can handle an unstable device


A device specific GSI on the other hand will have a dedicated developer who has created the GSI using the device specific code. 

This will remove some of the bugs that the generic GSI would have with the hardware.
## What the GSI is not 

- Do not use the GSI on a device you use as a daily driver. Use it only on a test device
- A GSI should boot on any compatible device, but may not offer a full support of all its features
- All hardware features will **not** work on a GSI
- All software features will **not** work on a GSI
- Remember the GSI is only used to test the OS implements vendor interfaces correctly. Which means it is a testing tool rather than a full fleged implmentation of the OS.
- There will be no OTA (over the air) updates for the GSI's. This means that the user will have to install new GSI builds manually every time it is released, or when they want want to update it. 

## Is there a GSI of /e/OS available?
Yes we have a series of GSI builds for various architectures. 

What is even better is now you can test it out. 

Before you grab the GSI build for your device check the [Device Requirement section](/support-topics/understand-and-use-gsi#device-requirements) below.  

To install the GSI on your device following the instructions given [here](install-GSI).

## Device requirements 
- Get the architecture information for your device. Check if it is
  - arm
  - arm64
  - arm with 64 binder

{% include alerts/tip.html content="You can find this information on sites like [GSMArena](https://www.gsmarena.com/)" %}

## Additional reference on GSI

- [Generic System Images](https://source.android.com/setup/build/gsi)
- [Treble experimentations](https://github.com/phhusson/treble_experimentations/wiki  )
