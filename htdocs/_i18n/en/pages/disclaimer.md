### Device Details Disclaimer

The device details provided here has been gleaned from multiple sources including device manufacturers web sites and online sources like [GSMArena](https://www.gsmarena.com/) and [LineageOS wiki](https://wiki.lineageos.org/). We have tried to ensure the correctness of this information during our review process. We also update the data based on the feedback we receive from our  users.

Even with all the reviews and feedback it is not possible to guarantee that the information provided would be 100% accurate or up to date. We would recommend that users double check the information by visiting the device manufacturer website and get the updated details.

If you think that information for a particular phone is wrong or missing, please contact us at <support@e.email> with the details.

#### Now for the legalese.

[e.foundation](https://e.foundation/) is not responsible for any errors or omissions, or for the results obtained from the use of the information provided for different devices. All smartphone specifications information on this site is provided “as is,” with no guarantee of completeness, accuracy, timeliness or the results obtained from the use of this information.
