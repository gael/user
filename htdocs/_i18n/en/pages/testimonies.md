## Report your experience!

We think that /e/ will become the 3rd main mobile OS in the future. You can be part of history! We are probably living in the most exciting part of the adventure with its inception: a few of us together - developers & users - shaping a revolution.

Because you are part of that revolution we’d like to tell your stories with /e/ and not just ours.

Talk about you and your experience: make a simple post with on twitter, mastodon and other social media, show what it looks like, why it is important for you. You can also open an account at Medium to talk write a small article with illustrations. Post some videos on [peertube](https://joinpeertube.org/en/) (or even youtube...)...

## And we will relay!

We love to hear from your positive experience, and we will relay all the best stories about /e/ to social media, in our newsletters... Just ping us mentioning [@gael_duval](https://twitter.com/gael_duval) for Twitter posts or [@gael@mastodon.social](https://mastodon.social/@gael) for Mastodon posts, or write us to warn us at: <contact@e.email>

Thanks again for your support!
