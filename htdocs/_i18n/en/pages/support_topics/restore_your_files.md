If you are here, we assume you have already created backups of the data on your smartphone.

If not, you can refer these guides to 
 - [backup your contacts](/support-topics/backup-your-contacts)
 - [backup your sms](/support-topics/backup-your-sms)
 - [backup your files](/support-topics/backup-your-files)


## Steps to restore your

### Contacts
- In the Contacts app
   - Click on `Import from .vcf file`
   - If you have added an `/e/ account` to the phone, it will ask you
   - whether you would like to add these contacts `locally` on your phone
   - the alternate location is the `/e/Cloud`

### SMS
- In the Message app
   - Click on `Restore`
   - Select the backup.


### File
- You can replace the files previously backed up to their respective original folders.