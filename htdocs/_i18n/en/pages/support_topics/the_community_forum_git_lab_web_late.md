- [Get a “/e/ user” title on this forum](https://community.e.foundation/t/howto-get-a-e-user-title-on-this-forum/1024)
- [Report an issue ](report-an-issue)
- [Create a log ]({% tl support-topics/create-a-log %})
- [Get a log from your phone](https://community.e.foundation/t/howto-get-a-log-from-your-phone/2366)
- [Enable adb logcat on first boot without authentication (debugging boot loops)](https://community.e.foundation/t/howto-enable-adb-logcat-on-first-boot-without-authentication-debugging-boot-loops/4496)
- [Use Weblate - for translation team members]({% tl how-to-use-weblate %})

