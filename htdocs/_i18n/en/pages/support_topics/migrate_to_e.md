### For iPhone Users
- [Create an /e/ account ](/create-an-ecloud-account)
- [Import contacts from Apple to eCloud](https://community.e.foundation/t/howto-import-contacts-from-apple-to-e-cloud/5532)

### Coming from Stock Android or Custom ROMs
- [Create an /e/ account ](/create-an-ecloud-account)
- [Add a Gmail account](/support-topics/add-a-gmail-account)
- [Backup your contacts](/support-topics/backup-your-contacts)
- [Backup your sms](/support-topics/backup-your-sms)
- [Backup your photo,videos and files](/support-topics/backup-your-files)
- [Restore your photo,videos and files](/support-topics/restore-your-files)
- [Migrate your data from Google Cloud to ecloud](/support-topics/migrate-data-from-google-cloud)
- [Import Gmail contacts into eCloud](https://community.e.foundation/t/howto-import-gmail-contacts-into-ecloud/2809)
- [Import Google Keep notes into /e/](https://community.e.foundation/t/how-to-import-google-keep-notes-into-e/3165/3)
- [Move contacts from Google account to /e/ (export, import)](https://community.e.foundation/t/howto-move-contacts-from-google-account-to-e-export-import/12390)
- [Transfer WhatsApp messages from Google Android to /e/ (backup and restore)](https://community.e.foundation/t/howto-transfer-whatsapp-messages-from-google-android-to-e-backup-and-restore/12389)
- [Download every app in the Google Play Store thanks to Aurora Store](https://community.e.foundation/t/howto-download-every-app-in-the-google-play-store-thanks-to-aurora-store/13622)
- [Some more useful tips and links](/support-topics/migration-suggestions)
