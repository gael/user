### My device is no longer supported by LineageOS. Will /e/ also stop support?

/e/ is not dependent on LineageOS in deciding which devices to support or stop supporting. We already have several devices which are not on the LineageOS supported list. We plan to port more devices to work with /e/ code.

### If my device is not supported by LineageOS will I still get the security patches on time?

Yes. We will download and apply the security patches for devices if they are available for download from the source.

### Will /e/ ROM be supporting keyboard-enabled smartphones ?

Porting on any device depends on:
- Is it possible to unlock the bootloader
- Do we have access to some reference source code tree to build Android on it
- Bandwidth of the developer (porting a new device with all features working, takes between 2 and 3 months for an expert developer)

### My device is almost ten years old. Will you continue support?

We will have to stop support for some older Android version like Nougat. The reason behind this is
- It is very difficult to support multiple OS versions at the same time
- Security updates cannot be back-ported forever. 

Please note  the /e/ experience is similar whatever version of Android is running in the background. You can check the list of supported devices [here]({% tl devices %})
### Would you recommend a phone for me ?

The decision to select a phone should be based on 

- It being on the [/e/OS supported list]({% tl devices %}) (if you plan to flash /e/OS )
- Availability in your location
- Easy access to hardware service centers
- Last but not the least if it suits your budget 

### Does /e/ allow for the bootloader to be locked on phones that support verified boot

We lock the bootloader only on preinstalled Fairphone 3/3+ at the moment. It is on the development roadmap to get this feature on all /e/OS supported devices.

### As a fork of LineageOS, do you use latest LineageOS source code when building your ROM

Yes, before each major build we first sync up latest LOS branches. Next we merge with our branches. 

### Does /e/ OS allow rooting or can apps that require root be used?

You can root /e/OS. That being said it is not really in the scope of our projects. As mentioned in this thread we are building /e/OS for the average user and not for geeks who already have a lot of choices or know how to technically modify their smartphones. 

For users who want to root the device please note that /e/ does not require rooting.  Also /e/ does not allow app rooting except for adb under developer options. Users can use third party tools (like Magisk) to access such features, but it is beyond the scope of this guide.
