
## Purpose of this guide
The purpose of this guide is to explain how to link your PC with the eCloud.
## Advantages of linking 

The eCloud is your personal repository of data comprising documents, music and image files. 

With /e/OS and your /e/ ID it is possible to easily transfer data from your phone to your personal eCloud. 

By linking your eCloud with your PC, you would be able to easily access your documents and images from both your PC as well as your smartphone. 


In this guide we will show how to link your PC with your eCloud. 

The method of linking may vary based on the Operating System you have on your PC. Here we cover a few of the popular Operating systems.

## How to link under different Operating Systems

[Ubuntu](/support-topics/link-ecloud-with-ubuntu)

[Windows](/support-topics/link-ecloud-with-windows)

We will be adding more Operating Systems to this list


## Still do not have an eCloud or /e/ account ? 

Check [this guide](/create-an-ecloud-account)
