
## Download

### Step 1

- Open your browser and browse to the [NextCloud website](https://nextcloud.com/)

![](../images/howtos/ecloud/ecloud_link_win_1.png)

### Step 2 

- On the website navigation bar you would see an option 'Get NextCloud` 
- Pointing the cursor on this will open a sub menu which will show the option `Desktop and Mobile apps`

### Step 3 

- Click the option will open up this screen 
- Click the button `Windows 8.1+` 

![](../images/howtos/ecloud/ecloud_link_win_2.png)

### Step 4

- This open a popup screen which will ask you where to save the NextCloud desktop installer file. 
- The file would be in the `.msi` format. At this point the installer will ask you which features to install. 
- Choose the default selected options.

![](../images/howtos/ecloud/ecloud_link_win_3.png)

### Step 5

- Here we save the downloaded exe to the Downloads folder. You can save it to any other folder. Remember where you choose to save the exe.    

![](../images/howtos/ecloud/ecloud_link_win_4.png)

## Run

### Step 1 

- Execute the .exe file by double clicking on it. 
- This will open a Dialog as shown here
![](../images/howtos/ecloud/ecloud_link_win_5.png)

### Step 2

- The exe will ask you where it should save the installation files. Choose the default values selected.

![](../images/howtos/ecloud/ecloud_link_win_6.png)

### Step 3

- Select `Install` in the next screen


![](../images/howtos/ecloud/ecloud_link_win_7.png)

### Step 4 

- A green colored progress bar marking the installation on your PC will appear.
- Wait for the installation process to end.



![](../images/howtos/ecloud/ecloud_link_win_8.png)

### Step 5 

- After a successful installation you will see a screen like this


![](../images/howtos/ecloud/ecloud_link_win_9.png)

### Step 6 

- Restart the system to complete the installation.
- You can restart by clicking the `Yes` button in the dialog that comes up as shown in the screenshot


![](../images/howtos/ecloud/ecloud_link_win_10.png)


## Link

### Step 1 
- Once your PC has rebooted you should be able to see the NextCloud desktop client application on your PC

- To start the NextCloud application click on the `Log in to your NextCloud` button
- 
![](../images/howtos/ecloud/ecloud_link_win_11.png)

### Step 2

Enter the credentials as under


`server` : https://ecloud.global

     
Press Next button to move to the next screens


![](../images/howtos/ecloud/ecloud_link_win_12.png)



### Step 3

- Here you will be asked to switch to your browser to connect to your account
- Click on the Icon on the Pop up



![](../images/howtos/ecloud/ecloud_link_win_13.png)


## Log In

When asked enter credentials as under

`Username` : Your /e/ID username. Write the complete username in the format username@e.email


`Password` : Your /e/ID password


![](../images/howtos/ecloud/ecloud_link_win_14.png)



## Authorize


At this point you need to authorize the app to connect to your ecloud account

- Grant access in this screen


![](../images/howtos/ecloud/ecloud_link_win_15.png)


- The next screen will display that your account is connected


![](../images/howtos/ecloud/ecloud_link_win_16.png)



- Set up a local folder to act as your NextCloud repository

![](../images/howtos/ecloud/ecloud_link_win_17.png)


> You would see different options available 
> 
>  `Use virtual files instead of downloading..`.this option will save space on the local drive  
> 
>  `Syncronize everything ..`.. this option will download all the files and sub folders ..will take up local storage space on your PC
> 
>  `Choose what to ..`..select what to sync

## Verify

- You can open the local folder and now it will display the folders as available on your eCloud. 

> Please note it might take a little time to open up the local folder depending on your internet speed. 