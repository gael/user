### Is /e/ open source?

Yes - all source code is available on our [/e/ Gitlab](https://gitlab.e.foundation/e). You can compile or fork it. Some prebuilt applications are used in the system. They are built separately from source code available [here](https://gitlab.e.foundation/e) or synced from open source repositories such as F-Droid. We ship one proprietary application though ([read the statement](/maps)).

### How can I contribute to an OpenSource Project like /e/ ?
You can contribute to the /e/ project in the following ways :
- Join one of the /e/ [projects looking for contributors](/projects-looking-for-contributors)
- Help the development team by solving [issues listed here](https://gitlab.e.foundation/e/backlog/-/issues)
- [Start building](build-e) /e/ for a new device and become a [ROM maintainer](/rom-maintainer)

If you have any suggestions, feel free to contact us! Send an email to <join@e.email>, with a clear subject line like (ROM Developer: Mediatek devices, Project developer : mail app).

### Is /e/ a combination of LineageOS and microG?

/e/ is forked from LineageOS. That being said we have made several changes to the code. Some of which are: 
- We have modified several parts of the system installation procedure, settings organization, default settings. 
- We have disabled and/or removed any software or services that were sending personal data to Google (for instance, the default search engine is no longer Google). 
- We have integrated microG by default, have replaced some of the default applications, and modified others. 
- We have added a synchronization background software service that syncs multimedia contents (pictures, videos, audio, files...) and settings to a cloud drive, when activated.
- Also, we have replaced the LineageOS launcher with our own launcher.  Written from scratch, the `Bliss Launcher` has a totally different look and feel from LineageOS.
- We have implemented several /e/ online services, with a single /e/ user identity (user@e.email). This infrastructure will be offered as docker images for self hosting: drive, email, calendar... to those who prefer self-hosting.
- We have added an account manager within the system with support for the single identity. It allows users to log only once, with a simple "user@e.email" identity, for getting access to /e/'s various online services (drive, email, calendar, notes, tasks).

Read the /e/ product description [here](what-s-e)

### Can I customize the ROM myself ?

Absolutely! /e/ was not made for geeks or power users. It is designed to offer a credible and attractive alternative to the average user. It provides more freedom to the user and a better respect of user's data privacy compared to mobile operating systems offered by the worldwide duopoly in place.

We really appreciate your involvement though. Your beta-testing and valuable feedback will help to make this alternative available to many more people who can't do this sort of thing for themselves.  

Feel free to share your unofficial or customized /e/ ROM's on [our forum](https://community.e.foundation/c/e-devices/unofficial-builds/92)

###  Can I get the default AOSP/LineageOS look on /e/OS ?

The beauty of the OS is that users can install applications and customize the look and feel of the OS. You can install a launcher of your preference and get the look you prefer.

### Is /e/OS stealing the work of LineageOS developers 

We are using the rules of open source software. Just like AOSP-Android is forking the Linux kernel work, just like LineageOS is forking AOSP work, /e/OS is forking the LineageOS code base. 

To look for the `LineageOS vs /e/OS` argument, think about `Debian vs Ubuntu`.

/e/'s focus is on the final end-user experience, and less on the hardware. 

/e/ and LineageOS's purposes and target users are different. 

We strongly encourage core developers to contribute upstream to LineageOS. 


### Does /e/ have a simple way for updating the system regularly?


Yes we have a simple 1-2 click `Over The Air` (OTA) update feature which is accessible from the Settings. 

If you already have /e/OS on your phone you can check for updates by going to 

`Settings >> system Updates >> Tap the circular arrow icon to manually trigger a refresh.`

If there are any system updates it will immediately show up on the screen.  

Be sure to check for updates over un-metered wifi or you may incur charges.

### How do I know if my device is supported?

This is our list of [supported devices]({% tl devices %})

You could check if there are some Unofficial builds, made by the community. Those are visible [here ](https://community.e.foundation/c/e-devices/unofficial-builds)
 
If you don’t find your phone in these lists, you can request support for it on our community forum.
We evaluate phone support based on most popular requests and feasibility. More at [this URL](https://community.e.foundation/c/e-devices/request-a-device)

### From which version of Android is /e/ forked?

- Android 11 (R)/LOS 18 (/e/-0.x-R branch). 
- Android 10 (Q)/LOS 17 (/e/-0.x-Q branch).
- Android 9 (Pie)/LOS 16 (/e/-0.x-P branch).
- Android 8 (Oreo)/LOS 15 (/e/-0.x-O branch).
- Android 7 (Nougat)/LOS 14 (/e/-0.x-N branch).

### Is /e/ stable?

No - we are in beta stage at the moment. The being said the system is pretty usable , but we  would still suggest you use it at your own risk!

### Why do you have this unsearchable name for the OS ?

We had to change our name for legal reasons [as explained here](https://www.indidea.org/gael/blog/leaving-apple-and-google-e-is-the-symbol-for-my-data-is-my-data/).

/e/ is a textual representation of our "e" symbol which means "my data is MY data". 

 If you look for "/e/OS" or "/e/ OS" in search engines you find us immediately. 

However we agree that typing /e/ in the browser search bar does not work as the browser ends up checking the local folder structure!

We plan to continue with the /e/OS name for the operating system itself, at least for a while

### Murena - the brand name

As on 19 October 2021, we announced our brand name. It is **Murena**


For more details check this [article published on Medium.](https://gael-duval.medium.com/murena-smartphones-and-cloud-will-protect-our-privacy-357788a1e585)