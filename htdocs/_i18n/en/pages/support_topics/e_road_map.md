### Do you have plans to work with other privacy focused companies like Purism or Pine?

We have been in touch with both project teams.

We were interested in creating a /e/ Librem but nothing came out of our discussions.

On Pine, we have a post with details of how to [port /e/OS](https://edevelopers-blog.medium.com/e-os-ports-for-the-pinebook-and-pinephone-596139c76479) to the PineBook. 

Based on our test of the /e/OS on the PinePhone, we found the current hardware was too slow to offer a decent user experience.


### Any plans on building a smartphone of your own?

We may work towards this in the future. 

Some of the key points we would want to consider in a smartphone of our own would be 
- Focus on value for privacy
- Physical kill switches for hardware components like camera, microphone
- A good battery that can be changed for better sustainability
- Possibly a smaller form factor 

This type of an endeavor would require a sizable investment besides reliable partners.

For now the creation of a /e/ Phone is not a priority item in our roadmap. We are focusing on making our /e/OS better for our users.


### Any plans in the future to expand on what devices you offer through your [murena Shop](https://murena.com/)?

We currently have three ranges of products:

- /e/OS on brand new Fairphone 3/3+ devices and sold in EU only because of incompatibility with certain US networks
- /e/OS on brand new GS290 (Gigaset) devices and sold in EU only because of incompatibility with certain US networks
- /e/OS on high grade refurbished smartphones (Galaxy S8, S9, S9+)
We have two new models in mind for the US but it needs to be confirmed. You can [subscribe to our newsletter](https://e.foundation/contact-e-2/#my-anchor1)for the announcement!

### Is the /e/OS compatibility with Asiapac networks in Australia or New Zealand ?
You can check with our [partner in Australia](https://www.ncryptcellular.com.au/)

### Is it possible to see /e/OS on tablets that you make?

It is possible to install /e/OS on tablets. You can check some of the device builds [here](devices/).

Unfortunately creating /e/ tablets is not in our short-term scope.

### Are there any plans on offering a desktop mode like Samsung Dex

That is something we have in mind since the beginning of the /e/ story. It is also regularly requested by some users and potential business partners.

The issue with this at the moment is that we have quite limited resources in term of development. We have to focus on the core project first. It is something we have in mind and on our roadmap.


### Will you be adding e-ink support
We would love to support "slower experience devices" with e-ink once the team has more bandwidth to handle the tasks.

### Would the installation wizard offer different levels of configurations for user like default, intermediate, expert ....?

We plan to offer the ability to uninstall default apps. That is in our [roadmap](https://community.e.foundation/t/e-2021-the-year-everything-is-going-to-change-2-2-a-product-roadmap/31253).

### Will there be more phone models added to the Easy Installer in the future?

The development on the Easy Installer is a community effort. Which means that users with development skills contribute to the code. Addition of more devices depends on efforts from the community. /e/ development team help with the bug fixes in the core source code of the Easy Installer.

### Would you be interested in aligning with other like minded project teams

Yes we are always ready to discuss. Feel free to get in touch with them and share the details with us on <contact@e.email> ID.

### Will /e/ project launch more web services, such as Spot?

First we have to improve Spot (our default search engine, meta-search engine forked from Searx) as it is too slow. We have other services and products in mind. You can [subscribe to our newsletter ](https://e.foundation/contact-e-2/#my-anchor1)for the announcement when we launch them!


### Will you disclose the calculation method of the privacy score integrated into /e/OS 

It is in the roadmap for future releases

### Does /e/ have ASHA support for hearing aids?

Not yet unfortunately
