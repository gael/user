### If I find a bug, should I send you an email?

No - please report bugs and send suggestions as [explained here](/support-topics/report-an-issue). Add as much details and also a [log](/support-topics/create-a-log) where possible. This makes it easier for our developers to debug the issue. Please note the assigned developers may contact you through the Gitlab comments section in case they need more inputs from your side.

Please avoid using email or IM for reporting issues, it's not an efficient process.

### I have some suggestions. How should I contact you?

We welcome you suggestion or feature requests. You can create a topic [here](https://community.e.foundation/c/e-smartphone-operating-system/request-a-feature/82). On the forum other users can go through your suggestions and provide their feedback. 
Based on a large number or user requests we will share it with our development team who will evaluate if the suggestion is technically possible. Based on their inputs we can plan to include it in our development roadmap.

If you have suggestions related to improve the privacy on our OS, you can send an email to <privacychallenge@e.email>

