### Is there a way to easily check if the services offered by /e/ are online

You can check the status of /e/ services at [this url](https://status.ecloud.global/) 

### Is my data on the /e/ servers encrypted?

  - All ecloud files are encrypted by server side encryption. 
  - All backup are encrypted 
  - True E2E (end to end) encryption for ecloud files, full databases encryption, and encryption for mails is in our plans as a long-term feature.

### Can I delete my /e/ ID?
Yes you can. Refer [this document](/create-an-ecloud-account#how-can-i-delete-my-e-account-eemail) to understand how this can be done.


### What can I do if I lost my phone?
> For users who had an /e/ ID registered on the phone you lost

The steps to be taken are as under:
- Request an account deactivation by sending a mail to <support@e.email>
- For the request use your /e/ mail ID or the recovery email you used when you created the /e/ ID
we have both email ID's in our database and will be able to help you deactivate your ID
- To explain further here you request for an account deactivation. Do not Delete your account as that will  disable your access to your account data on the [eCloud](https://ecloud.global) as well.

### Some additional information if you lost your phone
As you may be aware eCloud is forked from NextCloud.

Next cloud has two features which help in such situations
 - Revoke Session

  The Revoke  Session feature is done from the server.
  You can view this in your ecloud under `settings/user/security >devices & sessions`

 - Wipe Device

  This implementation has not been added as yet into the OS.

### Can the eCloud admin access my data

On any Nextcloud instance, an administrator can see your files and all the information in the database. Not only NextCloud admins, Server admins (ie root) have this access. The only way to make files inaccessible is by using end-to-end encryption.

Nextcloud has no plans for database encryption as per our understanding. We are planning to provide an additional service down the line, maybe based on [EteSync](https://www.etesync.com/). The team needs to investigate its feasibility and the User Experience to provide a seamless operation between the phone and the web interface.

Our eCloud admins need to make backups, perform upgrades, reset passwords, etc.  We have implemented Nextcloud's server side encryption on our servers. 
We do not intend to enable end to end encryption (E2EE) [plugin from Nextcloud](https://apps.nextcloud.com/apps/end_to_end_encryption) because it is not satisfactory in its current state and can lead to data loss, as shown on the user reviews.

You can also host your own [ecloud](https://gitlab.e.foundation/e/infra/ecloud-selfhosting) or a [plain Nextcloud](https://nextcloud.com/athome/) (no e-mail server included in that case).

### Can the eCloud admin read our emails ?

Only a few team members can read your emails, unless some encryption mechanism is used like [PGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy). This is the case on any e-mail provider except custom non-standard solutions. Our goal at /e/ is also to set up something that works out of the box including key creation, discovery and seamless experience between eOS and webmail. This will need specific R&D.

### I want to know more about the security on eCloud

Since this is a common query from /e/ users, we would like to clarify a few points:

  -  ecloud.global is a modified version of [ecloud-selfhosting](https://gitlab.e.foundation/e/infra/ecloud-selfhosting), which is based upon several open source projects.
  -  We have implemented [Nextcloud's server side encryption](https://nextcloud.com/blog/encryption-in-nextcloud/) on our servers. As you maybe aware SSE is a requirement for E2EE.
  -  We have a long-standing relationship with a security expert in charge of hardening and monitoring our systems, including ecloud.global.
  -  Read more about it on your [eCloud instance](https://ecloud.global/settings/user/privacy)

A few of the improvements applied to ecloud.global in regards to the base ecloud-selfhosting instance:

  -  Performance tuning adapted to the scale of the service (number of users, storage size, etc).
  -  High availability for all core services: nextcloud, mariadb and redis, behind a HAProxy load balancer.
  -  We try to always keep the infrastructure and applications in compliance with the best available security hardening guidelines available such as [DevSec Hardening Framework](https://github.com/dev-sec) and CIS Benchmark for Ubuntu.
  -  We apply process confinement techniques and mitigation provided by [AppArmor](https://apparmor.net/) and systemd.
  -  We use [Wazuh](https://wazuh.com/) monitoring for threat detection.

If you think (or can prove) that there is a security vulnerability in ecloud.global or the ecloud-selfhosting project, please contact us directly: <security@e.email> 

Kindly use the following key to encrypt any sensitive disclosure:
[https://keys.openpgp.org/vks/v1/by-fingerprint/F242DB4B0F002ED0AB73A5D06E25E121E5939DAF](https://keys.openpgp.org/vks/v1/by-fingerprint/F242DB4B0F002ED0AB73A5D06E25E121E5939DAF)


### Can a custom cloud be configured to connect to eCloud.
You can configure /e/OS to talk to another Nextcloud instance, though you won't get all the features like common username for all services including mail etc. You can also [selfhost ecloud](https://gitlab.e.foundation/e/infra/ecloud-selfhosting) on your own servers if you want.

### Updates coming to eCloud
  Besides the **Wipe Device** feature the **Find my phone** feature is planned for implementation in Q3 of 2021

### Where is eCloud data stored?
The ecloud servers are located in Finland and run on renewable energy. You can refer some documentation on the eCloud [here](https://e.foundation/ecloud/) We will add some more technical documentation around this.
  
