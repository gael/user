### What is the current state of De- googlisation on /e/ ?

For a detailed response please go through the document given [here](https://e.foundation/wp-content/uploads/2020/09/e-state-of-degooglisation.pdf)

### Does /e/OS check for "unwanted" code before loading ?

Please note, we do not check for any `unwanted` code loading before the /e/OS on any of the devices we support. 

The reason is very simple. /e/'s primary focus is deGoogling and improving Privacy related to the industrial collection of personal data by big tech firms. 

To further elaborate the point, /e/OS is not intended for users with strong security needs. 

Here we mean people who can be targeted by organizations like Governments, intelligence agencies, criminal organizations etc. We believe such people should not be using /e/OS expecting it would protect them from this extra high-level of scrutiny. There are other alternative Operating Systems which focus on such aspects and may be better suited for such users.

The most serious "real life" security issue for regular /e/ users would be a scenario where the device is stolen or is lost.

That is why we encourage our users to implement the encryption mode on their device.

We are thinking about introducing a way by which /e/OS users can safely do a complete device wipe from their user account at [ecloud.global](https://ecloud.global).

At /e/ we are always interested in improving the product. At the same time we also have to keep our efforts focused given the small size of our development team. 

We welcome community contributors who can help us enhance security aspects of the /e/OS. 

If you think you can help us with this send us your details on <contact@e.email>

### How does Google collect my data. Has stock LineageOS removed all closed-source  or user telemetry  gathering components

A recent study conducted by [Prof. Doug Leith](https://www.scss.tcd.ie/doug.leith/) at Trinity College Dublin along with [Dr. Paul Patras](https://scholar.google.com/citations?user=0kC3nVgAAAAJ&hl=en) and Haoyu Liu at the University of Edinburgh examined the data sent by six variants of the Android OS developed by Samsung, Xiaomi, Huawei, Realme, LineageOS and /e/OS.
Among the findings of this study is this relevant statement.."With the exception of e/OS, all of the handset manufacturers examined collect a list of all the apps installed on a handset. This is potentially sensitive information since it can reveal user interests.."

The complete report is available for review [here](https://www.scss.tcd.ie/doug.leith/Android_privacy_report.pdf)

Some other reports that discuss this subject are linked here 

  - [Trinity College Report](https://www.scss.tcd.ie/doug.leith/apple_google.pdf) 
  - [Vanderbilt Study](https://engineering.vanderbilt.edu/news/2018/study-of-google-data-collection-comes-amid-increased-scrutiny-over-digital-privacy/)


### How is /e/ different from MicroG or LOS 
As you may be aware MicroG and LOS also provide distributions with local emulation of service API achieved by network strength triangulation. 

To understand how /e/ is different please read these documents:
 - [What is /e/]({% tl what-s-e %}#whats-in-e)  
 - [Current state of degooglisation](https://e.foundation/wp-content/uploads/2020/09/e-state-of-degooglisation.pdf) 


###  Is an Apple phone a safe option for a privacy conscious person ?

- iPhone uses Google as a search engine. Ideally they should drop Google search for a more pro-privacy search engine - a meta-search engine like Qwant or DuckDuckGo. Due to this dependency on Google search their phones give away a lot of personal data. 6 MB per day to be precise [read more](https://digitalcontentnext.org/blog/2018/08/21/google-data-collection-research/)

- This Apple - Google deal is a [$12 billion](https://www.businessinsider.fr/us/google-apple-search-deal-doj-antitrust-suit-2020-10) per year deal. This does not work in the advantage 

- Last but not the least, Apple claims that they are acting in favor of privacy. That is claimed privacy. /e/ supports auditable privacy

