### Prerequisites:
{% include alerts/danger.html content="These instructions are valid only for the openSUSE linux and may not work on other Linux distributions." %}
- We ran this test on the openSuse Leap 15.2
- Download latest [SP Flash Tool](https://spflashtool.com/download/)
- Install SP Flash Tool (details see below).
- Download [Android 10 firmware](https://www.androidfilehost.com/?fid=17248734326145687642)
& extract it.

1. First take a backup of your existing data
1. PC and phone are not connected via USB.
1. Device is switched off.
1. Open SP Flash Tool.
1. Go to Download tab (choose "Download Only) and select scatter file
"MT6763_Android_scatter.txt" inside the extracted firmware
1. Press Download.
1. Plug in the device.

The process of downloading the firmware will immediately begin.

{% include alerts/tip.html content="In case you face an error `BROM ERROR` at step #7." %}

**Solution**

Remove modemmanager from your openSUSE PC via Yast2

Reboot the PC and run SP Flash Tool again with the steps 1 to 7

*Note*: After successful installation you can reinstall modemmanager on the PC.

## How to install and run SP Flash Tool on openSUSE

### Prerequisites:
- Verify if `libpng12-0` is installed, if not install `libpng12-0`
- Download latest [SP Flash Tool](https://spflashtool.com/download/) for linux and extract to a folder of your
choice

Run in bash (inside the folder of your choice)

```
chmod +x flash_tool flash_tool.sh
sudo adduser <username> dialout
newgrp – dialout
```

Click here to [return](/devices/GS290/install#ensure-you-are-on-android-10) to continue the installation of /e/OS
