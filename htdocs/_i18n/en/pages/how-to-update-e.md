1. Open the updater app

    `Settings` > `System` > `Advanced` > `Updater`

    ![](images/screenshot_20201119_10_38_46.png)
    ![](images/screenshot_20201119_10_38_53.png)
    ![](images/screenshot_20201119_10_39_01.png)
    ![](images/screenshot_20201119_10_39_06.png)

1. On the last available update, tap on `Download`

    ![](images/screenshot_20201119_10_39_11.png)

    > If no update available, tap the **refresh button** on top right

    > The Updater download the necessary file. If you are on LTE, a warning popup ask you if you prefer to switch to Wifi

1. Once downloaded, tap on `Install`. Tap on `OK` to start the installation.

    ![](images/screenshot_20201119_10_54_46.png)
    ![](images/screenshot_20201119_10_56_03.png)

    > The Install process starts. It may take a long time

1. Once finished, reboot your device.

    ![](images/screenshot_20201119_11_08_39.png)

The device reboots. Update applied! 👏
