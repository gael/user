## GS290
### Solution 1: Using Windows update

{% include alerts/tip.html content="Your phone must be in Fastboot mode, if you miss the fastboot interface driver" %}

1. Go to Windows Update
  - Click on the Start menu in the lower left corner.
  - Click on “Settings”.
  - Go to “Update and Security”.

   <img src="../../images/windows-update-menu.png" width="400" alt="screenshot of windows update menu">

1. Install the necessary driver
  - Click on “Search for updates”.

   <img src="../../images/windows-searchForUpdate.png" width="400" alt="screenshot of windows looking for update">

  - A new section named “Show optional updates” should appear. Click on it.

   <img src="../../images/windows-showOptionnalUpdate.png" width="400" alt="Scrrenshot of window optionnal update menu">

  - Click on “Driver updates” to display the list.

   <img src="../../images/windows-clickOnDriverUpdates.png" width="400" alt="screenshot of windows drivers update menu">

  - Select the driver named “MediaTek - Other hardware - Android ADB Interface” and click on “Download and install. If it doesn’t work and you have other drivers listed, feel free to install them too.

   <img src="../../images/windows-selectDriverToUpdate-GS290.png" width="400" alt="Screenshot of driver selection of 'MediaTek - Other hardware - Android ADB Interface'">

 [Source from community forum](https://community.e.foundation/t/howto-gs290-stuck-in-fastboot-bootloader-mode-with-the-easy-installer-on-windows/29037)

### Solution 2: Manual installation of fastboot drivers for the GS290

1. Download the driver. Unfortunately, Gigaset does not provide an official "fastboot interface driver".
You can use [the one we extracted](https://images.ecloud.global/tools/drivers/windows/fastboot), but there can be cases where it may work.
1. Start your phone in Fastboot mode (turn it off then keep pressing Power + Volume up. Then select Fastboot with Volume up, and confirm with Volume down)
1. Plug your phone to your windows computer
1. Open device manager from windows settings
1. Select the android device with the warning icon
1. Right click to open details of the device
1. Move to Driver section, and select install/update driver
1. A message prompt will open. Choose to use local file.
1. Select the folder where you downloaded the driver
1. Then wait for the driver to install.

{% include alerts/tip.html content="You can follow the [official android documentation about driver installation](https://developer.android.com/studio/run/oem-usb0)" %}


{% include alerts/tip.html content="You may have to reboot your computer after driver installation, to make them available." %}

