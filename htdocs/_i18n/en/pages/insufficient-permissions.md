When running: `adb reboot bootloader` on your PC if you see this error

```
Error: insufficient permissions for device
```

**Solution**
1. Log in as root.
1. Go to folder /etc/udev/rules.d/".
1. Create file "51-android.rules".
1. Obtain vendor ID of the device

    - Connect phone to PC via USB cable
     > Here we take the example of the Gigaset GS290 device
    - Obtain vendor ID by running lsusb in bash.

1.  This should result in a screen with a few lines of output one of which would look like this:

    ```
    Bus 004 Device 004: ID 0e8d:201c MediaTek Inc
    ```

    Here it means that the GigaSet vendor ID is 0e8d.

1. Maintain line below in file "51-android.rules"

    ```
    SUBSYSTEM=="usb", ATTR{idVendor}=="0e8d", MODE="0666", GROUP="plugdev"
    ```

1. Assign read permissions on the file & reboot

    ```
    sudo chmod a+r /etc/udev/rules.d/51-android.rules
    ```

    Alternatively you can reload udev and the adb daemon with these commands

    ```
    sudo udevadm control --reload-rules
    adb kill-server
    adb start-server
    ```
