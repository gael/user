What to do when you encounter an issue in /e/OS?

## Connect to our GitLab server

   * If you have an account, [login](https://gitlab.e.foundation/users/sign_in)
   * If not, [create one](https://gitlab.e.foundation/e)

## Go to our issue dashboard

   - /e/ issues are listed and created [here](https://gitlab.e.foundation/e/backlog/-/issues)

   - Before creating a new issue, please search if it has previously been reported. To check for this

   - Type the bug in the search text box on the top of the screen as shown below

   ![New Search](/images/new_issue_search.png)

   - The screen will also display some other similar worded issues

   - If this search does not return any results create a new issue

## Create a new issue

   - Select a template for the issue from the drop down marked 'Choose a template'
   - 'Apply template'
   - Add a Title
   - Fill in all the details as requested in the template
   - Always [add logs](https://community.e.foundation/t/howto-get-a-log-from-your-phone/2366) where possible. This helps the developers debug the issue better.
   - Once you have finished, click `Submit issue`.



## Getting an error message in new ID creation on /e/OS Gitlab?

   - Some users may get an error message, while attempting to create a new ID on the /e/OS Gitlab
   - The error message may ask you to contact the site administrator. 
   - In case you run into such an error, please send a mail with the details of the ID you wish to create and the mail ID you got the error on, to <helpdesk@e.email>
   - Let us explain the reason behind this error. Some time back we had an issue with a large number of dummy IDs swamping our servers. To resolve this issue we disabled a few domains. The way to by pass this issue is to send your details to the email ID mentioned above. The team will help you with the ID creation.



## Syntax and Source

   * [Source](https://docs.gitlab.com/ee/user/project/description_templates.html#setting-a-default-template-for-issues-and-merge-requests)
   * [Markdown (GitLab)](https://docs.gitlab.com/ee/user/markdown.html)
   * [Markdown Guide  (GitLab)](https://about.gitlab.com/handbook/product/technical-writing/markdown-guide/)
