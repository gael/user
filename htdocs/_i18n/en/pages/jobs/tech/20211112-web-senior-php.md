## Responsibilities 

- Develop and maintain PHP applications for our ecloud platform.
- Coordinate with other developers: define style guides, tools and best practices.
- Ensure code ships on time and with a high level of quality – even that of your colleagues.
- Perform code reviews and provide technical mentoring to more junior developers.
- Keeping your skills sharp and up to date, then sharing that knowledge.
- Contribute to a positive vibe in the team.

## Requirements

- 8+ years of experience developing with PHP, most of them in a team.
- Confidence developing and connecting a single-page application (Angular, Vue or React) to the backend.
- Capable of implementing any given responsive website design using modern CSS.
- Knowledge of software design patterns and clean code, and the ability to break those rules when needed.
- Ideally having shipped complex business logic at a scale of thousands of users.
- Proficient with git, even complex and unusual operations. 
- Basic knowledge of docker containers.
- Having been a team lead before, or desire to become one.
- Attention to detail: we run a high-traffic platform where everything must work smoothly.
- Applied knowledge of web technologies to improve app performance and optimize client/server resources.
- Proactivity, excellent problem-solving and team skills.
- Fluent communication in English.
