## Responsibilities 

- Maintain and expand our fleet of dedicated root servers, automating tasks with **Ansible and GitLab CI/CD**.
- Migrate from _pets_ to _cattle_ approach using some container orchestration like **Docker Swarm or Nomad**.
- Proactively enhance our CI/CD pipelines to simplify operations and shorten release cycles. 
- Ensure high availability of our systems. Troubleshoot hardware, OS, application and network issues.
- Provide tier 2/3 assistance to customer support team for issues relating to infrastructure.
- Participate in **on-duty schedules** to resolve incidents affecting application availability.
- Collaborating with developers to review, fine tune, deploy and monitor new application versions.

## Requirements

- Solid experience working in medium or large-scale distributed applications, managing dozens of machines.
- Proficient with Ansible, which is our current automation tool.
- Experience setting up Continuous Integration flows with GitLab, GitHub, Travis, CircleCI or other similar platforms.
- Understanding of TCP/IP, UDP, SSL/TLS and other related Internet protocols.
- Proficient with shell scripting.
- Proactivity, excellent problem-solving and team skills.
- Fluent communication in English.
