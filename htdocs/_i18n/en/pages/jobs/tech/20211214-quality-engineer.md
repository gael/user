## Responsibilities 

- Ensure high quality level on /e/ products.
- Participate actively with internal resources to understand new features and bug fixes.
- Write and maintain test plans.
- Raise issues efficiently through different channels.
- Tweak all redundant actions to improve the testing worklow.
- Develop and maintain automation testing frameworks.
- Collaborate well to build excellent relationships across a global team and community.
- Proactively enhance our process after each release, learning and adjusting constantly.

## Requirements

- Good experience in tests to catch all kind of issues.
- Experience with Linux or mobile operating systems.
- Understanding how set up an OS on a device.
- Ability to test a software product from scratch.
- Good verbal and written communication skills.
- Familiarity with Shell command line.

## Will be a plus

- Experience with Android custom ROM.
- Understanding a programming language like Java, Kotlin or Python.
- Experience contributing to relevant open source projects.
- Familiarity with a Web or Android automation framework.
- Experience using Git, Gitlab.
- Ability to work with container technology like Docker.
- 2+ years of industry experience as a quality engineer.
