## Responsibilities 

- After 3 months we expect you to **become the maintainer of e.foundation and murena.com** – our 2 WordPress sites – keeping them attractive, reactive and up-to-date.
- Evaluate and connect to third-party services and partners.
- **Develop custom plugins** to solve specific business needs of our e-commerce platform.
- Implement major front-end redesigns when needed.
- Work together with a junior WP developer. **Perform code reviews and technical mentoring**.
- Propose and implement enhancements to the website performance and usability.

## Requirements

- At least 5 years of experience with WordPress and 2 with Woocommerce.
- Sound knowledge of PHP and WordPress: themes, plugins, filters, hooks. 
- Capable of implementing any given responsive website design using modern CSS and JS.
- Good knowledge of web technologies: HTTP, REST APIs. 
- Proficient with the usual flows of git.
- Ability to work directly with customers (the shop manager, in this case).
- Proactivity, excellent problem-solving and team skills.
- Fluent communication in English.
