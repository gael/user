## Responsibilities 

- Maintain and expand our fleet of dedicated storage servers, balancing capacity, performance and costs.
- Set up and maintain staging and production Ceph clusters in different DCs across the globe. 
- Proactively **monitor and improve our backup routines** and data integrity checks.
- Ensure high availability of our storage systems. Troubleshoot hardware, OS, application and network issues.
- Provide tier 2/3 assistance to customer support team for issues relating to infrastructure.
- Participate in **on-duty schedules** to resolve incidents affecting application availability.

## Requirements

- Mandatory: Experience managing **at least a TeraByte-scale production Ceph cluster**; preferably both RADOS Gateway and CephFS.
- MinIO or other object storage solutions a plus.
- Experience with **NFS, RAID and LVM**.
- Knowledge of Borg or similar encrypted backups solution.
- Understanding of TCP/IP, UDP, SSL/TLS and other related Internet protocols.
- Proficient with shell scripting.
- Proactivity, excellent problem-solving and team skills.
- Fluent communication in English.
