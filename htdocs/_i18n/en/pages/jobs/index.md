{% include snippets/why-work-at-e.md %}

## Software Engineering

The recruiting process will include at least one technical challenge and one or two interviews.

Please write to [techjobs@e.email](mailto:techjobs@e.email)

 {:.jobs-table}

  Position  | Description
  ------------- | -------------
  Android applications developer  | We are looking for experienced (5+ years) Android developers to work on various projects [Join us!](mailto:techjobs@e.email)
  Android Framework developer | Do you build and modify a custom ROM? Can you hack into the low-level layers of AOSP? [Get in touch!](mailto:techjobs@e.email)
  Android Embedded engineer | Device support for custom ROMs doesn't afraid you? Solving bootloops and SELinux issues are a game for you? [Get in touch!](mailto:techjobs@e.email)
  Network engineer | SIP, client-side networking including VoLTE/VoWiFi are not a secret for you? Even on Samsung devices?  [Get in touch!](mailto:techjobs@e.email)
  Reverse-engineers | Love to understand undocumented things like secret APIs? We need reverse engineers who never surrender until they find the solution [Get in touch!](mailto:techjobs@e.email)
  Camera developer | Want to take the challenge to make open source camera apps as good as high-end camera apps? [Get in touch!](mailto:techjobs@e.email)    
  Automation / Devops | Ansible, GitLab; server and container orchestration. [Full job description]({% translate_link jobs/tech/20211111-sysadmin-devops-automation %})  
  Wordpress developer | Experienced with both WP and Woocommerce. [Full job description]({% translate_link jobs/tech/20211112-web-wordpress %})  

## Other positions

Please write to [jobs@e.email](mailto:jobs@e.email)

  {:.jobs-table}

  Position   | Description
  ------------- | -------------
  Customer support and after sales | We are expanding our customer support and after sales team. Love to help solve problems big or small and always with a smile? :) [Contact us](mailto:jobs@e.email)   

## Important notes

- **Professional working proficiency in English** is mandatory for all positions (ILR level 3).
- All positions are **full time** if not specified otherwise.
- Please **include a CV** and ideally a cover letter in your submission.
- Due to the size of the team, **we are currently not accepting interns** or junior candidates with no previous experience.
