It appears that the boot.img is not always properly installed on some FP3 (/e/ pie version). It can lead to some error during updates.

Apply the following step to fix it:

1. Download your /e/ version [https://images.ecloud.global/dev/FP3/](https://images.ecloud.global/dev/FP3/)
1. Unzip the archive
1. Run the following script to resinstall properly all partitions, WITHOUT wiping data. 

    ⚠️ Kindly run commands one by one and check for the result. If you faced an error, reboot to bootloader again and continue.

    ```
    fastboot flash system_a system.img -S 522239K
    fastboot flash boot_a boot.img
    fastboot flash vendor_a vendor.img -S 522239K
    fastboot flash product_a product.img -S 522239K
    fastboot flash dtbo_a dtbo.img
    fastboot flash vbmeta_a vbmeta.img

    fastboot flash system_b system.img -S 522239K
    fastboot flash boot_b boot.img
    fastboot flash vendor_b vendor.img -S 522239K
    fastboot flash product_b product.img -S 522239K
    fastboot flash dtbo_b dtbo.img
    fastboot flash vbmeta_b vbmeta.img
    ```
1. Then run `fastboot reboot` to reboot your device on /e/.
