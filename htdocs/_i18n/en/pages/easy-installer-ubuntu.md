## Installation steps for linux

### via the command line

> Depending of your configuration, you may need to prefix the commands with `sudo`

- Open a console and type the below command to install the Easy Installer snap app

    ```shell
    snap install easy-installer --channel=latest/beta
    ```

### Installing the easy way !!
> Here we use the Ubuntu Software application

  - Open the Ubuntu Software application. It should open a screen like this
![](/images/ubuntu-software.png)

  - Go to the 'Explore' tab or click on the search icon
  - Type in 'easy-installer' . This should fetch the /e/OS easy installer as shown below

![](/images/easy-installer_search.png)

 - Click on the 'install' button to start the installation process
 ![](images/easy-installer_2.png)
 -
 - Depending on your configuration you may be asked to provide the admin user password to complete the installation
 - Once the installation is complete you will see this screen  

 ![](images/easy-installer_4.png)

 - Click on the Permissions button to view the permission needed for the easy-installer to run

 ![](images/easy-installer_5.png)

{% include snippets/easy_installer.html
   title="Want to learn more about these Permission ? "
   details=site.data.easy-installer.list
 %}

![](/images/easyinstaller-in-apps.png)

- After the installation is complete a new icon should be available on your Computer as shown in the screenshot above

- Click on it to open and try out our Easy Installer !!

### Reference guide to install on multiple OS with snap
To install the EasyInstaller on different Operating Systems using snap please refer [this guide](https://snapcraft.io/easy-installer)
