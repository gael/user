In `App Lounge`, the default /e/OS application repository, anyone can vet their favorite apps for privacy. Trackers embedded in apps and permissions are highlighted for all to see and make informed choices.

## What are trackers and permissions?

`Trackers` are pieces of software embedded in apps meant to collect data about what you do with the app, where you use it or target you with customized ads.

`Permissions` are actions that can be performed by the app on the phone, for example accessing contacts and SMS or using certain features like the camera or the Internet.

To help everyone judge the safety of an application regarding data leakage and application access before installing it on their phone, each app in App Lounge gets a privacy score.

Scoring is based on 2 criteria:

  - Trackers embedded in the application
  -	Permissions required by the application

## The calculation algorithm

The score is a rating out of 10 combined with a colour code from red to green. The higher the numeric value, the safer the application is for the user.
As mentioned above the amount of permissions and trackers found in an application combine to form the application’s privacy score:

{% include alerts/tip.html content="Privacy score = value derived from Trackers + value derived from Permissions" %}


- the number of trackers makes the main part of the scoring. It is based on [reports computed by Exodus Privacy](https://reports.exodus-privacy.eu.org/en/reports/list/) we access via an API.

- the number of permissions makes up the remaining part of the scoring. It is also based on [reports computed by Exodus Privacy](https://reports.exodus-privacy.eu.org/en/reports/list/).

### Value as derived from Trackers.
  -	0 tackers : tracking score = 9
  -	1 tracker : tracking score = 8
  -	2 trackers : tracking score = 7
  -	3 trackers : tracking score = 6
  -	4 trackers : tracking score = 5
  -	5 trackers : tracking score = 4
  -	6 or more trackers : tracking score = 0


### Value as derived from the Permissions
  - 1 Point : one to five permissions
  -	0 points : more than five permissions

The source code of the Privacy Score’s computing algorithm is available [here](https://gitlab.e.foundation/e/os/apps/-/blob/main/app/src/main/java/foundation/e/apps/PrivacyInfoViewModel.kt#L78).
The logic for calculating the Privacy Score will be improved further based on feedback and after observing it in practice.

## The Privacy Score’s limits

The Privacy Score is an help provided to App Lounge’s users but it has its limits. For instance:
 -	If an application does not use any trackers but has other ways to collect its user’s data it can have a good even excellent Privacy Score whereas the user’s privacy is not observed.
 -	It cannot say if the permissions requested by the application are legitimate.


## Feedback and Suggestions
We welcome suggestions and feedback to improve /e/ Operating System. You can log improvement issues [in GitLab](https://gitlab.e.foundation/e/backlog/-/issues) or send your comments to <helpdesk@e.email>.


{% include alerts/tip.html content="Exodus Privacy is not responsible for our privacy score calculation." %}
