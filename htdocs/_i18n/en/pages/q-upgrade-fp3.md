{% include alerts/danger.html content="Please note the upgrade process will wipe out the data on your device. Ensure you take a backup of important data from your device before initiating the upgrade." %}

## Basic Requirements

* adb and fastbooot should be setup and working on your PC
> To set up adb follow details provided [here](../../pages/install-adb)

* adb and fastboot need root rights in Linux and administrator rights in Windows.
> In Linux prepend sudo to any adb or fastboot command For e.g. “sudo fastboot reboot” and
in Windows start the command window as administrator.

## Unlock the bootloader (if locked)

On the device
1. Enable developer mode
1. In developer options, tick `Allow OEM unlock`
1. Reboot your device on fastboot mode

From your computer
1. Connect your device to the computer
1. Run `adb reboot bootloader` to reboot the device in fastboot mode
1. Run `fastboot flashing unlock` to unlock the bootloader
    - Select **UNLOCK THE BOOTLOADER** with `Volume +` and valid with `Power`

## Download

* Download Upgrade Build for the FP3 [here](https://images.ecloud.global/dev/FP3/)

## Installing /e/ from the bootloader

{% include alerts/tip.html content="Some of the commands have a -S after them. Please note it is a hyphen '-' followed by a capital 'S' and the value 522239K "%}

1. Unzip the archive

    ```
    unzip <zip>
    ```
1. Wipe user data

    ```
    fastboot -w
    ```
1. Install /e/ using the following commands (from fastboot mode)

    ```
    fastboot flash system_a system.img -S 522239K
    fastboot flash boot_a boot.img
    fastboot flash vendor_a vendor.img -S 522239K
    fastboot flash dtbo_a dtbo.img
    fastboot flash vbmeta_a vbmeta.img

    fastboot flash system_b system.img -S 522239K
    fastboot flash boot_b boot.img
    fastboot flash vendor_b vendor.img -S 522239K
    fastboot flash dtbo_b dtbo.img
    fastboot flash vbmeta_b vbmeta.img
    ```
1. Relock the bootloader

    ```
    fastboot flashing lock
    ```

    - Confirm on the device to lock the bootloader.
    - Select **LOCK THE BOOTLOADER** with `Volume +` and valid with `Power`
1. Enjoy! The device will now reboot automatically on /e/.

### Getting a `fastboot` error ?

-  On some versions of `fastboot` you may get an error while running the `Fastboot flash boot boot.img` command

- The below error may show up on the console

    ```
    fastboot error: couldn’t parse partition size ‘0x’
    ```
- A workaround for this error is as under. Run this command on the console:

    ```
    fastboot getvar current-slot
    ```
- This would show the current active slot. For e.g. if it shows `a` , run the below command

    ```
    fastboot --set-active=b
    fastboot flash boot_b boot.img
    ```
