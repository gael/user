
## What is adb

To understand what adb is please read [this guide](pages/adb)

## Steps to Install adb on your PC

{% include alerts/tip.html content="Windows OS users click [here](install-adb-windows)" %}

There are two methods using which Linux users can run adb or fastboot tools on their system

-  Installing and running the adb and fastboot tools using the code available in the Operating system. 
-  Downloading the tools from Android Developer tools and running it. We recommend this method


{% include alerts/danger.html content="The default adb installation on OS like Ubuntu can be outdated and can cause errors. We would recommend that linux distribution users ensure they use the installation process given here" %}



1. The [android site](https://developer.android.com/studio/releases/platform-tools) has different versions of the **Platform Tools** as you can see in the screenshot below. Each is built for a particular Computer environment. 

    ![](/images/howtos/adb/adb_install_linux_01.png)
     
{% include alerts/tip.html content="adb, fastboot tools are referred to as Platform Tools." %}


1. In this guide we will share instructions on how to use the Platform tools on a Ubuntu Computer. The same instructions should work on other Linux distributions.


1. Dowload the link titled `Download SDK Platform-Tools for linux`. 

1. Save the downloaded file in a folder or location you can remember. 

1. Browse into this folder and here extract the file you just downloaded


1. Open a console or a terminal inside this folder

    {% include alerts/tip.html content ="In Ubuntu you can right click in a folder and choose the option to `Open in Terminal` "%}


1. In the terminal type and execute the commands given below

  

 **Command to run the adb tools**
    
   `path-to-the-extracted-archive/platform-tools/`
    
at the end of the above command add the command you want to execute for e.g. `./adb devices`

  > The complete command would now look like 

  > yourPCName@Desktop:~/platform-tools$ ./adb devices

**Command to run For fastboot**

  `path-to-the-extracted-archive/platform-tools/`

at the end of the above command add the commands you want to execute for e.g  `./fastboot devices`

  > The complete command would now look like

  > yourPCName@Desktop:~/platform-tools$ ./fastboot devices

  {% include alerts/tip.html content ="Press the 'Enter' key to execute the command in a console or terminal."%}

### Add the path to you PC configuration


To avoid having to type the above commands every time you can add the location of the platform tools to you system configuration
> On Ubuntu systems

Add the below text to your .bashrc file

  `export PATH="$HOME/platform-tools:$PATH"`


{% include alerts/tip.html content ="You will be able to see the .bashrc file in your home directory. It is a hidden file. Press `ctrl` and `h` keys to display hidden files. "%}

> here we have assumed you have saved the downloaded files to a folder named `platform-tools` and this is in your home directory

After this you should be able to access `adb` and `fastboot` by typing only the below commands in a console

`adb devices`

or

`fastboot devices`
