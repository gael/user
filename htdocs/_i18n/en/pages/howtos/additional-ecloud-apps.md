## Purpose of the document

This guide explains how to control which applications are available on your ecloud.global account, disabling those that you don't need and enabling those that provide functionality which is of interest to you.
It also tries to cover some of the frequently asked question around app availability on ecloud.global.

## FAQ on available applications

### Which applications are currently available on ecloud.global?
   - Files
   - E-mail (Rainloop)
   - Contacts
   - Calendar
   - Photos
   - Notes (Nextcloud)
   - Tasks
   - Activity
   - Carnet (alternative notes app)
   - Deck (project planning board)
   - Bookmarks
   - News (RSS reader)

### Why don't you offer all possible applications?
For different reasons:
 - All need checking and sometimes tweaking. Some don't work out of the box or require external components.
 - Once we enable them, we commit to the app being available in the future, keeping it updated and offering support to our users.
 - We need to consider security and performance impact of a given app if enabled for everyone.
 - It would complicate the UI and usability of the cloud.

We [ran a survey](https://community.e.foundation/t/adding-new-apps-at-ecloud-global/12404/69) last year and we used the results to enable _Carnet_, _Deck_, _Bookmarks_ and _News_.\
_Maps_ and _Forms_ need further polish before they can be made available.

### Can you enable application XYZ?

We will run similar polls from time to time. You can create a topic on the [Community Forum](https://community.e.foundation/) and see if other users agree with such app being useful. We will then notice popular topics and evaluate the proposal.

### Do premium subscribers have access to more or better apps?

Not for now, but this may change in the future. For instance, we may offer apps that have some higher hardware requirements (like streaming music), or a licensed limit on the number of users.

## Configuring application order

You can decide the order of the applications in the navigation by just draging and droping the apps icons like the video below:

{% include components/video.html
    poster_image="/images/howtos/ecloud/reordering_applications.png"
    name="Reordering applications"
    mp4="/images/howtos/ecloud/reordering_applications.mp4"
    webm="/images/howtos/ecloud/reordering_applications.webm"
%}
