## Purpose of the document

Explain what is Hide My Email, and how to use it.

## Hide My Email

Hide My Email is a new random email address (alias), which can only be used to receive emails. You can use it on websites to avoid disclosing your real email address.

## Where to find it

Click on your main account icon on top right, then settings.

![account settings](/images/howtos/ecloud/account_settings.png)

Click on "Hide My Email" on the left sidebar menu. You will see your address. 
You can also click copy button to copy this email address, so you can directly paste it on the website, where you want to use it.

![copy hide my email](/images/howtos/ecloud/copy_hide_my_email.png)

## How to use it optimally without affecting your inbox

See [this document](/support-topics/email-filters#how-to-use-with-hide-my-email) which shows how to set a filter which will collect emails sent to your Hide My Email address in another folder.


