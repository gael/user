## Export contacts from Gmail using a .vcf file on a S9+

Steps : 
- Export contacts from Gmail to a .vcf file
- Attached your S9+ to the laptop using a USB data cable
- Open the S9+ Downloads Folder
- Placing the vcf file into that Folder
- Now in Contacts >> Settings  import the .vcf file
- Done.

## Workaround for VoLTE/VoWiFi for TMobile users
As you may be aware Volte/VoWifi are not available on /e/OS. 
- A temporary work around is available for users on TMobile. 
- Write to them and in return they would send you details of what is called a  “Cellspot”. 
- It propagates their bands wirelessly in your home and back-hauls them via my home internet
- You may need to run Ethernet from the device to your network switch. 
