1. Go to [ecloud settings](https://ecloud.global/settings/user/security)

1. Confirm that you are at the **Security** tab

   ![Ecloud screen showing where is the security tab](/images/howtos/change_recovery_address/security_tab.png "Ecloud menu") 

1. Change your recovery email

   ![Security screen showing where is the recovery address input field](/images/howtos/change_recovery_address/address_input.png "Button to change the recovery email")

1. Click on "Change Recovery Email" button

    ![Security screen showing the button to change the recovery address](/images/howtos/change_recovery_address/change_recovery_button.png "Recovery address field")

    - **Note**: Perhaps you need to authenticate to confirm your changes

      ![Modal asking you to authenticate](/images/howtos/change_recovery_address/auth_modal.png "Modal to authenticate to confirm")

1. Confirm that your changes were saved

    ![Message showing that the changes were saved](/images/howtos/change_recovery_address/change_confirmed.png "Message confirming your changes")
