## Purpose of the document
Explain how to use filters, in order to set rules, to automatically organise your incoming emails.

## How to configure

Open [the email app in the cloud](https://ecloud.global/apps/rainloop/), click the person icon on top right, then "settings".

![email settings](/images/howtos/ecloud/rainloop_email_settings.png)

Click "filters" on the left, then "Add a filter".
Give a name to the filter, then click "Add a condition". Here you can choose between different conditions like emails from or to section which will trigger a given action.
Then on Actions, choose what you would like to happen with these emails, and click done. Don't forget to click "Save" button for changes to be saved on the server.

Here is an example with a filter which will move all emails coming from gitlab@e.email to the folder named Gitlab:

![create a filter](/images/howtos/ecloud/create_a_filter.png)

## How to use with Hide my email
Open email app in the cloud, click on settings button at the bottom.

![email settings](/images/howtos/ecloud/email_settings.png)

Click on "create folder" and give the folder a name. Click create. Then click "back" button.

![create email folder](/images/howtos/ecloud/create_email_folder.png)

Now let's find out your "Hide my email" address. Click on your main account icon on top right. Then settings.

![account settings](/images/howtos/ecloud/account_settings.png)

Click on "Hide My Email" on the left. Then click copy button, to copy your alias.

![copy hide my email](/images/howtos/ecloud/copy_hide_my_email.png)

Now go back to the email app, click the person icon on top right, then "settings".

![email settings](/images/howtos/ecloud/rainloop_email_settings.png)

Click "filters" on the left, then "Add a filter". Give a name to the filter, in the conditions select "Recipients", "contains", and paste you hide my email address. In Actions, select "Move to" and the folder you created for these emails.

![create hme filter](/images/howtos/ecloud/create_hme_filter.png)

Click "Done", then click "Save". Click "Back". Now emails sent to your "Hide my email" address will be collected in the folder you created.
