## Upgrade your /e/ Cloud storage plan 
1. Login to your [/e/ Cloud](https://ecloud.global) account using your credentials
1. Login to your [murena.com](https://murena.com) account using credentials of account with which you bought your original subscription
1. If you have paid for your original subscription using **PayPal** as the payment method, please cancel your original subscription before going to the next step
    - In case you want to cancel subscription before subscription plan ends, please contact support at [helpdesk@e.email](mailto:helpdesk@e.email)
1. Upgrade from your /e/ Cloud account by clicking on the "Increase storage size" button in the "Files" app
![Screen showing "Increase storage Size" button](/images/howtos/ecloud/upgrade_storage.png "Increase storage size button")
1. You will be taken to a page with plans for upgrade. Select your desired plan from here
![Screen showing storage plans page](/images/howtos/ecloud/storage_plans.png "Storage plans")
1. You will be taken to checkout where you can pay for your storage upgrade
1. Once payment is successful, your storage quota will be updated automatically
