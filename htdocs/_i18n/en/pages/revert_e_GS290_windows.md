{% include alerts/danger.html content="These instructions are only for Windows PC users"%}

{% include alerts/danger.html content="The install will wipe all data on your phone."%}
1. Download and install [MTK driver](https://www.mediafire.com/file/b1xjnx3mj7b7f8y/MTK_USB_All_v1.0.8.zip/file) 
2. Download and extract the [SP Flash Tool](https://spflashtool.com/download/)
3. Download and extract the [stock rom firmware](https://androidfilehost.com/?fid=2981970449027570889)
4. Next  open sp flash tool where you extracted it.

    ![SP Flash tool screenshots](../images/howtos/spFlashTool.jpg)

5. Go to "download" tab (**NEVER** use "format")
6. In scatter file select `MT6763_Android_scatter.txt` inside the extracted firmware

    ![SP Flash select scatter file screenshots](../images/howtos/spFlashTool-scatterFile.jpg)

7. Click on "download" button.

    ![SP Flash download button screenshots](../images/howtos/spFlashTool-dlButton.jpg)

8. Turn off the phone
9. Plug in the phone ..it will ask you to plug in at this point
10. Now it will be downgraded to android 10
11. This process will wipe all your existing data
12. At the end you should see a pop-up with a green validation image. Long press on power button to restart the phone


Click here to [return](/devices/GS290/install#before-flashing) to continue the installation of /e/OS
