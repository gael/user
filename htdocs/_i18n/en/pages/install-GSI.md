{% include alerts/warning.html content="Make sure you can rollback to stock firmware in case /e/ doesn't work at all" %}

## Installing fastboot

### On Windows

{% include alerts/tip.html content="Ensure you install your device drivers on Windows PC before proceeding." %}

On Windows you will also need to find you device adb drivers

Sometimes, this [adb clockworkmod driver](https://adb.clockworkmod.com/) is enough

To install fastboot on your Windows PC follow [this guide](/pages/install-adb-windows)


### On Ubuntu / debian based linux

If you do not have fastboot installed on your linux device follow [this guide](/pages/install-adb)

## Find your phone architecture

An easy way to do this is to install `treble info`

> You will find the `treble info` app in the /e/OS default Apps Store as shown in the screenshot below


![](/images/TrebleCheck_Apps-small.png)

This is to verify that the 
- Phone is Project Treble compatible
- Check the CPU architecture it should show 
  - arm or 
  - arm 64

- If the device is "System as root" then a A/B build will be necessary

So, please note the following information:
- name of the required system image (for example system-arm64-ab.img.xz)
- Under Dynamic Partition, check if you need fastbootd


## Remove dm verity

- This can be done on some devices using twrp 
- A slightly more complicated method is by extracting the kernel and reflashing it with the below command

```shell
fastboot flash --disable-verity --disable-verification boot boot.img
```

## Flash the system image

Once you know which system image you need, download it

- [Arm A](https://images.ecloud.global/dev/treble_arm_avN/) (system-arm-a.img.xz)
- [Arm A/B](https://images.ecloud.global/dev/treble_arm_bvN/) (system-arm-ab.img.xz)
- [Arm64 A](https://images.ecloud.global/dev/treble_arm64_avN/) (system-arm64-a.img.xz)
- [Arm64 A/B](https://images.ecloud.global/dev/treble_arm64_bvN/) (system-arm64-ab.img.xz)
- [Binder 64 A](https://images.ecloud.global/dev/treble_a64_avN/) (system-a64-a.img.xz)
- [Binder 64 A/B](https://images.ecloud.global/dev/treble_a64_bvN/) (system-a64-ab.img.xz)

if fastbootd is required, start with

```shell
fastboot reboot fastboot
```

then, in every cases, extract system.img and flash it with this command line

```shell
fastboot flash system system.img
```

## Erase userdata

```shell
fastboot -w
```

## Optional steps

- install a custom recovery