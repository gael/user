## What is the /e/OS easy-installer?

- The Easy Installer is a desktop application which **helps users install /e/** on supported devices.

- In its current version the tool works on the **Linux** and **Windows OS**

![](/images/easy-installer-mainscreen.png)

## How can I install the /e/OS easy-installer?

The easy-installer beta is available for Linux and Windows OS.

[Installation guide for Linux](easy-installer-linux)

[Installation guide for Windows](easy-installer-windows)
## List of devices supported by the easy-installer

- The easy-installer beta version supports **10 devices**

{% include alerts/tip.html content="To get the Easy Installer working on your PC you would need to install the OS specific fastboot drivers first. Follow the instructions in the Install Guides gives against each device." %}


| **Vendor** | **Device Name**                     | **CodeName** | **Additional Info**   |**Driver Installation**                                          | **Model (s)**         |
| ---------- | ----------------------------------- | ------------ | --------------------- | --------------------------------------------------------------- | --------------------- |
| Gigaset    | [GS290](devices/GS290)              | gs290        |                       |[Install Guide](pages/install_easy_installer/win_gs290)          | gs290                 |
| Fairphone  | [Fairphone 3](devices/FP3)          | FP3          |                       |[Install Guide](pages/install_easy_installer/win_fp3)            | FP3                   |
| Fairphone  | [Fairphone 3+](devices/FP3)         | FP3+         |                       |[Install Guide](pages/install_easy_installer/win_fp3)            | FP3+                  |
| Samsung    | [Galaxy S9](devices/starlte)        | starlte      | Exynos only           |[Install Guide](pages/install_easy_installer/win_samsung_phones) | SM-G960F, SM-G960F/DS |
| Samsung    | [Galaxy S9 Plus](devices/star2lte)  | star2lte     | Exynos only           |[Install Guide](pages/install_easy_installer/win_samsung_phones) | SM-G965F, SM-G965F/DS |
| Samsung    | [Galaxy S8](devices/dreamlte)       | dreamlte     | Exynos only           |[Install Guide](pages/install_easy_installer/win_samsung_phones) | SM-G950F              |
| Samsung    | [Galaxy S8 Plus](devices/dream2lte) | dream2lte    | Exynos only           |[Install Guide](pages/install_easy_installer/win_samsung_phones) | SM-G955F              |
| Samsung    | [Galaxy S7](devices/herolte)        | herolte      | Exynos only           |[Install Guide](pages/install_easy_installer/win_samsung_phones) | SM-G930F              |
| Samsung    | [Galaxy S7 Edge](devices/hero2lte)  | hero2lte     | Exynos only           |[Install Guide](pages/install_easy_installer/win_samsung_phones) | SM-G935F              |
  
>For more details you can refer a [user updated post](https://community.e.foundation/t/list-devices-working-with-the-easy-installer/28396) on the [forum](https://community.e.foundation) where the Easy Installer was tested by /e/OS users against these models
## Why are more devices and OS not supported?

Due to resource constraints the /e/ team cannot support all the work needed to add more devices or other operating systems. Here we request users in the community with development experience to help us on:
- Porting the Easy Installer to MacOS
   - the java software should run without issues
   - some development work may be required on the script to port it
- Support new devices
   - with its architecture based configuration files, it is easy to add new devices
  
{% include alerts/tip.html content="We share details on how you can add more devices in [this document](easy-installer-contribute)"%}




## I want to develop the source further. How can I help?

To build or improve upon the easy-installer code check the [documentation here...](easy-installer-contribute)


{% include alerts/tip.html content="To discuss about the easy-installer project, get help or contribute on it come to [our Community Forum](https://community.e.foundation/c/community/easy-installer/136)" %}

## Still have some queries on the easy-installer ?
Check out the [easy-installer FAQ](easy-installer-faq)

**Thank you in advance!**
