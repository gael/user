/e/ is about to release Android Q version of its OS. You can find below the list of devices available for the beta:

- [Essential Phone - mata](devices/mata)
- [Oneplus 2 - oneplus2](devices/oneplus2)
- [Oneplus 3 - oneplus3](devices/oneplus3)
- [OnePlus 6 - enchilada](devices/enchilada)
- [Oneplus 6T - fajita](devices/fajita)
- [Oneplus 7 - guacamoleb](devices/guacamoleb)
- [Oneplus 7T - guacamole](devices/guacamole)
- [Motorola G7 - river](devices/river)
- [Motorola G7 Plus - lake](devices/lake)
- [Xiaomi Mi A1 - tissot](devices/tissot)


Kindly be aware of the beta status of those builds. It means that some issue can be present. Find [here](https://gitlab.e.foundation/e/backlog/-/issues?scope=all&utf8=%E2%9C%93&state=opened&not%5Blabel_name%5D%5B%5D=status%3A%3Atest&epic_id=25) the list of bugs already reported.

Feel free to [create a new issue](https://gitlab.e.foundation/e/backlog/-/issues/new) if you are facing a strange behavior or error.

Thank you in advance for your feedback!

> /!\\ At the moment, the beta is only available for fresh install.
