## What's in /e/OS?

/e/OS is a complete mobile ecosystem which includes a mobile operating system (ROM) and online-services. The /e/OS project was launched at the end of 2017. It has historically been the first mobile OS to focus on deGoogling.

/e/OS is:
- **Open source**
- **Pro-privacy**
- Compatible with existing **Android applications**
- Cares about **usability**
- running on more than 200 different smartphone models

/e/OS is an **alternative to the Apple/Google duopoly** for smartphone users.

/e/OS consists of:
*  an installable mobile operating system for smartphones, which is forked from LineageOS/AOSP, and completely "deGoogled"
*  a default set of open source applications that have been improved and optimized for the user
*  various online services that are linked to the mobile operating system, such as: a meta search engine for the web, a cloud drive with data synchronization, mail, calendar, notes, tasks.

/e/OS is also available pre-installed on a range of [Murena smartphones](https://murena.com).


## Description of the /e/OS ROM

The /e/OS ROM is a fork of Android and in particular the LineageOS flavor of Android. 

At present we support five Android versions: 
 - R
 - Q
 - Pie

 Discontinued:
 - Oreo 
 - Nougat


### DeGoogling / UnGoogling in /e/OS

The goal of "deGoogling" is 
- To remove or disable any feature or code that is sending data to Google servers, or at least to anonymize those accesses
- To offer non-Google default online services, including for search.

- the Google default search engine is removed and replaced by other services (see below in [default apps](/what-s-e/#e-default-applications) and services)
- Google Services are replaced by microG and alternative services (see below for more details)
- All Google apps are removed and replaced by equivalent Open Source applications. The one exception is the [Maps Application](/maps)
- No use of Google servers to check connectivity  
- NTP servers are not Google NTP servers anymore
- DNS default servers are not Google anymore, and their settings can be enforced by the user to a specific server
- Geolocation is using Mozilla Location Services in addition to GPS
- CalDAV/CardDAV management and synchronization application (DAVDroid) is fully integrated with the user account and calendar/contact application

More details about the state of /e/OS OS deGooglisation can be found in [this white paper](https://e.foundation/wp-content/uploads/2020/09/e-state-of-degooglisation.pdf).

### Advanced Privacy features in /e/OS

![](../images/advancedprivacy.jpg)

Starting from /e/OS V1, /e/OS features a new "Advanced Privacy" module that offers the following features:
- tracker management: users know in real time the number of active trackers used by installed applications. All trackers' activity can be cut, with some granularity by application
- location faking: /e/OS users can choose to have their location faked for applications that use location. Faking goes from "random plausible locations" to a specific, chosen, location.
- IP address faking: /e/OS users can chose to fake their IP address, for all application, or specific applications. This is made possible by redirecting the internet traffic to the TOR network.

Also, an "hide my email" address alias is provided to each user, for users who don't want to disclose their real email address.

### /e/OS User interface

![](../images/e_os_v1_launcher.jpg)

- A new launcher "BlissLauncher" was developed to offer a simple and attractive look and feel. Its features include: 

  - Easy to understand User Interface with almost no learning curve for the new user
  - Application widget support 
  - Uses the "Spot" search engine by default
  - Icons that were designed specifically for the project. 
  - Improvements made in settings to offer a better looking interface

### Services

- A specific file synchronization service has been developed for multimedia and file contents.
 
It can use [Murena cloud services server at https://murena.io](https://murena.io) (formerly "ecloud.global) or [self-hosted cloud service](https://gitlab.e.foundation/e/infra/ecloud-selfhosting)s (beta).
- The weather provider is the LOS weather provider using data fetched from [Open Weather Map](https://openweathermap.org/)
- Account Manager has been modified to support /e/OS/-Murena user accounts, with a single identity, thus providing /e/OS users
   -  Mail
   -  Content sync
   -  Calendar
   -  Contacts
   -  Notes
   -  Tasks 

/e/OS users can decide where they want to store their data. It could be either the [Murena cloud](https://murena.io) based on NextCloud, or [self-hosted](https://gitlab.e.foundation/e/infra/ecloud-selfhosting) by the user.

## /e/OS default applications

All pre-installed applications are open source applications except for the Maps app (read details about this in the [Maps FAQ](/maps)).

- [Web-browser](https://gitlab.e.foundation/e/apps/browser): an deGgoogled fork of Chromium, built from Bromite patch-sets, with specific /e/OS settings. It features an ad-blocker by default.
- [Mail](https://gitlab.e.foundation/e/apps/mail): a fork of K9-mail for better user experience, some bugfixes and support for oauth, with OpenKeyChain for PGP encryption support
- [Message](https://gitlab.e.foundation/e/apps/Message): a fork of QKSMS
- [Camera](https://gitlab.e.foundation/e/apps/camera): a fork of OpenCamera
- [Dialer](https://gitlab.e.foundation/e/os/android_packages_apps_Dialer): default Android dialer application
- [Calendar](https://gitlab.e.foundation/e/apps/calendar): a fork of Etar calendar, that fixes crashes with calendar invitations
- [Contact](https://gitlab.e.foundation/e/os/android_packages_apps_Contacts): default Android contact application
- [Clock](https://gitlab.e.foundation/e/os/android_packages_apps_DeskClock): a fork of Android deskclock application
- [Gallery](https://gitlab.e.foundation/e/os/android_packages_apps_Gallery2): default Android gallery3d application
- [Filemanager](https://gitlab.e.foundation/e/os/android_packages_apps_DocumentsUI): Android documentsui application
- [Sound recorder](https://gitlab.e.foundation/e/os/android_packages_apps_Recorder): default LineageOS sound recorder
- [Calculator](https://gitlab.e.foundation/e/os/android_packages_apps_ExactCalculator): default Android calculator2 application
- [Keyboard](https://gitlab.e.foundation/e/os/android_packages_inputmethods_LatinIME): default Android keyboard
- [App Lounge](https://gitlab.e.foundation/e/apps/apps) is the /e/OS application installer, that offers [millions applications](https://e.foundation/e-os-available-applications/). Applications are accessed from Google Play Store (commercial apps), F-Droid (open source apps) and also Progressive Web Apps, all at a single place. App Lounge features a "Privacy Score" for each application, computed from the number of trackers found by Exodus Privacy in each application, and from the number of permissions.

Other preinstalled applications:
- [Maps](/maps#how-magicearth-is-added-into-eos): MagicEarth (read the [Maps FAQ](/maps) for details about its license)
- [PDF reader](https://gitlab.e.foundation/e/apps/pdfviewer): PdfViewer Plus
- [Notes](https://gitlab.e.foundation/e/apps/notes): a fork of NextCloud Notes to support Murena online accounts
- [Tasks](https://gitlab.e.foundation/e/apps/tasks): OpenTasks

## Online services

![online_services_login_eos_ecloud](../images/online_services_login_eos_ecloud.png)

- "spot" at [https://spot.ecloud.global](https://spot.ecloud.global) search engine is a fork of [SearX](https://searx.me/)  meta-search engine. Improvements made include:
  
  - Peformance enhancements
  - User interface improvement

- [Murena Cloud](https://murena.io) includes 
  -  Drive
  -  Mail
  -  Calendar
  -  Contacts
  -  Notes
  -  Tasks
  -  Office

It is built upon NextCloud, Postfix, Dovecot and OnlyOffice. 

It has been integrated to offer a single login identity in /e/OS as well as online through a web interface.

Users can retrieve their data at [https://ecloud.global](https://ecloud.global) services, or [self-hosted](https://gitlab.e.foundation/e/infra/ecloud-selfhosting) on their own server

## /e/OS Development Updates

### 2019 
We [addressed a number of issues](https://gitlab.e.foundation/search?group_id=&project_id=&repository_ref=&scope=issues&search=Infosec+Handbook+Review)  with ungoogling. 

This included among others: 
 - removed connectivity check against Google servers
 - replaced Google NTP servers by pool.ntp.org servers
 - replaced Google DNS servers by 9.9.9.9 by default and offer users to set the DNS servers of their choice

More details about the state of /e/ OS deGooglisation can be found in [this white paper](https://e.foundation/wp-content/uploads/2020/09/e-state-of-degooglisation.pdf).



### 2020 
- We introduced the "easy installer" . It lets users install /e/ OS without using obscure tools or the command line. 
  
  Currently available in Linux and Windows. Development is now community driven.
- Working on pro-privacy features such as a "privacy dashboard"
- Added more supported smartphones (FairPhone 3 and 3+, PinePhone, GigaSet...) 
- Worked on adding more features such as "my SMS to cloud" and "my geolocation to cloud" (Work in Progress)
- Added Progressive Web Apps (PWAs) to the application installer V2


###  2021

- Add the capability to un-install some of the system applications where possible
- Offer corresponding Google-free SDKs for app publishers, because we believe that web apps is the future for most mobile applications.
- Make /e/OS available in two versions:
  - standard /e/OS with default applications
  - /e/OS with minimal number of default/pre-installed applications 
   > This is partially implemented as some devices which have less disk space get a [MINIMAL_APPS](/support-topics/build-e#5-build-options) version of the /e/OS

### 2022

- /e/OS V1 is available
- App Lounge with access to the full catalog of Android apps is available
- Advanced Privacy is added to /e/OS

## What to expect in future releases?
Check out the [eRoadmap section]({% translate_link support-topics/e-road-map %}) in our FAQ

## Want to give /e/OS a try?

[Install it now](/devices/)!

Or 

Purchase [a Murena smartphone](https://murena.com).
