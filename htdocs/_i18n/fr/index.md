## À propos du Projet

> En savoir plus sur le projet /e/

- [What's /e/? Read the product description!]({% translate_link what-s-e %})
- [Install /e/ mobile OS on a supported smartphone!]({% translate_link devices %})
- [Create a free ecloud account]({% translate_link create-an-ecloud-account %})
- [HOWTOs]({% translate_link support-topics %})
- [FAQ]({% translate_link support-topics %})
- [Share your /e/xperience!]({% translate_link testimonies %})


## Contribute to /e/

> Do you want to help the project?

- [Projects looking for contributors]({% translate_link projects-looking-for-contributors %})
- [Job openings at /e/]({% translate_link jobs %})
- [ROM Maintainer]({% translate_link rom-maintainer %})
- [Translators]({% translate_link translators %})
- [Testers]({% translate_link testers %})
- [Volunteers]({% translate_link volunteers %})

## Learn more about

> Information about the project or its applications

- [/e/ OS easy-installer]({% translate_link easy-installer %})
- [Maps application]({% translate_link maps %})
- [The different build types]({% translate_link build-status %})
- [Apps Installer FAQ]({% translate_link apps %})
- [e.foundation Legal Notice & Privacy](https://e.foundation/legal-notice-privacy/)

## Shortcuts

> Quick links to /e/ websites, forums and telegram channels

- [/e/ web site](https://e.foundation/)
- [/e/ Source Code](https://gitlab.e.foundation/e)
- [Need Help? Support?](https://e.foundation/contact/)
- [Report a bug in the /e/ ROM or apps](https://gitlab.e.foundation/e/backlog/-/issues/new)
- [Add your Smartphone to the supported list](https://community.e.foundation/c/e-devices/request-a-device)
