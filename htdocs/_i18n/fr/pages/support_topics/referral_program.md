# Programme de parrainage Ecloud

Obtenez jusqu'à 40€ de crédits pour votre stockage cloud en invitant vos amis sur eCloud !

Pour chaque ami·e qui rejoint et crée un compte eCloud, vous gagnerez 2€ que vous pourrez utilisrr pour votre stockage eCloud.

ecloud is your personal email account, your agenda and contacts, your drive on the cloud and
your online office suite, all combined in one single service, simple to use. ecloud is powered by
proven open-source software like NextCloud and OnlyOffice.

Your ecloud account is free up to 1GB of storage. Our paid plans start at 1,99€ per month for
20GB.

## How does this work?

Once you have a valid e.email account, it is very easy.

All you have to do is sign up at [https://murena.com/my-account/](https://murena.com/my-account/), our ecommerce platform, so we can assign your cloud credits.

Connectez-vous à votre compte murena.com,  repérez la rubrique « Parrainage » et copiez votre
lien de parrainage unique et anonyme.

You can copy your unique link easily and use it elsewhere, for example, emailing it to your
friends or even sharing it in a tweet.

For every friend who uses this link to create a free account, you will receive 2€ of cloud
credits in your murena.com account.

## Comment inviter mes amis ?

It is very simple, you just have to send the unique registration link located in your murena.com
account. The link is completely anonymous, it does not collect identifying information such as
name or email address.

## How can I use the credits?

Your credits can be used once you subscribe to a extended storage plan. They will be
converted in discount towards your paid subscription.

## How long are my credits valid?

Your credits will be valid for 24 months.

## How long will this referral program last?

The ecloud referral program will be active for 2 months, from April 6th to June 6th, 2022.

## Can I redeem my credits against the purchase of a phone or something else at murena.com?

Your credits are only valid towards the subscription of paid cloud plan.

## How does it work for my friends?

Upon clicking on the referral link, your friend will be taken to the account invitation page where
she/he will be able to create a user ID and password.

Upon completing the account creation process, your friend will have a link to an ecloud account where she/he can access the newly created e.email account, the cloud storage or get started with the only office suite.

## How do I create an ecloud account?

It is very simple. Visit [https://murena.io/signup](https://murena.io/signup) to request your invitation. You will only be included in the referral program once you also sign up at [https://murena.com/my-account/](https://murena.com/my-account/) and use your referral links.

## What is ecloud?

ecloud is a complete, fully “deGoogled”, online ecosystem.

ecloud is your personal email account with the e.email domain, your agenda and contacts, your drive on the cloud and your online office suite, all combined in one single service, simple to use. ecloud is powered by proven open-source software like NextCloud and OnlyOffice.

## Is the referring link anonymous ?

The link is completely anonymous, it does not collect identifying information such as name or
email address. The person sending the link has no visibility to which people have used the
referring link.

## I don’t see my credits in my murena.com space, what shall I do ?

Contact us at [helpdesk@e.email](mailto:helpdesk@e.email) so we can investigate and we can see how we solve this issue
for you.
