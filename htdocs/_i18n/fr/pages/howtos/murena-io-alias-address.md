## Purpose of the document

This document explains how to use murena.io email alias for your email account.

## murena.io email adresses

Ecloud users now have the choice to use murena.io domain for emails. For example, a user called "jane.doe" is now able to use both `jane.doe@e.email` and `jane.doe@murena.io` email adresses. You can send emails with this alias, so others will see received emails coming from your murena.io alias. You can use it as your default email address for sending emails, but don't forget that your account name is still the one with e.email

## How to configure it on ecloud

Open email app in the cloud, click on settings button at the bottom.

![email settings](/images/howtos/ecloud/email_settings.png)

Click on "Accounts". Under identities section you will see both your e.email and murena.io identities. You can move your murena.io identity above e.email identity using the icon with 6 dots. The one on the top will be used by default when you are writing a new email.

![identities](/images/howtos/ecloud/rainloop_identities.png)

Now, click back, then new message. You will see your new identity in the "from" section. If you click on it, you can change it to the old one. For the next email, you will again see the default identity.

## How to configure it on the phone

Ouvrez l'application "Mail". Cliquez sur l'icône des 3 lignes en haut à gauche, puis sur "Paramètres". Sous Comptes, cliquez sur votre adresse e-mail.

![email settings](/images/howtos/ecloud/phone_email_settings.png)

Maintenant cliquez sur "Envoi du courriel, puis sur "Gérer les identités". Cliquez sur l'icône avec 3 points en haut à droite, puis sur "Nouvelle identité".
Entrez votre nom et votre nouvelle adresse e-mail. Puis cliquez sur "enregistrer".

![edit identity](/images/howtos/ecloud/phone_email_edit_identity.png)

Maintenant, sur l'écran "Gérer les identités", appuyez et maintenez votre doigt sur la nouvelle identité que vous avez créée, puis choisissez "Déplacer en haut / établir par défaut". Celle qui se trouve en haut sera utilisée chaque fois que vous écrirez un nouvel e-mail.

![make it default identity](/images/howtos/ecloud/phone_make_default_identity.png)

Cliquez sur "retour" plusieurs fois jusqu'à ce que vous soyez de retour dans votre boîte de réception. Maintenant, chaque fois que vous écrivez un e-mail (vous pouvez cliquer sur l'icône du stylo), vous verrez votre nouvelle identité dans la section "de". Si vous cliquez dessus, vous pourrez la remplacer temporairement par l'ancienne identité.

## Comment configurer dans une application de messagerie électronique de bureau

Veuillez noter que vous pouvez utiliser murena.io comme adresse électronique **mais pas comme identifiant d'utilisateur**.
Dans votre client de messagerie, vous devez toujours vous identifier avec l'adresse en `@e.email`. 
Normalement, vos paramètres devraient être détectés automatiquement. Entrez votre mot de passe et sauvegardez-le si vous ne voulez pas qu'on vous le demande à chaque fois. Les paramètres pour une configuration manuelle sont disponibles [ici](https://e.foundation/email-configuration/).

![email_client_1](/images/howtos/ecloud/email_client_1.png)

Une fois que votre courriel est configuré dans l'application comme d'habitude, ouvrez les paramètres du compte.

![email properties](/images/howtos/ecloud/evolution_email_properties.png)

Dans "alias", ajoutez votre alias murena.io. Cliquez ensuite sur OK.

![aliases](/images/howtos/ecloud/evolution_aliases.png)

Vous devriez maintenant être en mesure de choisir manuellement l'adresse électronique à utiliser dans le champ "de" à chaque fois. Dans votre application de messagerie, vous devriez pouvoir définir l'adresse "de" à utiliser par défaut en ajoutant une nouvelle identité et en choisissant de l'utiliser par défaut. 

## Astuces

Vous pouvez également utiliser un [filtre de courriel](/support-topics/email-filters) pour trier vos courriels envoyés aux deux adresses dans des dossiers différents.
