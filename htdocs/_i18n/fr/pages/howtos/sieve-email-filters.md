## Objectif du document
Expliquer comment utiliser les filtres, afin de définir des règles pour organiser automatiquement vos e-mails entrants.

## Comment configurer

Ouvrez [l'application mail dans le cloud](https://ecloud.global/apps/rainloop/), cliquez sur l'icône représentant une personne en haut à droite, puis sur "paramètres".

![email settings](/images/howtos/ecloud/rainloop_email_settings.png)

Cliquez sur "filtres" à gauche, puis sur "Ajouter un filtre".
Donnez un nom au filtre, puis cliquez sur "Ajouter une condition". Ici, vous pouvez choisir entre différentes conditions, comme les e-mails en provenance de ou à destination de, qui déclencheront une action donnée.
Ensuite, dans la section "Actions", choisissez ce que vous voulez qu'il advienne de ces e-mails, puis cliquez sur "Terminé". N'oubliez pas de cliquer sur le bouton "Enregistrer" pour que les changements soient enregistrés sur le serveur.

Voici un exemple avec un filtre qui déplacera tous les e-mails provenant de gitlab@e.email vers le dossier nommé Gitlab :

![create a filter](/images/howtos/ecloud/create_a_filter.png)

## Mode d'emploi de Masquer mon adresse e-mail
Ouvrez l'application mail dans le cloud, cliquez sur le bouton paramètres en bas.

![email settings](/images/howtos/ecloud/email_settings.png)

Cliquez sur "créer un dossier" et donnez un nom au dossier. Cliquez sur créer. Puis cliquez sur le bouton "retour".

![create email folder](/images/howtos/ecloud/create_email_folder.png)

Maintenant, trouvons votre adresse "Masquer mon adresse e-mail". Cliquez sur l'icône de votre compte principal en haut à droite. Puis sur les paramètres.

![account settings](/images/howtos/ecloud/account_settings.png)

Cliquez sur "Masquer mon adresse e-mail" sur la gauche. Puis cliquez sur le bouton "Copier", pour copier votre alias.

![copy hide my email](/images/howtos/ecloud/copy_hide_my_email.png)

Maintenant retournez dans l'application mail, cliquez sur l'icône en haut à droite représentant une personne, puis sur "paramètres".

![email settings](/images/howtos/ecloud/rainloop_email_settings.png)

Cliquez sur "filtres" à gauche, puis "Ajouter un filtre". Donnez un nom au filtre, dans les conditions sélectionnez "Destinataires", "contient", et collez votre adresse Masquer mon adresse e-mail. Dans Actions, sélectionnez "Déplacer vers" et le dossier que vous avez créé pour ces emails.

![create hme filter](/images/howtos/ecloud/create_hme_filter.png)

Cliquez sur "Terminé", puis sur "Enregistrer". Cliquez sur "Retour". Désormais, les courriels envoyés à votre adresse "Masquer mon adresse e-mail" seront collectés dans le dossier que vous avez créé.
